# Helper script for setting up CoViS build system
#
# The following variables are set:
#   COVIS_COMPILER
#   COVIS_BUILD_STATIC
#   COVIS_PRECOMPILE
#   COVIS_INCLUDE_DIRS
#   COVIS_LIBRARY_DIRS
#   COVIS_LIBRARIES
#   COVIS_DEFINITIONS
#   COVIS_DEFINITIONS_EXTRA
#   INSTALL_DIR_INC
#   INSTALL_DIR_LIB
#   INSTALL_DIR_BIN
#   INSTALL_DIR_DOC
#   INSTALL_DIR_CMAKE
#

##
# Check for C++ compiler
##
if(NOT CMAKE_CXX_COMPILER)
  message(FATAL_ERROR ">> C++ compiler missing!")
endif(NOT CMAKE_CXX_COMPILER)

##
# Check for valid compiler type (GCC, CLANG)
# Stores upper-case compiler string in COVIS_COMPILER
# Taken from here: http://stackoverflow.com/questions/10046114
##
string(TOUPPER ${CMAKE_CXX_COMPILER_ID} COVIS_COMPILER)
if(COVIS_COMPILER STREQUAL "GNU") # GNU, set to GCC
  set(COVIS_COMPILER "GCC")
elseif(COVIS_COMPILER STREQUAL "CLANG") # Clang
  # Do nothing for now
elseif(COVIS_COMPILER STREQUAL "INTEL") # ICC
  message(FATAL_ERROR "Intel compiler not supported!")
elseif(COVIS_COMPILER STREQUAL "MSVC") # MSVC
  message(FATAL_ERROR "Visual Studio  compiler not supported!")
else(COVIS_COMPILER STREQUAL "GNU")
  message(FATAL_ERROR "Unknown compiler ID: ${CMAKE_CXX_COMPILER_ID}!")
endif(COVIS_COMPILER STREQUAL "GNU")

##
# In case of GCC, get version (for use below)
##
if(COVIS_COMPILER STREQUAL "GCC")
    execute_process(COMMAND ${CMAKE_CXX_COMPILER} -dumpversion
                    OUTPUT_VARIABLE GCC_VERSION)
    string(REGEX MATCHALL "[0-9.]+" GCC_VERSION ${GCC_VERSION})
endif(COVIS_COMPILER STREQUAL "GCC")

##
# Build type option, default to Release
##
if(CMAKE_BUILD_TYPE)
  if(NOT (CMAKE_BUILD_TYPE STREQUAL "Release" OR
          CMAKE_BUILD_TYPE STREQUAL "Debug" OR
          CMAKE_BUILD_TYPE STREQUAL "RelWithDebInfo" OR
          CMAKE_BUILD_TYPE STREQUAL "MinSizeRel"))
    message(STATUS ">> Unknown build type: \"${CMAKE_BUILD_TYPE}\"! Disabling build flags...")
  endif()
else(CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE "Release" CACHE STRING "Build type: Release, Debug, RelWithDebInfo, MinSizeRel" FORCE)
endif(CMAKE_BUILD_TYPE)

##
# Library type option
##
option(COVIS_BUILD_STATIC "Set to ON to build static libraries" OFF)

##
# Precompile option
##
option(COVIS_PRECOMPILE "Set to ON to precompile CoViS with a limited set of point types" OFF)

##
# The main tuple of state variables necessary for using CoViS from outside
##
set(COVIS_INCLUDE_DIRS "" CACHE STRING "" FORCE)
set(COVIS_LIBRARY_DIRS "" CACHE STRING "" FORCE)
set(COVIS_LIBRARIES "" CACHE STRING "" FORCE)
set(COVIS_DEFINITIONS "" CACHE STRING "" FORCE)

##
# C++11 flag for GCC < 5 and Clang
##
if(COVIS_COMPILER STREQUAL "GCC")
  if(GCC_VERSION VERSION_LESS 4.7)
    # GCC version < 4.7 uses c++0x
    list(APPEND COVIS_DEFINITIONS -std=c++0x)
  else(GCC_VERSION VERSION_LESS 4.7)
    list(APPEND COVIS_DEFINITIONS -std=c++11)
  endif(GCC_VERSION VERSION_LESS 4.7)
elseif(COVIS_COMPILER STREQUAL "CLANG")
  list(APPEND COVIS_DEFINITIONS -std=c++11)
endif(COVIS_COMPILER STREQUAL "GCC")

##
# Warning flags
##
if(COVIS_COMPILER STREQUAL "GCC" OR COVIS_COMPILER STREQUAL "CLANG")
  # TODO: I don't want the -Wno-* flags, but goddamn PCL+VTK are causing them!
  list(APPEND COVIS_DEFINITIONS -Wall -Wextra -Wno-comment -Wno-deprecated-declarations -Wno-invalid-offsetof)
endif(COVIS_COMPILER STREQUAL "GCC" OR COVIS_COMPILER STREQUAL "CLANG")

# TODO: Yet another one, this time caused by Python
if(COVIS_COMPILER STREQUAL "CLANG")
  list(APPEND COVIS_DEFINITIONS -Wno-deprecated-register)
endif(COVIS_COMPILER STREQUAL "CLANG")

##
# Extra compiler flags only for this build
##
if(NOT COVIS_DEFINITIONS_EXTRA)
  set(COVIS_DEFINITIONS_EXTRA "" CACHE STRING "Extra CoViS definitions")
endif(NOT COVIS_DEFINITIONS_EXTRA)

##
# Setup installation variables - CMake installation prefixes these with
# CMAKE_INSTALL_PREFIX
##
set(INSTALL_DIR_INC include)
set(INSTALL_DIR_LIB lib)
set(INSTALL_DIR_BIN bin)
set(INSTALL_DIR_DOC share/doc)
set(INSTALL_DIR_CMAKE ${CMAKE_ROOT}/Modules)

##
# Make sure that libraries/binaries are built into convenient locations
##
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/${INSTALL_DIR_BIN})
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/${INSTALL_DIR_LIB})
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/${INSTALL_DIR_LIB})
