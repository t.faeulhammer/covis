# Helper script for CoViS deployment
# 
# This script performs the following actions:
#   Touch up of state variables
#   Configuration and installation of the find script FindCOVIS.cmake
#   Creation of the target 'uninstall'
#
# This script assumes valid information in
#   COVIS_HEADERS
#   COVIS_SOURCES
#   COVIS_LIBRARY
#   COVIS_LIBRARIES
#   COVIS_INCLUDE_DIRS
#   COVIS_DEFINITIONS
#   INSTALL_DIR_INC
#   INSTALL_DIR_LIB
#   INSTALL_DIR_BIN
# 
# This script updates COVIS_LIBRARIES and COVIS_LIBRARIES_BUILD to include the main library
#


##
# Check variables
##
if(NOT COVIS_HEADERS)
  message(FATAL_ERROR "COVIS_HEADERS not defined!")
endif(NOT COVIS_HEADERS)
if(NOT COVIS_SOURCES)
  message(FATAL_ERROR "COVIS_SOURCES not defined!")
endif(NOT COVIS_SOURCES)
if(NOT COVIS_LIBRARY)
  message(FATAL_ERROR "COVIS_LIBRARY not defined!")
endif(NOT COVIS_LIBRARY)
if(NOT COVIS_LIBRARIES)
  message(FATAL_ERROR "COVIS_LIBRARIES not defined!")
endif(NOT COVIS_LIBRARIES)
if(NOT COVIS_INCLUDE_DIRS)
  message(FATAL_ERROR "COVIS_INCLUDE_DIRS not defined!")
endif(NOT COVIS_INCLUDE_DIRS)
if(NOT COVIS_DEFINITIONS)
  message(FATAL_ERROR "COVIS_DEFINITIONS not defined!")
endif(NOT COVIS_DEFINITIONS)
if(NOT INSTALL_DIR_INC)
  message(FATAL_ERROR "INSTALL_DIR_INC not defined!")
endif(NOT INSTALL_DIR_INC)
if(NOT INSTALL_DIR_LIB)
  message(FATAL_ERROR "INSTALL_DIR_LIB not defined!")
endif(NOT INSTALL_DIR_LIB)
if(NOT INSTALL_DIR_BIN)
  message(FATAL_ERROR "INSTALL_DIR_BIN not defined!")
endif(NOT INSTALL_DIR_BIN)

##
# Set include/library dirs for build
##
set(COVIS_INCLUDE_DIRS_BUILD ${COVIS_INCLUDE_DIRS} ${CMAKE_SOURCE_DIR}/src)
set(COVIS_LIBRARY_DIRS_BUILD ${COVIS_LIBRARY_DIRS} ${CMAKE_LIBRARY_OUTPUT_DIRECTORY})

##
# Add extra includes and definitions to build, add OpenMP seperately
##
include_directories(${COVIS_INCLUDE_DIRS_BUILD})
link_directories(${COVIS_LIBRARY_DIRS_BUILD})
add_definitions(${COVIS_DEFINITIONS})
add_definitions(${COVIS_DEFINITIONS_EXTRA})
if(COVIS_USE_OPENMP)
  add_definitions(${OpenMP_CXX_FLAGS})
endif(COVIS_USE_OPENMP)

##
# Create main library
##
if(COVIS_BUILD_STATIC)
  add_library(${COVIS_LIBRARY} STATIC ${COVIS_SOURCES} ${COVIS_HEADERS})
else(COVIS_BUILD_STATIC)
  add_library(${COVIS_LIBRARY} SHARED ${COVIS_SOURCES} ${COVIS_HEADERS})
endif(COVIS_BUILD_STATIC)

##
# Link main library to deps, add OpenMP seperately
##
if(COVIS_USE_OPENMP)
  # TODO: This should not be necessary!
  if(COVIS_COMPILER STREQUAL "GCC")
    target_link_libraries(${COVIS_LIBRARY} ${COVIS_LIBRARIES} gomp)
  elseif(COVIS_COMPILER STREQUAL "CLANG")
    target_link_libraries(${COVIS_LIBRARY} ${COVIS_LIBRARIES} omp)
  endif(COVIS_COMPILER STREQUAL "GCC")
else(COVIS_USE_OPENMP)
  target_link_libraries(${COVIS_LIBRARY} ${COVIS_LIBRARIES})
endif(COVIS_USE_OPENMP)

##
# Link examples
##
set(exlist "")
foreach(extarget ${COVIS_EXAMPLES})
  add_executable(example_${extarget} EXCLUDE_FROM_ALL ${CMAKE_CURRENT_SOURCE_DIR}/example/${extarget}/${extarget}.cpp)
  add_dependencies(example_${extarget} ${COVIS_LIBRARY})
  target_link_libraries(example_${extarget} ${COVIS_LIBRARY})
  list(APPEND exlist example_${extarget})
endforeach()
add_custom_target(example DEPENDS ${exlist})

##
# Install headers
##
install(DIRECTORY ${CMAKE_SOURCE_DIR}/src/ DESTINATION ${INSTALL_DIR_INC}/
        FILES_MATCHING PATTERN "*.h")
install(DIRECTORY ${CMAKE_SOURCE_DIR}/src/ DESTINATION ${INSTALL_DIR_INC}/
        FILES_MATCHING PATTERN "*.hpp")
        
##
# Install main library
##
install(TARGETS ${COVIS_LIBRARY}
        RUNTIME DESTINATION ${INSTALL_DIR_BIN}
        LIBRARY DESTINATION ${INSTALL_DIR_LIB}
        ARCHIVE DESTINATION ${INSTALL_DIR_LIB}
        COMPONENT ${COVIS_LIBRARY})

##
# Install Python bindings
##
if(COVIS_BUILD_PYTHON)
  add_library(covis_python_common SHARED ${CMAKE_CURRENT_SOURCE_DIR}/python/covis_python_common.cpp ${CMAKE_CURRENT_SOURCE_DIR}/python/covis_python_common.h)
  file(MAKE_DIRECTORY ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/covis)
  configure_file(${CMAKE_CURRENT_SOURCE_DIR}/python/__init__.py ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/covis)
  foreach(mod ${COVIS_MODULES_PYTHON})
    add_library(covis_python_${mod} SHARED ${CMAKE_CURRENT_SOURCE_DIR}/python/${mod}.cpp)
    target_link_libraries(covis_python_${mod} ${COVIS_LIBRARY} covis_python_common)
    set_target_properties(covis_python_${mod} PROPERTIES PREFIX "" OUTPUT_NAME ${mod} LIBRARY_OUTPUT_DIRECTORY ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/covis)

    list(APPEND COVIS_LIBRARIES_PYTHON covis_python_${mod})
  endforeach(mod)

  add_custom_target(covis_python DEPENDS ${COVIS_LIBRARIES_PYTHON})

  install(TARGETS covis_python_common LIBRARY DESTINATION ${INSTALL_DIR_LIB})
  install(TARGETS ${COVIS_LIBRARIES_PYTHON} LIBRARY DESTINATION ${INSTALL_DIR_LIB}/covis)
endif(COVIS_BUILD_PYTHON)

##
# Get the full file name of the main library
##
if(COVIS_BUILD_STATIC)
  set(COVIS_LIB_FULLFILE ${CMAKE_STATIC_LIBRARY_PREFIX}${COVIS_LIBRARY}${CMAKE_STATIC_LIBRARY_SUFFIX})
else(COVIS_BUILD_STATIC)
  set(COVIS_LIB_FULLFILE ${CMAKE_SHARED_LIBRARY_PREFIX}${COVIS_LIBRARY}${CMAKE_SHARED_LIBRARY_SUFFIX})
endif(COVIS_BUILD_STATIC)

##
# Include main library and its paths in state
##
set(COVIS_LIBRARIES_BUILD ${COVIS_LIBRARIES})
if(COVIS_BUILD_STATIC)
  set(COVIS_LIBRARIES_BUILD ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/${COVIS_LIB_FULLFILE} ${COVIS_LIBRARIES_BUILD})
  set(COVIS_LIBRARIES ${CMAKE_INSTALL_PREFIX}/${INSTALL_DIR_LIB}/${COVIS_LIB_FULLFILE} ${COVIS_LIBRARIES})
else(COVIS_BUILD_STATIC)
  list(APPEND COVIS_LIBRARIES_BUILD ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/${COVIS_LIB_FULLFILE})
  list(APPEND COVIS_LIBRARIES ${CMAKE_INSTALL_PREFIX}/${INSTALL_DIR_LIB}/${COVIS_LIB_FULLFILE})
endif(COVIS_BUILD_STATIC)
list(APPEND COVIS_INCLUDE_DIRS ${CMAKE_INSTALL_PREFIX}/${INSTALL_DIR_INC})
list(APPEND COVIS_LIBRARY_DIRS ${CMAKE_INSTALL_PREFIX}/${INSTALL_DIR_LIB})


##
# Touch up state variables
##
string(REPLACE "/;" ";" COVIS_INCLUDE_DIRS "${COVIS_INCLUDE_DIRS}")
string(REPLACE "/;" ";" COVIS_INCLUDE_DIRS_BUILD "${COVIS_INCLUDE_DIRS_BUILD}")
list(REMOVE_DUPLICATES COVIS_INCLUDE_DIRS)
list(REMOVE_DUPLICATES COVIS_INCLUDE_DIRS_BUILD)

string(REPLACE "/;" ";" COVIS_LIBRARY_DIRS "${COVIS_LIBRARY_DIRS}")
string(REPLACE "/;" ";" COVIS_LIBRARY_DIRS_BUILD "${COVIS_LIBRARY_DIRS_BUILD}")
list(REMOVE_DUPLICATES COVIS_LIBRARY_DIRS)
list(REMOVE_DUPLICATES COVIS_LIBRARY_DIRS_BUILD)

string(REPLACE "debug;" "" COVIS_LIBRARIES "${COVIS_LIBRARIES}")
string(REPLACE "optimized;" "" COVIS_LIBRARIES "${COVIS_LIBRARIES}")
string(REPLACE "general;" "" COVIS_LIBRARIES "${COVIS_LIBRARIES}")
list(REMOVE_DUPLICATES COVIS_LIBRARIES)

string(REPLACE "debug;" "" COVIS_LIBRARIES_BUILD "${COVIS_LIBRARIES_BUILD}")
string(REPLACE "optimized;" "" COVIS_LIBRARIES_BUILD "${COVIS_LIBRARIES_BUILD}")
string(REPLACE "general;" "" COVIS_LIBRARIES_BUILD "${COVIS_LIBRARIES_BUILD}")
list(REMOVE_DUPLICATES COVIS_LIBRARIES_BUILD)

string(REPLACE " " ";" COVIS_DEFINITIONS "${COVIS_DEFINITIONS}")
list(REMOVE_DUPLICATES COVIS_DEFINITIONS)

##
# Propagate state variables to cache
##
set(COVIS_INCLUDE_DIRS ${COVIS_INCLUDE_DIRS} CACHE STRING "CoViS include directories" FORCE)
set(COVIS_INCLUDE_DIRS_BUILD ${COVIS_INCLUDE_DIRS_BUILD} CACHE STRING "CoViS include directories for build" FORCE)
set(COVIS_LIBRARY_DIRS ${COVIS_LIBRARY_DIRS} CACHE STRING "CoViS library directories" FORCE)
set(COVIS_LIBRARY_DIRS_BUILD ${COVIS_LIBRARY_DIRS_BUILD} CACHE STRING "CoViS library directories for build" FORCE)
set(COVIS_LIBRARIES ${COVIS_LIBRARIES} CACHE STRING "CoViS libraries" FORCE)
set(COVIS_LIBRARIES_BUILD ${COVIS_LIBRARIES_BUILD} CACHE STRING "CoViS libraries" FORCE)
set(COVIS_DEFINITIONS ${COVIS_DEFINITIONS} CACHE STRING "CoViS definitions" FORCE)
mark_as_advanced(COVIS_INCLUDE_DIRS COVIS_INCLUDE_DIRS_BUILD
                 COVIS_LIBRARY_DIRS COVIS_LIBRARY_DIRS_BUILD
                 COVIS_LIBRARIES COVIS_LIBRARIES_BUILD
                 COVIS_DEFINITIONS)

##
# Create the config script
##
string(TOUPPER ${COVIS_LIBRARY} COVIS_LIBRARY_UPPER)
include(CMakePackageConfigHelpers)
configure_package_config_file(${CMAKE_SOURCE_DIR}/cmake/${COVIS_LIBRARY_UPPER}Config.cmake.in
                              lib/cmake/${COVIS_LIBRARY_UPPER}/${COVIS_LIBRARY_UPPER}Config.cmake
                              INSTALL_DESTINATION ${CMAKE_BINARY_DIR})
write_basic_package_version_file(${CMAKE_BINARY_DIR}/lib/cmake/${COVIS_LIBRARY_UPPER}/${COVIS_LIBRARY_UPPER}ConfigVersion.cmake
                                 VERSION ${COVIS_VERSION}
                                 COMPATIBILITY AnyNewerVersion)

##
# Create and install the find script
##
configure_file(${CMAKE_SOURCE_DIR}/cmake/Find${COVIS_LIBRARY_UPPER}.cmake.in
               ${CMAKE_BINARY_DIR}/cmake/Find${COVIS_LIBRARY_UPPER}.cmake
               @ONLY)
install(FILES ${CMAKE_BINARY_DIR}/cmake/Find${COVIS_LIBRARY_UPPER}.cmake
        DESTINATION ${INSTALL_DIR_CMAKE}/
        COMPONENT ${COVIS_LIBRARY})

##
# Setup uninstallation - also removes the whole directory of includes and docs
##
set(UNINSTALL_DIRS ${CMAKE_INSTALL_PREFIX}/${INSTALL_DIR_INC}/${COVIS_LIBRARY}
                   ${CMAKE_INSTALL_PREFIX}/${INSTALL_DIR_DOC}/${COVIS_LIBRARY})
configure_file("${CMAKE_SOURCE_DIR}/cmake/cmake_uninstall.cmake.in"
               "${CMAKE_BINARY_DIR}/cmake_uninstall.cmake"
               IMMEDIATE @ONLY)
add_custom_target(uninstall COMMAND ${CMAKE_COMMAND} -P ${CMAKE_BINARY_DIR}/cmake_uninstall.cmake)
