// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <covis/covis.h>
using namespace covis;

/*
 * Main entry point
 */
int main(int argc, const char** argv) {
    // Setup program options
    core::ProgramOptions po;
    
    po.addPositional("root-path", "root path of your dataset");
    
    po.addOption("object-dir", 'o', "objects", "subdirectory for the object models");
    po.addOption("scene-dir", 's', "scenes", "subdirectory for the scene models");
    po.addOption("pose-dir", 'p', "ground_truth", "subdirectory for the ground truth pose models");
    po.addOption("object-ext", ".pcd", "object file extension");
    po.addOption("scene-ext", ".pcd", "scene file extension");
    po.addOption("pose-ext", ".txt", "pose file extension");
    po.addOption("pose-sep", "-", "pose file separator");
    po.addOption("object-regex", "", "set this option to use a regular expression search when collecting object files");
    po.addOption("scene-regex", "", "set this option to use a regular expression search when collecting scene files");
    
    if(!po.parse(argc, argv))
        return 0;
    
    // Instantiate the dataset loader
    util::DatasetLoader dl(
            po.getValue("root-path"),
            po.getValue("object-dir"),
            po.getValue("scene-dir"),
            po.getValue("pose-dir"),
            po.getValue("object-ext"),
            po.getValue("scene-ext"),
            po.getValue("pose-ext"),
            po.getValue("pose-sep")
    );
    dl.setRegexObject(po.getValue("object-regex"));
    dl.setRegexScene(po.getValue("scene-regex"));
    
    // Resolve all file names
    dl.parse();
    
    // Print some global info for the dataset
    COVIS_MSG_INFO("Object paths:");
    core::print(dl.getObjectPaths(), std::cout, "", "", "\n", "\n\n");
    COVIS_MSG_INFO("Object labels:");
    core::print(dl.getObjectLabels(), std::cout, "", "", "\n", "\n\n");
    COVIS_MSG_INFO("Scene paths:");
    core::print(dl.getScenePaths(), std::cout, "", "", "\n", "\n\n");
    COVIS_MSG_INFO("Scene labels:");
    core::print(dl.getSceneLabels(), std::cout, "", "", "\n", "\n\n");
    COVIS_MSG_INFO("Ground truth paths:");
    core::print(dl.getPosePaths(), std::cout, "", "", "\n", "\n\n");

    // Short version
    COVIS_MSG(dl);
    
    // Get and print available data for first scene
    util::DatasetLoader::SceneEntry scene = dl.at(0);
    COVIS_MSG(scene);
    
    // Show the ground truth poses
    visu::DetectionVisu<pcl::PointXYZRGBA> dv;
    dv.setTitle(scene.label);
    dv.setQueries(scene.objectsAs<pcl::PointXYZRGBA>());
    dv.setTarget(scene.sceneAs<pcl::PointXYZRGBA>());
    dv.setPoses(scene.poses);
    dv.show();
    
    return 0;
}
