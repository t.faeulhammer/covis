// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <covis/covis.h>
using namespace covis;

// Point and feature types
typedef pcl::PointXYZRGBNormal PointT;

// Loaded point clouds and/or meshes
std::vector<pcl::PolygonMeshPtr> queryMesh;
std::vector<pcl::PointCloud<PointT>::Ptr> querySurf, queryCloud;
pcl::PolygonMeshPtr targetMesh;
pcl::PointCloud<PointT>::Ptr targetSurf, targetCloud;

/*
 * Main entry point
 */
int main(int argc, const char **argv) {
    // Setup program options
    core::ProgramOptions po;
    po.addPositional("query", "point cloud or mesh file(s) for object model(s)");
    po.addPositional("target", "point cloud or mesh file(s) for scene model(s)");

    // Surfaces and normals
    po.addOption("query-scale", 1,
                 "sometimes object models are given in other units, e.g. [mm] - use this value to scale the coordinates");
    po.addOption("resolution-surface", 'r', 0,
                 "downsample point clouds to this resolution (<= 0 for the average object resolution)");
    po.addOption("far", 0, "do not consider target points beyond this depth (0 for disabled)");
    po.addFlag("remove-table", "remove the dominant plane from the scene");
    po.addOption("radius-normal", 'n', 5, "normal estimation radius in mr (<= 0 means 5 resolution units)");
    po.addFlag('o', "orient-query-normals", "ensure consistent normal orientation for the query model");
    po.addFlag("concave", "set this flag to assume non-convex query models during normal correction");
    po.addOption("remove-planar-structures", 0,
                 "set to a value between 0 and 1 to remove planar structures with this as a lower curvature threshold");

    // Features and matching
    po.addOption("resolution-query", 5, "resolution of query features in mr (<= 0 for two resolution units)");
    po.addOption("resolution-target", 5, "resolution of target features in mr (<= 0 for two resolution units)");
    po.addOption("feature", "si",
                 "name the features to compute - possible names are: " + feature::FeatureNames);
    po.addOption("radius-feature", 'f', 0.125,
                 "feature estimation radius as a fraction of the average object model diagonal - use zero for automatic feature radius");
    po.addOption("pca", 1, "use this fraction of the PCA energy (0 to 1) to reduce the feature dimension and speed up the search");
    po.addOption("trees", 4, "set the number of trees for approximate high-dimensional feature search");
    po.addOption("checks", 512, "set the number of checks for approximate high-dimensional feature search");
    po.addOption("knn", 1, "set the number of k-nearest neighbors for approximate high-dimensional feature search");

    // Estimation
    po.addOption("detector", "ransac", "choose detector: ransac or voting");
    po.addOption("inlier-threshold", 't', 5, "Euclidean inlier threshold (<= 0 for infinite)");
    po.addOption("inlier-fraction", 'a', 0.01, "inlier fraction required for accepting a RANSAC pose");
    po.addOption("bandwidth-translation", -1, "absolute translational bandwidth (if <= 0, use 10 x resolution) for voting");
    po.addOption("bandwidth-rotation", 22.5, "absolute rotational bandwidth [deg] between 0 and 180 for voting");
    po.addOption("tessellation", 6, "rotational tessellation [deg] for voting");
    po.addOption("kde-threshold", 0, "upper KDE threshold for voting (<= 0 means infinite)");
    po.addOption("cutoff", 100, "use the <cutoff> % best correspondences for pose estimation");
    po.addOption("iterations-ransac", 'i', 1000, "RANSAC iterations");
    po.addFlag('u', "full-evaluation",
               "enable full pose evaluation during RANSAC, otherwise only the existing feature matches are used during verification");
    po.addOption("multi-instance", 1, "number of instances to search for");
    po.addOption("iterations-icp", 50, "number of pose refinement iterations");
    po.addOption("verification", "greedy", "use one of none,greedy,conflict,global to remove bad detections");

    // Misc.
    po.addFlag('v', "verbose", "show additional information");
    po.addFlag('z', "visualize", "show visualizations");
    po.addFlag("save-poses", "save poses");
    po.addFlag("save-scores", "save scores (KDE for voting and 1-penalty for RANSAC)");

    // Parse
    if(!po.parse(argc, argv))
        return 1;

    // Misc.
    const bool verbose = po.getFlag("verbose");
    const bool visualize = po.getFlag("visualize");
    const bool savePoses = po.getFlag("save-poses");
    const bool saveScores = po.getFlag("save-scores");

    if(verbose)
        po.print();

    // Load object models
    const std::vector<std::string> queries = po.getVector("query");

    querySurf.resize(queries.size());
    queryMesh.resize(queries.size());
    bool hasQueryMesh = true;
    for(size_t i = 0; i < queries.size(); ++i) {
        const std::string fquery = queries[i];
        COVIS_MSG_INFO("Loading query surface " << i << " from file " << core::filename(fquery) << "...");
        queryMesh[i].reset(new pcl::PolygonMesh);
        util::load(fquery, *queryMesh[i]);
        hasQueryMesh &= (queryMesh[i]->polygons.size() > 0);
    }

    // Surface scaling
    const float queryScale = po.getValue<float>("query-scale");
    float resolution = po.getValue<float>("resolution-surface");
    const bool resolutionInput = (resolution > 0);
    if(!resolutionInput)
        resolution = 0;
    float diag = 0;
    for(size_t i = 0; i < queries.size(); ++i) {
        if(!resolutionInput)
            resolution += detect::computeResolution(queryMesh[i]) * queryScale;
        diag += detect::computeDiagonal(queryMesh[i]) * queryScale;
    }
    if(!resolutionInput)
        resolution /= float(queries.size());
    diag /= float(queries.size());

    const float far = po.getValue<float>("far");
    const bool removeTable = po.getFlag("remove-table");
    const float nrad =
            po.getValue<float>("radius-normal") > 0.0 ?
            po.getValue<float>("radius-normal") * resolution :
            5 * resolution;
    const bool orientQueryNormals = po.getFlag("orient-query-normals");
    const bool concave = po.getFlag("concave");
    const float removePlanarThres = po.getValue<float>("remove-planar-structures");

    // Features and matching
    const float resQuery =
            po.getValue<float>("resolution-query") > 0.0 ?
            po.getValue<float>("resolution-query") * resolution :
            5 * resolution;
    const float resTarget =
            po.getValue<float>("resolution-target") > 0.0 ?
            po.getValue<float>("resolution-target") * resolution :
            5 * resolution;
    std::vector<std::string> features = po.getVector("feature");
    COVIS_ASSERT(!features.empty());
    std::vector<float> frad = po.getVector<float>("radius-feature");
    if(frad.empty()) {
        if(verbose)
            COVIS_MSG_INFO("No feature radius specified - using automatic radius for all features");
        frad.assign(features.size(), 0);
    } else {
        if(frad.size() == 1 && features.size() > 1) {
            COVIS_MSG_INFO("Using the same radius (" << frad[0] * diag << ") for all features...");
            frad.assign(features.size(), frad[0]);
        } else if(frad.size() > 1 && features.size() == 1) {
            COVIS_MSG_INFO(
                    "Using multi-scale fusion (" << frad.size() << " scales) for the same feature (" << features[0]
                                                 << ")...");
            features.assign(frad.size(), features[0]);
        }

        for(size_t i = 0; i < frad.size(); ++i)
            frad[i] = (frad[i] > 0 ? frad[i] * diag : 0);
    }
    COVIS_ASSERT_MSG(frad.size() == features.size(), "You must specify as many feature radii as features!");

    const double pcaVariation = po.getValue<double>("pca");
    const size_t trees = po.getValue<size_t>("trees");
    const size_t checks = po.getValue<size_t>("checks");
    const size_t knn = po.getValue<size_t>("knn");

    // Estimation
    const std::string detector = po.getValue("detector");
    const float inlierThreshold =
            (po.getValue<float>("inlier-threshold") > 0.0 ?
             po.getValue<float>("inlier-threshold") * resolution :
             5 * resolution);
    const float inlierFraction = po.getValue<float>("inlier-fraction");
    double bwt = po.getValue<double>("bandwidth-translation");
    if(bwt <= 0)
        bwt = 10 * resolution;
    double bwrot = po.getValue<double>("bandwidth-rotation");
    double tes = po.getValue<double>("tessellation");
    COVIS_ASSERT(bwrot > 0 && bwrot <= 180);
    COVIS_ASSERT(tes > 0 && tes < 360);
    bwrot *= M_PI / 180.0;
    tes *= M_PI / 180.0;
    const double kdeThres = po.getValue<double>("kde-threshold");
    const size_t cutoff = po.getValue<size_t>("cutoff");
    COVIS_ASSERT(cutoff > 0 && cutoff <= 100);
    const size_t ransacIterations = po.getValue<size_t>("iterations-ransac");
    const bool fullEvaluation = po.getFlag("full-evaluation");
    const size_t multiInstance = po.getValue<size_t>("multi-instance");
    const size_t iterationsIcp = po.getValue<size_t>("iterations-icp");
    const std::string verification = po.getValue("verification");

    /*
     * Preprocess objects
     */
    {
        core::ScopedTimer::Ptr t;
        if(verbose)
            t.reset(new core::ScopedTimer("Preprocessing"));

        for(size_t i = 0; i < queries.size(); ++i) {
            if(verbose)
                COVIS_MSG_INFO("\t" << queries[i]);
            querySurf[i] = filter::preprocess<PointT>(queryMesh[i], queryScale, true, 0, resolution, nrad, orientQueryNormals, concave);
            COVIS_ASSERT(!querySurf[i]->empty());
        }

        // Automatically determine feature radius
        for(size_t i = 0; i < features.size(); ++i) {
            if(frad[i] <= 0) {
                std::vector<float> radii(querySurf.size());
                for(size_t j = 0; j < querySurf.size(); ++j)
                    radii[j] = feature::optimizeFeatureRadius<PointT>(querySurf[j], features[i], 100, 0, 0, 0,
                                                                      (pcaVariation < 1 ? pcaVariation : 0.95),
                                                                      verbose);
                frad[i] = core::mean(radii);
                if(verbose)
                    COVIS_MSG_INFO("Final averaged automatic feature radius for " << features[i] << ": " << frad[i]);
            }
        }

        // Generate feature points
        queryCloud.resize(querySurf.size());
        for(size_t i = 0; i < queries.size(); ++i) {
            queryCloud[i] = filter::downsample<PointT>(querySurf[i], resQuery);

            if(removePlanarThres > 0) {
                if(verbose)
                    COVIS_MSG_INFO("Removing planar structures with a radius of " << frad[0] << "...");
                filter::removePlanarStructures<PointT>(queryCloud[i], frad[0], removePlanarThres);
            }

            COVIS_ASSERT(!queryCloud[i]->empty());
        }
    }

    /*
     * Compute object features, PCA project and index
     */
    feature::MatrixVecT queryFeat(features.size());
    std::vector<core::PCA> pca(features.size());
    std::vector<detect::EigenSearch<feature::MatrixT,flann::L2<float> > > querySearch(features.size());
    {
        core::ScopedTimer::Ptr t;
        if(verbose) {
            t.reset(new core::ScopedTimer("Object features"));
            COVIS_MSG_INFO("Feature names:");
            core::print(features);
            COVIS_MSG_INFO("Feature radii:");
            core::print(frad);
            COVIS_MSG_INFO("Total number of object features:");
            size_t ftotal = 0;
            for(size_t i = 0; i < queryCloud.size(); ++i)
                ftotal += queryCloud[i]->size();
            COVIS_MSG(ftotal);
        }

        for(size_t i = 0; i < features.size(); ++i) {
            queryFeat[i] = feature::computeFeature<PointT>(features[i], queryCloud, querySurf, frad[i]);

            if(pcaVariation < 1) {
                if(verbose)
                    COVIS_MSG("\tUsing PCA to reduce the dimension of " << features[i] << "...");
                pca[i] = core::pcaTrain(queryFeat[i], pcaVariation);

                queryFeat[i] = core::pcaProject(queryFeat[i], pca[i]);

                if(verbose)
                    COVIS_MSG("\t\t" << pca[i].first.size() << " --> " << queryFeat[i].rows());
            }

            querySearch[i].setTrees(trees);
            querySearch[i].setChecks(checks);
            querySearch[i].setTarget(queryFeat[i]);
        }
    }

    // Prepare for recognition
    detect::Recognition<PointT> rec;
    if(boost::iequals(detector, "ransac")) {
        core::randgen.seed(std::time(0));
        rec.setDetector(detect::Recognition<PointT>::RANSAC);
        rec.setRansacIterations(ransacIterations);
        rec.setFullEvaluation(fullEvaluation);
    } else if(boost::iequals(detector, "voting")) {
        rec.setDetector(detect::Recognition<PointT>::VOTING);
        rec.setTranslationBandwidth(bwt);
        rec.setRotationBandwidth(bwrot);
        rec.setTessellation(tes);
        rec.setKdeThreshold(kdeThres);
    } else {
        COVIS_THROW("Unknown detector: " << detector);
    }
    rec.setSources(queryCloud);
    std::vector<std::string> labels(queries.size());
    for(size_t i = 0; i < queries.size(); ++i)
        labels[i] = core::stem(queries[i]);
    rec.setSourceNames(labels);
    rec.setCorrespondenceFraction(double(cutoff) / 100.0);
    rec.setInlierThreshold(inlierThreshold);
    rec.setInlierFraction(inlierFraction);
    rec.setMultiInstance(multiInstance);
    rec.setRefinementIterations(iterationsIcp);
    if(boost::iequals(verification, "none"))
        rec.setVerifier(detect::Recognition<PointT>::NONE);
    else if(boost::iequals(verification, "greedy"))
        rec.setVerifier(detect::Recognition<PointT>::GREEDY);
    else if(boost::iequals(verification, "conflict"))
        rec.setVerifier(detect::Recognition<PointT>::CONFLICT);
    else if(boost::iequals(verification, "global"))
        rec.setVerifier(detect::Recognition<PointT>::GLOBAL);
    else
        COVIS_THROW("Unknown verification method: " << verification << "!");
//    rec.setSourcesVerification(querySurf);
    rec.setVerbose(verbose);

    /*
     * Run
     */
    std::vector<std::string> targets = po.getVector("target");
    for(size_t i = 0; i < targets.size(); ++i) {
        // Load
        targetMesh.reset(new pcl::PolygonMesh);
        if(verbose)
            COVIS_MSG_INFO("Loading target surface from file " << core::filename(targets[i]) << "...");
        util::load(targets[i], *targetMesh);
        const bool hasTargetMesh = (targetMesh->polygons.size() > 0);

        // Start timer
        core::ScopedTimer::Ptr t;
        if(verbose)
            t.reset(new core::ScopedTimer("Recognition, scene " +
                                          core::stringify(i + 1) + "/" + core::stringify(targets.size()) +
                                          " (" + core::filename(targets[i]) + ")"));

        targetSurf = filter::preprocess<PointT>(targetMesh, 1, true, far, resolution, nrad, false, false);

        if(removeTable) {
            const size_t sz = targetSurf->size();
            targetSurf = filter::removeDominantPlane<PointT>(targetSurf);
            if(verbose)
                COVIS_MSG_INFO("Removed " << sz - targetSurf->size() << "/" << sz << " table points from the scene!");
        }

        if(targetSurf->empty()) {
            COVIS_MSG_WARN("No target points left after preprocessing! Skipping scene...");
            continue;
        }

        // Generate feature points
        targetCloud = filter::downsample<PointT>(targetSurf, resTarget);

        if(removePlanarThres > 0) {
            if(verbose)
                COVIS_MSG_INFO("Removing planar structures with a radius of " << frad[0] << "...");
            filter::removePlanarStructures<PointT>(targetCloud, frad[0], removePlanarThres);
        }

        COVIS_ASSERT(!targetCloud->empty());

        if(verbose) {
            t->intermediate("preprocessing");
            COVIS_MSG_INFO("Feature names:");
            core::print(features);
            COVIS_MSG_INFO("Feature radii:");
            core::print(frad);
        }

        feature::MatrixVecT targetFeat(features.size());
        for(size_t j = 0; j < features.size(); ++j) {
            targetFeat[j] = feature::computeFeature<PointT>(features[j], targetCloud, targetSurf, frad[j]);

            if(pcaVariation < 1)
                targetFeat[j] = core::pcaProject(targetFeat[j], pca[j]);
        }

        if(verbose)
            t->intermediate(core::stringify(targetCloud->size()) + " features");

        // Perform feature matching
        std::vector<core::Correspondence::VecPtr> ratioCorr(features.size());
        if(verbose && features.size() > 1)
            COVIS_MSG_WARN("Multiple features for fusion enabled - ignoroing k-NN parameter and using 2-NN ratio...");
        for(size_t j = 0; j < features.size(); ++j) {
            if(features.size() > 1)
                ratioCorr[j] = querySearch[j].ratio(targetFeat[j]);
            else
                ratioCorr[j] = core::flatten(*querySearch[j].knn(targetFeat[j], knn));
        }

        core::Correspondence::VecPtr fusionCorr;
        if(features.size() > 1)
            fusionCorr = detect::computeFusionMatches(ratioCorr);
        else
            fusionCorr = ratioCorr[0];

        if(verbose)
            t->intermediate("matching " + core::stringify(targetFeat[0].cols()) + " --> " + core::stringify(queryFeat[0].cols()) + " features");

        // Estimate
        rec.setTarget(targetCloud);
        rec.setFeatureCorrespondences(*fusionCorr);

        core::Detection::Vec detections = rec.recognize();

        // Show and save
        if(detections.empty()) {
            COVIS_MSG_WARN("No objects found!");
        } else {
            // Remove any existing pose files for this scene and print detections
            COVIS_MSG_INFO("Got the following detections:");
            std::vector<std::string> posefiles(detections.size());
            std::vector<std::string> scorefiles(detections.size());
            for(size_t j = 0; j < detections.size(); ++j) {
                COVIS_MSG(detections[j]);

                posefiles[j] = detections[j].label + "-" + core::stem(targets[i]) + ".txt";
                if(savePoses && boost::filesystem::is_regular_file(boost::filesystem::path(posefiles[j])))
                    boost::filesystem::remove(boost::filesystem::path(posefiles[j]));

                scorefiles[j] = detections[j].label + "-" + core::stem(targets[i]) + "-scores.txt";
                if(saveScores && boost::filesystem::is_regular_file(boost::filesystem::path(scorefiles[j])))
                    boost::filesystem::remove(boost::filesystem::path(scorefiles[j]));
            }

            // Save poses
            if(savePoses) {
                if(verbose)
                    COVIS_MSG_INFO("Saving " << detections.size() << " poses...");
                for(size_t j = 0; j < detections.size(); ++j) {
                    if(verbose)
                        COVIS_MSG("\tSaving pose " << j + 1 << "/" << detections.size() << " to file " << posefiles[j]
                                                   << "...");
                    util::saveEigen(posefiles[j], detections[j].pose, true, false, true);
                }
            }

            // Save scores
            if(saveScores) {
                if(verbose)
                    COVIS_MSG_INFO("Saving " << detections.size() << " scores...");
                for(size_t j = 0; j < detections.size(); ++j) {
                    if(verbose)
                        COVIS_MSG("\tSaving score " << j + 1 << "/" << detections.size() << " to file " << posefiles[j]
                                                   << "...");

                    Eigen::VectorXf score(1);
                    if(boost::iequals(detector, "ransac"))
                        score(0) = 1 - detections[j].penalty;
                    else
                        score(0) = detections[j].params["kde"];
                    util::saveEigen(scorefiles[j], score, true, false, true);
                }
            }
            t.reset();

            // Show detections
            if(visualize) {
                COVIS_MSG_INFO("Showing detections...");

                if(hasQueryMesh) {
                    if(hasTargetMesh)
                        visu::showDetections(queryMesh, targetMesh, detections,
                                             "Detections in scene " + core::stem(targets[i]));
                    else
                        visu::showDetections<PointT>(queryMesh, targetSurf, detections,
                                                     "Detections in scene " + core::stem(targets[i]));
                } else {
                    visu::showDetections<PointT>(querySurf, targetSurf, detections,
                                                 "Detections in scene " + core::stem(targets[i]));
                }
            }
        }
    }

    return 0;
}
