// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <covis/covis.h>
using namespace covis;

// OpenCV
#include <opencv2/opencv.hpp>
using namespace cv;

using namespace std;

/*
 * Main entry point
 */
int main(int argc, const char** argv) {
    // Setup program options
    core::ProgramOptions po;
    po.addPositional("template", "template image(s)");
    po.addPositional("image", "test image");
    
    po.addOption("scale", 's', 1, "scale all images by this factor to speed up processing");
    po.addFlag('e', "external", "use only external edges for object templates");
    po.addOption("roi", 'r', 0, "set a ROI as <x,y,width,height>");
    po.addOption("ksize", 'k', 5, "set the blur kernel size used before edge extraction (must be >= 3 and odd)");
    po.addOption("low-edge-threshold", 'l', 0, "set the low edge detection threshold, if zero an automatic value will be used");
    po.addOption("high-edge-threshold", 'h', 0, "set the low edge detection threshold, if zero an automatic value will be used");
    po.addFlag('n', "normalize", "set this flag to normalize the test image");
    po.addOption("threshold", 't', 0, "if positive, accept all detections up to this threshold");
    
    // Parse
    if(!po.parse(argc, argv))
        return 1;
    po.print();
    
    const std::vector<std::string> tpath = po.getVector("template");
    const double scale = po.getValue<double>("scale");
    COVIS_ASSERT(scale > 0 && scale <= 1);
    const bool external = po.getFlag("external");
    const std::vector<double> roii = po.getVector<double>("roi");
    Rect roi;
    if(roii.size() != 1) {
        COVIS_ASSERT_MSG(roii.size() == 4, "ROI must  be specified as a 4-vector of <x,y,width,height>!");
        roi = Rect(roii[0], roii[1], roii[2], roii[3]);
    }
    const int ksize = po.getValue<int>("ksize");
    double lowEdgeThreshold = po.getValue<double>("low-edge-threshold");
    double highEdgeThreshold = po.getValue<double>("high-edge-threshold");
    const bool normalizee = po.getFlag("normalize");
    const double threshold = po.getValue<double>("threshold");

    // Training data to be loaded for the 2D matcher
    COVIS_MSG("Loading " << tpath.size() << " templates...");
    std::vector<Mat> templates(tpath.size());
    for(size_t i = 0; i < tpath.size(); ++i) {
        // Get RGB template
        Mat t = imread(tpath[i], IMREAD_UNCHANGED);
        COVIS_ASSERT_MSG(!t.empty(), "Cannot read template image file " << tpath[i] << "!");
        
        if(scale < 1)
            resize(t, t, Size(), scale, scale);
        
        templates[i] = t.clone();
    }
    
    /*
     * Load test image and preprocess it
     */
    Mat img =  imread(po.getValue("image"), IMREAD_UNCHANGED);
    COVIS_ASSERT_MSG(!img.empty(), "Cannot read test image " << po.getValue("image") << "!");
    
    if(scale < 1)
        resize(img, img, Size(), scale, scale);
    
    /*
     * Run template matching
     */
    detect::TemplateMatching tm(img.cols, img.rows);
    if(roi.area() > 0)
        tm.setROI(roi);
    tm.setBlurKernelSize(ksize);
    tm.setNormalize(normalizee);
    tm.setLowEdgeThreshold(lowEdgeThreshold);
    tm.setHighEdgeThreshold(highEdgeThreshold);
    tm.loadTemplates(templates, external);
    tm.loadImage(img);
    {
        core::ScopedTimer t("Template matching");
        size_t id;
        Point pmin;
        float min;
        Mat response;
        tm.matchEdges(id, pmin, min, response);
    }

    // Outputs for all possible matches after suppression
    vector<size_t> vid;
    vector<Point> vpmin;
    vector<float> vmin;
    tm.getAllMatches(vid, vpmin, vmin);
    
    if(threshold > 0) {
        COVIS_MSG("Thresholding " << vid.size() << " detections...");   
        size_t i;
        for(i = 0; i < vid.size(); ++i)
            if(vmin[i] > threshold)
                break;
        vid.resize(i);
    } else {
        vid.resize(1);
    }
    
    if(vid.empty()) {
        COVIS_MSG_WARN("Object not found!");
        return 0;
    }
    
    // Draw detection(s)
    tm.drawEdges(img);
    if(threshold > 0) {
        for(size_t i = 0; i < vid.size(); ++i)
            tm.drawDetection(img, vid[i], vpmin[i], true);
    } else {
        tm.drawDetection(img, vid[0], vpmin[0], true, true);
    }
    imshow("Detection", img);
    waitKey();
    
    return 0;
}
