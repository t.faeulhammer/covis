// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <covis/covis.h>
using namespace covis;

// PCL
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/filter.h>
#include <pcl/io/pcd_io.h>

// Point cloud type
typedef pcl::PointXYZ PointT;
pcl::PointCloud<PointT>::Ptr cloud;

/*
 * Main entry point
 */
int main(int argc, const char** argv) {
    // Setup program options
    core::ProgramOptions po;
    po.addPositional("pcdfile", "point cloud file");
    po.addOption("min-scale", 0.002, "minimum scale (Euclidean radius)");
    po.addOption("octaves", 8, "number of octaves (sqrt2 increases in radius)");
    
    // Parse
    if(!po.parse(argc, argv))
        return 1;
    
    // Get
    const float minScale = po.getValue<float>("min-scale");
    const size_t octaves = po.getValue<size_t>("octaves");
    
    po.print();
    
    // Load point cloud
    cloud.reset(new pcl::PointCloud<PointT>);
    COVIS_ASSERT(pcl::io::loadPCDFile<PointT>(po.getValue("pcdfile"), *cloud) == 0);
    std::vector<int> dummy;
    pcl::removeNaNFromPointCloud(*cloud, *cloud, dummy);
    COVIS_ASSERT(!cloud->empty());
    
    // Compute ID
    pcl::PointCloud<core::Ids>::Ptr result;
    {
        core::ScopedTimer t("Intrinsic dimension 3D");
        filter::IntrinsicDimension3D<PointT, core::Ids> id;
        id.setMinScale(minScale);
        id.setOctaves(octaves);
        result = id.filter(cloud);
    }
    
    // Perform labeling
    std::vector<float> label(result->size());
    for(size_t i = 0; i < result->size(); ++i) {
        const core::Ids& id = (*result)[i];
        label[i] = id.id0 > id.id1 ? (id.id0 > id.id2 ? 0 : 2) : (id.id1 > id.id2 ? 1 : 2);
    }
    
    // Show results
    visu::showScalarField<PointT>(cloud, label, "ID {0, 1, 2}");
    visu::showScalarField<PointT, core::Ids>(cloud, result, "scale", "Scale");
    
    return 0;
}
