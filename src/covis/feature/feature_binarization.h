// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FEATURE_FEATURE_BINARIZATION_H
#define COVIS_FEATURE_FEATURE_BINARIZATION_H

// Own
#include "eigen_feature.h"
#include "feature_base.h"

// Boost
#include <boost/dynamic_bitset.hpp>

namespace covis {
    namespace feature {
        /// Container type for blocks of bits used by binary feature representations
        typedef unsigned char BinaryBlockT;

        /// Binary feature matrix type
        typedef Eigen::Matrix<BinaryBlockT, Eigen::Dynamic, Eigen::Dynamic> BinaryMatrixT;

        /**
         * @class FeatureBinarization
         * @ingroup feature
         * @brief Binarize real-valued features
         *
         * This class implements the method for binarizing features desribed in @cite prakhya2015b.
         * Note that the dimension of the feature must be divisible by 8.
         *
         * @sa @ref detect::EigenSearchBinary
         * @author Anders Glent Buch
         */
        class FeatureBinarization : public FeatureBase {
            public:
                /// Empty constructor
                FeatureBinarization() {}

                /// Empty destructor
                virtual ~FeatureBinarization() {}

                /**
                 * Binarize  a set of features
                 * @param features input features
                 * @return binarized features, encoded byte-wise
                 */
                BinaryMatrixT compute(const MatrixT& features);

            private:
                /**
                 * Binarize a single feature
                 * @param feature feature
                 * @return binarized feature
                 */
                boost::dynamic_bitset<BinaryBlockT> binarize(
                        const Eigen::Matrix<MatrixT::Scalar, Eigen::Dynamic, 1>& feature);
        };

        /**
         * @ingroup feature
         * Binarize a set of features
         * @param features features
         * @return binarized features, encoded byte-wise
         */
        BinaryMatrixT binarizeFeatures(const MatrixT& features);
    }
}

#endif
