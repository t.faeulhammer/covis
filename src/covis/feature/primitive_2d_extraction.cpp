
#include "primitive_2d_extraction.h"

#include "../filter/intrinsic_dimension.h"
#include "../filter/monogenic_signal.h"
#include "../core/line_segment_2d.h"
#include "../core/texlet_2d.h"

//OpenCV
#include <opencv2/imgproc/imgproc.hpp>

using namespace covis;
using namespace covis::feature;


std::pair<std::vector<core::Texlet2D>, std::vector<core::LineSegment2D> >
Primitive2DExtraction::ExtractECVPrimitives(const cv::Mat &image, const cv::Mat &idResult, const core::Ids &ids)
{
    cv::Mat_<cv::Vec3b> img = image;

    // Run monogenic signal filtering
    filter::MonogenicSignal ms;
    cv::Mat result = ms.filter(image);
    // Split into separate images
    std::vector<cv::Mat_<double> > mop;
    cv::split(result, mop);
    cv::Mat_<double> m = mop[0];
    cv::Mat_<double> o = mop[1];
    cv::Mat_<double> p = mop[2];



    std::pair<std::vector<core::Texlet2D>, std::vector<core::LineSegment2D> >  output;

    std::vector<cv::Mat_<double> > i012d;
    cv::split(idResult, i012d);
    for (int j = 0; j < idResult.rows; j++)
    {
        for (int i = 0; i < idResult.cols; i++)
        {
            bool line = true;
            bool texlet = false;
            double _id0 = i012d[0][j][i];
            double _id1 = i012d[1][j][i];
            double _id2 = i012d[2][j][i];

            double energy = 1. - _id0;

            if (energy < ids.id1 || _id2 > ids.id2)
            {
                line = false;
            }

            if (_id1 < ids.id1 && _id2 < ids.id2)
            {
                line = false;
            }

            if (!line && (_id2 > ids.id2 || _id1 > ids.id1)) texlet = true;

            if (line) {
                core::LineSegment2D l2d = core::LineSegment2D(core::Ids(_id0, _id1, _id2), Gradient( m[j][i], o[j][i]));
                l2d.x = i;
                l2d.y = j;
                l2d.r = img[j][i][0];
                l2d.g = img[j][i][1];
                l2d.b = img[j][i][2];
                output.second.push_back(l2d);
            }

            if (texlet) {
                core::Texlet2D tex2d = core::Texlet2D(core::Ids(_id0, _id1, _id2), Gradient( m[j][i], o[j][i]));
                tex2d.x = i;
                tex2d.y = j;
                tex2d.r = img[j][i][0];
                tex2d.g = img[j][i][1];
                tex2d.b = img[j][i][2];
                output.first.push_back(tex2d);
            }
        }
    }
    return output;
}
