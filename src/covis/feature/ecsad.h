// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FEATURE_ECSAD_H
#define COVIS_FEATURE_ECSAD_H

// Own
#include "feature_3d.h"
#include "../core/macros.h"

// PCL
#include <pcl/point_types.h>

// Register the ECSAD feature
POINT_CLOUD_REGISTER_POINT_STRUCT(pcl::Histogram<30>, (float[30],histogram,histogram))

namespace covis {
    namespace feature {
        /// Convenience type for the default NDHist type
        typedef pcl::Histogram<30> ECSADT;

        /**
         * @class ECSAD
         * @ingroup feature
         * @brief Equivalent Circumference Surface Angle Descriptor
         * 
         * This class provides a relatively low-dimensional (30), but highly accurate and efficient local shape feature,
         * which can be used for both local shape description and 3D edge detection.
         * 
         * For more information, see @cite jorgensen2014geometric.
         * 
         * @attention Using the non-const version of @ref compute(typename pcl::PointCloud<PointNT>::Ptr cloud)
         * "compute()" avoids taking an internal copy, but also alters the surface normals of the input
         * 
         * @todo Add training/testing code for edge detection and examples
         *
         * @sa @ref ECSAD, @ref PCLFeature
         *
         * @tparam PointNT input point type, must contain XYZ data and normal fields
         * @author Anders Glent Buch
         */
        template<typename PointNT>
        class ECSAD : public Feature3D<PointNT, pcl::Histogram<30> > {
            using Feature3D<PointNT,pcl::Histogram<30> >::_radius;
            using Feature3D<PointNT,pcl::Histogram<30> >::_surface;

            public:
                /// Convenience typedef for the feature type returned by the class
                typedef pcl::Histogram<30> Histogram;
                
                /// Empty constructor
                ECSAD() {}

                /// Empty destructor
                virtual ~ECSAD() {}
                
                /**
                 * Compute ECSAD descriptors
                 * @param cloud input point cloud, must contain XYZ data and normal fields
                 * @return histograms, L1 normalized (but all zero for invalid points or points with no neighbors)
                 */
                pcl::PointCloud<Histogram>::Ptr compute(typename pcl::PointCloud<PointNT>::ConstPtr cloud) {
                    typename pcl::PointCloud<PointNT>::Ptr cloudMutable(new typename pcl::PointCloud<PointNT>);
                    *cloudMutable = *cloud;
                    
                    return compute(cloudMutable);
                }

                /**
                 * Compute ECSAD descriptors
                 * @note for this overload, the normal fields are altered
                 * @param cloud input point cloud, must contain XYZ data and normal fields
                 * @return histograms, L1 normalized (but all zero for invalid points or points with no neighbors)
                 */
                pcl::PointCloud<Histogram>::Ptr compute(typename pcl::PointCloud<PointNT>::Ptr cloud);

            private:
                /**
                 * Cross product
                 * @param vec1 first vector
                 * @param vec2 second vector
                 * @param res result
                 */
                inline void cross(float *  vec1, float *  vec2, float *  res) {
                   res[0] = vec1[1] * vec2[2] - vec1[2] * vec2[1];
                   res[1] = vec1[2] * vec2[0] - vec1[0] * vec2[2];
                   res[2] = vec1[0] * vec2[1] - vec1[1] * vec2[0];
                }
                
                /**
                 * Three-dimensional dot product
                 * @param vec1 first vector
                 * @param vec2 second vector
                 * @return dot product
                 */
                inline float dot(float* vec1, float* vec2) {
                   return vec1[0] * vec2[0] + vec1[1] * vec2[1] + vec1[2] * vec2[2];
                }
                
                /// Internal parameters for creating ECSAD
                struct ECSADPatch {
                    static const int Slices = 6;
                    static const int Depth = 4;
                    static const int SlicesHalf = Slices / 2;
                    static const int DescSize = Slices * (Depth + 1) * Depth / 2;
                    static const int AngleDescSize = DescSize / 2;

                    float center[3];
                    float orientation[3];
                    float xAx[3];
                    float yAx[3];

                    float desc[DescSize];
                    float angleDesc[AngleDescSize];
                    bool border[DescSize];

                    float primeCurv;
                    float secondaryCurv;
                };
        };
        
        /**
         * @ingroup feature
         * Compute ECSAD features for a point cloud
         * @param cloud input point cloud
         * @param radius search radius
         * @return ECSAD histogram features
         */
        template<typename PointNT>
        inline typename pcl::PointCloud<typename ECSAD<PointNT>::Histogram>::Ptr computeECSAD(
                typename pcl::PointCloud<PointNT>::ConstPtr cloud,
                float radius) {
            ECSAD<PointNT> ecsad;
            ecsad.setRadius(radius);
            return ecsad.compute(cloud);
        }
        
        /**
         * @ingroup feature
         * Compute ECSAD features for a point cloud with an external search surface
         * @param cloud input point cloud
         * @param surface external search surface
         * @param radius search radius
         * @return ECSAD histogram features
         */
        template<typename PointNT>
        inline typename pcl::PointCloud<typename ECSAD<PointNT>::Histogram>::Ptr computeECSAD(
                typename pcl::PointCloud<PointNT>::ConstPtr cloud,
                typename pcl::PointCloud<PointNT>::ConstPtr surface,
                float radius) {
            ECSAD<PointNT> ecsad;
            ecsad.setSurface(surface);
            ecsad.setRadius(radius);
            return ecsad.compute(cloud);
        }
    }
}

#ifndef COVIS_PRECOMPILE
#include "ecsad_impl.hpp"
#endif

#endif
