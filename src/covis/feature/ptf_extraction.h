// Copyright (c) 2016, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FEATURE_PTF_EXTRACTION_H
#define COVIS_FEATURE_PTF_EXTRACTION_H

// Own
#include "feature_3d.h"
#include "../core/ptf.h"

// PCL
#include <pcl/point_types.h>

namespace covis {
    namespace feature {
        /**
         * @class PTFExtraction
         * @ingroup feature
         * @brief Class for computing point triplet features
         *
         * This class provides computation of point triplet feature (PTF) - a 15-dimensional feature,
         * which captures a quite primitive relations between three surface point. PTF is used for
         * known object detection.
         *
         * @todo Add citation when paper is accepted
         *
         * @tparam PointInT input point type, must contain XYZ data and normal fields
         * @author Lilita Kiforenko
         */
        template<typename PointInT>
        class PTFExtraction : public FeatureBase {

        public:
            /// Empty constructor
            PTFExtraction() : _min_distance(0.01), _max_distance(0.1), _debug(false) {}

            /// Empty destructor
            virtual ~PTFExtraction() {}

            /// The minimum Eucledian distance between two points
            inline void setMinDistance(float distance) {
                this->_min_distance = distance;
            }

            /// The maximum Eucledian distance between two points
            inline void setMaximumDistance(float distance) {
                this->_max_distance = distance;
            }

            inline void setDebug(bool debug) {
                this->_debug = debug;
            }

            /**
             * Compute PTFs
             * @param cloud input point cloud, must contain XYZ data and normal fields
             * @param ptf_out  cloud containing PTFs
             * @param ptf_out_indexes indexes for cloud containing PTFs
             * @param number_of_rings limits the amount of point triplets, default val = 10, larger number_of_rings
             * increases precision, but descreases processing speed
             */
            inline void
            compute(typename pcl::PointCloud<PointInT>::ConstPtr cloud, pcl::PointCloud<covis::core::PTF> &ptf_out,
                    pcl::PointCloud<covis::core::PTF_Index> &ptf_out_indexes, int number_of_rings = 10);

        private:
            float _min_distance, _max_distance;
            bool _debug;


        };

    }
}

#ifndef COVIS_PRECOMPILE
#include "ptf_extraction_impl.hpp"
#endif

#endif
