
#ifndef COVIS_FEATURE_PRIMITIVE2DEXTRACTION_H_
#define COVIS_FEATURE_PRIMITIVE2DEXTRACTION_H_

#include "feature_base.h"
#include "../core/texlet_2d.h"
#include "../core/line_segment_2d.h"

#include <stdio.h>

#include <opencv2/imgproc/imgproc.hpp>

namespace covis {
    namespace feature {
        class Primitive2DExtraction : public FeatureBase  {
            public:
                /// Empty constructor
                Primitive2DExtraction() {}
                /// Empty destructor
                virtual ~Primitive2DExtraction() {}

                /**
                 * Extract Texlet2D and LineSegment2D
                 * @param image source image
                 * @param idResult 3-channel double precision image representing intrinsic dimensions (i0D, i1D and i2D)
                 * @param ids doubles representing intrinsic dimensions (i0D, i1D and i2D)
                 * @return pair of vector<Texlet2D> and vector<LineSegment2D>
                 */
                std::pair<std::vector<core::Texlet2D>, std::vector<core::LineSegment2D> >
                ExtractECVPrimitives(const cv::Mat& image, const cv::Mat& idResult, const core::Ids& ids);

        };
    }
}
#endif /* COVIS_FEATURE_PRIMITIVE2DEXTRACTION_H_ */
