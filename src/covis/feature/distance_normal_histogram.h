// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FEATURE_DISTANCE_NORMAL_HISTOGRAM_H
#define COVIS_FEATURE_DISTANCE_NORMAL_HISTOGRAM_H

// Own
#include "feature_3d.h"
#include "../core/macros.h"

// PCL
#include <pcl/point_types.h>

// Register the default NDHist feature
POINT_CLOUD_REGISTER_POINT_STRUCT(pcl::Histogram<128>, (float[128], histogram, histogram))

namespace covis {
    namespace feature {
        /// Convenience type for the default NDHist type
        typedef pcl::Histogram<128> NDHistT;

        /**
         * @class DistanceNormalHistogram
         * @ingroup feature
         * @brief Simple point cloud feature based on 2D histograms of distances vs. relative normal angles
         * (dot products)
         * 
         * This feature resembles the Spin images to a certain degree, and the final descriptor is also "flattened"
         * to a 1D vector for matching purposes. If you want to use these features for matching purposes, remember to
         * register the chosen output histogram type. The default type <b>pcl::Histogram<128></b> is already registered,
         * but if you want to use other histogram binnings, registration is needed like this:
         * 
         * POINT_CLOUD_REGISTER_POINT_STRUCT(pcl::Histogram<NDist * NNorm>, (float[NDist * NNorm], histogram, histogram))
         *
         * @sa @ref ECSAD
         *
         * @tparam PointNT input point type, must contain XYZ+normal data
         * @tparam NDist output histogram dimension along the distance dimension
         * @tparam NNorm output histogram dimension along the normal dimension
         * @author Anders Glent Buch
         */
        template<typename PointNT, int NDist = 8, int NNorm = 16>
        class DistanceNormalHistogram : public Feature3D<PointNT, pcl::Histogram<NDist * NNorm> > {
            using Feature3D<PointNT,pcl::Histogram<NDist * NNorm> >::_radius;
            using Feature3D<PointNT,pcl::Histogram<NDist * NNorm> >::_surface;
            
            public:
                /// Output histogram of this class
                typedef pcl::Histogram<NDist * NNorm> Histogram;
                
                /// Empty constructor
                DistanceNormalHistogram() : _skipNegatives(true) {}

                /// Empty destructor
                virtual ~DistanceNormalHistogram() {}
                
                /**
                 * Compute relative distance vs. normal orientation histograms
                 * @param cloud input point cloud, must contain XYZ and normal data
                 * @return histograms (zero for invalid points or points with no neighbors)
                 */
                virtual typename pcl::PointCloud<Histogram>::Ptr compute(typename pcl::PointCloud<PointNT>::ConstPtr cloud);
                
                /**
                 * Set skip negative flag
                 * @param skipNegatives skip negative flag
                 */
                inline void setSkipNegatives(bool skipNegatives) {
                    _skipNegatives = skipNegatives;
                }
                
            protected:
                /// If set to true, disregard anti-parallel neighbor normals in the histogram building
                bool _skipNegatives;
        };

        /**
         * @ingroup feature
         * 
         * Compute relative distance/normal orientation histograms using @ref DistanceNormalHistogram
         * 
         * @param cloud input point cloud, must contain XYZ and normal data
         * @param radius search radius
         * @param skipNegatives set to true to avoid anti-parallel neighbors in histogram building
         * @tparam NDist output histogram dimension along the distance dimension
         * @tparam NNorm output histogram dimension along the normal dimension
         * @return histograms (zero for invalid points or points with no valid neighbors)
         */
        template<typename PointNT, int NDist, int NNorm>
        inline typename pcl::PointCloud<typename DistanceNormalHistogram<PointNT,NDist,NNorm>::Histogram>::Ptr
        computeDistanceNormalHistogram(typename pcl::PointCloud<PointNT>::ConstPtr cloud,
                float radius,
                bool skipNegatives = true) {
            DistanceNormalHistogram<PointNT, NDist, NNorm> nh;
            nh.setRadius(radius);
            nh.setSkipNegatives(skipNegatives);
            
            return nh.compute(cloud);
        }

        /**
         * @ingroup feature
         * 
         * Compute relative distance/normal orientation histograms using @ref DistanceNormalHistogram
         * based on an external surface
         * 
         * @param cloud input point cloud, must contain XYZ and normal data
         * @param surface external search surface, must contain XYZ and normal data
         * @param radius search radius
         * @param skipNegatives set to true to avoid anti-parallel neighbors in histogram building
         * @tparam NDist output histogram dimension along the distance dimension
         * @tparam NNorm output histogram dimension along the normal dimension
         * @return histograms (zero for invalid points or points with no valid neighbors)
         */
        template<typename PointNT, int NDist, int NNorm>
        inline typename pcl::PointCloud<typename DistanceNormalHistogram<PointNT,NDist,NNorm>::Histogram>::Ptr
        computeDistanceNormalHistogram(typename pcl::PointCloud<PointNT>::ConstPtr cloud,
                typename pcl::PointCloud<PointNT>::ConstPtr surface,
                float radius,
                bool skipNegatives = true) {
            DistanceNormalHistogram<PointNT, NDist, NNorm> nh;
            nh.setRadius(radius);
            nh.setSurface(surface);
            nh.setSkipNegatives(skipNegatives);
            
            return nh.compute(cloud);
        }

        /**
         * @ingroup feature
         *
         * Compute relative distance/normal orientation histograms using @ref DistanceNormalHistogram
         *
         * @param cloud input point cloud, must contain XYZ and normal data
         * @param radius search radius
         * @param skipNegatives set to true to avoid anti-parallel neighbors in histogram building
         * @return histograms (zero for invalid points or points with no valid neighbors)
         */
        template<typename PointNT>
        inline typename pcl::PointCloud<typename DistanceNormalHistogram<PointNT>::Histogram>::Ptr
        computeDistanceNormalHistogram(typename pcl::PointCloud<PointNT>::ConstPtr cloud,
                                       float radius,
                                       bool skipNegatives = true) {
            DistanceNormalHistogram<PointNT> nh;
            nh.setRadius(radius);
            nh.setSkipNegatives(skipNegatives);

            return nh.compute(cloud);
        }

        /**
         * @ingroup feature
         *
         * Compute relative distance/normal orientation histograms using @ref DistanceNormalHistogram
         * based on an external surface
         *
         * @param cloud input point cloud, must contain XYZ and normal data
         * @param surface external search surface, must contain XYZ and normal data
         * @param radius search radius
         * @param skipNegatives set to true to avoid anti-parallel neighbors in histogram building
         * @return histograms (zero for invalid points or points with no valid neighbors)
         */
        template<typename PointNT>
        inline typename pcl::PointCloud<typename DistanceNormalHistogram<PointNT>::Histogram>::Ptr
        computeDistanceNormalHistogram(typename pcl::PointCloud<PointNT>::ConstPtr cloud,
                                        typename pcl::PointCloud<PointNT>::ConstPtr surface,
                                        float radius,
                                        bool skipNegatives = true) {
            DistanceNormalHistogram<PointNT> nh;
            nh.setRadius(radius);
            nh.setSurface(surface);
            nh.setSkipNegatives(skipNegatives);

            return nh.compute(cloud);
        }
    }
}

#include "distance_normal_histogram_impl.hpp"

#endif
