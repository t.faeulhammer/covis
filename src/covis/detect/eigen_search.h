// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_DETECT_EIGEN_SEARCH_H
#define COVIS_DETECT_EIGEN_SEARCH_H

// Own
#include "search_base.h"
#include "../core/correspondence.h"

// PCL
#include <pcl/conversions.h>
#include <pcl/PolygonMesh.h>
#include <pcl/common/common.h>

// Eigen
#include <eigen3/Eigen/Core>

// FLANN
#include <flann/flann.hpp>

namespace covis {
    namespace detect {
        /**
         * @ingroup detect
         * @class EigenSearch
         * @brief Search both low- and high-dimensional data in an Eigen matrix using FLANN's k-d trees
         *
         * Depending on the input data dimension, this class automatically selects a single or multiple randomized
         * k-d trees to do the searh
         * 
         * @note This class returns squared distances and it always casts the input data to floats before indexing
         * @note The default storage order for Eigen matrices is column-major, so this is also assumed by this class,
         * i.e. observations of individual points/features are assumed to lie column-wise in the matrix
         * 
         * @tparam Derived the type of Eigen matrix, defaults to <b>Eigen::MatrixXf</b>
         * @tparam DistT distance type, defaults to <b>flann::L2<float></b>
         * @author Anders Glent Buch
         */
        template<typename Derived=Eigen::MatrixXf, typename DistT=flann::L2<float> >
        class EigenSearch : public DetectBase {
            public:
                /// Eigen matrix type
                typedef Eigen::MatrixBase<Derived> MatrixT;

                /// Aligned vector of Eigen matrices
                typedef std::vector<MatrixT,Eigen::aligned_allocator<MatrixT> > MatrixVecT;

                /// Pointer type
                typedef boost::shared_ptr<EigenSearch<Derived> > Ptr;
                
                /// Pointer to const type
                typedef boost::shared_ptr<const EigenSearch<Derived> > ConstPtr;
                
                /**
                 * Constructor: setup k-d tree to default parameters
                 * @param sort set to false to disable sorted results
                 */
                EigenSearch(bool sort = true) : _sort(sort), _leaf(15), _trees(4), _checks(512) {}

                /**
                 * Constructor: setup k-d tree and index data
                 * @param target target point/feature set
                 * @param sort set to false to disable sorted results
                 */
                EigenSearch(const MatrixT& target, bool sort = true) : _sort(sort), _leaf(15), _trees(4), _checks(512) {
                    setTarget(target);
                }
                
                /// Empty destructor
                virtual ~EigenSearch() {}

                /**
                 * Set the sorting flag
                 * @param sort sorting flag
                 */
                inline void setSort(bool sort) {
                    _sort = sort;
                }

                /**
                 * Get sorting flag
                 * @return sorting flag
                 */
                inline bool getSort() const {
                    return _sort;
                }

                /**
                 * Set leaf size
                 * @param leaf leaf size
                 */
                inline void setLeaf(size_t leaf) {
                    _leaf = leaf;
                }

                /**
                 * Get number of leaves
                 * @return number of leaves
                 */
                inline size_t getLeaf() const {
                    return _leaf;
                }

                /**
                 * Set number of trees to use during high-dimensional search - set this to zero to use an
                 * exact, linear search
                 * @param trees number of trees
                 */
                inline void setTrees(size_t trees) {
                    _trees = trees;
                }

                /**
                 * Get number of trees
                 * @return number of trees
                 */
                inline size_t getTrees() const {
                    return _trees;
                }

                /**
                 * Set number of checks to perform during high-dimensional search - set to <= 0 for infinite and thereby
                 * accurate but slow search
                 * @param checks number of checks
                 */
                inline void setChecks(size_t checks) {
                    if(checks > 0)
                        _checks = checks;
                    else
                        _checks = -1;
                }

                /**
                 * Get number of checks
                 * @return number of checks
                 */
                inline size_t getChecks() const {
                    return _checks;
                }

                /**
                 * Set target point/feature set and index the data
                 * @param target target point/feature set
                 */
                inline void setTarget(const Derived& target) {
                    _target = target;
                    index();
                }

                /**
                 * Get target point/feature set
                 * @return target point/feature set
                 */
                inline Derived getTarget() const {
                    return _target;
                }

                /**
                 * Compute k-NN point/feature matches
                 * @param query query point/feature set
                 * @param k number of matches
                 * @return matches as correspondences
                 */
                core::Correspondence::VecPtr knn(const MatrixT& query, size_t k) const;

                /**
                 * Compute k-NN point/feature matches for a single query feature, given as a column expression
                 * @param query query point/feature
                 * @param k number of matches
                 * @return matches as correspondences
                 */
                inline core::Correspondence knn(const typename MatrixT::ColXpr& query, size_t k) const {
                    return (*knn(Derived(query), k))[0];
                }

                /**
                 * Compute radius matches
                 * @param query query point/feature set
                 * @param r search radius
                 * @return matches as correspondences
                 */
                core::Correspondence::VecPtr radius(const MatrixT& query, float r) const;

                /**
                 * Compute radius matches for a single query feature, given as a column expression
                 * @param query query point/feature
                 * @param r search radius
                 * @return matches as correspondences
                 */
                inline core::Correspondence radius(const typename MatrixT::ColXpr& query, float r) const {
                    return (*radius(Derived(query), r))[0];
                }

                /**
                 * Perform a 2-NN search and return correspondences with the distance set to the ratio
                 * of the distance to the first match to the distance to the seconds, all squared for efficiency
                 * @param query query features
                 * @return matches as correspondences
                 */
                inline core::Correspondence::VecPtr ratio(const MatrixT& query) const {
                    core::Correspondence::VecPtr c = this->knn(query, 2);
                    for(size_t i = 0; i < c->size(); ++i) {
                        // Handle invalids
                        float ratio;
                        if((*c)[i].size() == 2)
                            ratio = (*c)[i].distance[0] / (*c)[i].distance[1];
                        else
                            ratio = 1; // Natural upper bound for ratio matching

                        if(!(*c)[i].empty())
                            (*c)[i] = core::Correspondence((*c)[i].query, (*c)[i].match[0], ratio);
                    }

                    return c;
                }

            private:

                /// Search structure type for low-dimensional data
                typedef typename flann::KDTreeSingleIndex<DistT> SingleTreeT;

                /// Single tree instance
                SingleTreeT _singleTree;

                /// Search structure type for high-dimensional data
                typedef typename flann::KDTreeIndex<DistT> MultiTreeT;

                /// Multi-tree instance
                MultiTreeT _multiTree;

                /// Search structure type for high-dimensional data if no trees should be used
                typedef typename flann::LinearIndex<DistT> LinearIndexT;

                /// Linear index
                LinearIndexT _linearIndex;

                /// Set to true (default) to return neighbors sorted by distance
                bool _sort;

                /// Maximum number of points in a leaf during low-dimensional search
                size_t _leaf;

                /// Number of randomized trees to use during high-dimensional search
                size_t _trees;

                /// Number of checks to perform during high-dimensional search
                size_t _checks;

                /// Resolved point type dimension
                size_t _dim;

                /// Target point/feature set
                Derived _target;

                /**
                 * Build search index
                 * @param target data to index
                 */
                void index();
        };
        
        /**
         * @ingroup detect
         * @brief Perform a single-shot bulk k-NN search in Euclidean space
         * @note This function returns squared distances
         * @param query query points
         * @param target target points to search into
         * @param k number of neighbors to search for
         * @param trees number of randomized trees to use in case of high-dimensional data - set this to zero to use an
         * exact, linear search
         * @param checks number of checks to perform during search in case of high-dimensional data
         * @return matches as correspondences
         */
        core::Correspondence::VecPtr knnSearch(const Eigen::MatrixXf& query,
                                               const Eigen::MatrixXf& target,
                                               size_t k,
                                               size_t trees = 4,
                                               size_t checks = 512);

        /// @copydoc knnSearch()
        inline core::Correspondence::VecPtr computeKnnMatches(const Eigen::MatrixXf& query,
                                                              const Eigen::MatrixXf& target,
                                                              size_t k,
                                                              size_t trees = 4,
                                                              size_t checks = 512) {
            return knnSearch(query, target, k, trees, checks);
        }

        /**
         * @ingroup detect
         * @brief Perform a single-shot bulk radius search in Euclidean space
         * @note This function returns squared distances
         * @param query query points
         * @param target target points to search into
         * @param r Euclidean search radius
         * @param trees number of randomized trees to use in case of high-dimensional data - set this to zero to use an
         * exact, linear search
         * @param checks number of checks to perform during search in case of high-dimensional data
         * @return matches as correspondences
         */
        core::Correspondence::VecPtr radiusSearch(const Eigen::MatrixXf& query,
                                                  const Eigen::MatrixXf& target,
                                                  float r,
                                                  size_t trees = 4,
                                                  size_t checks = 512);

        /// @copydoc radiusSearch()
        inline core::Correspondence::VecPtr computeRadiusMatches(const Eigen::MatrixXf& query,
                                                                 const Eigen::MatrixXf& target,
                                                                 float r,
                                                                 size_t trees = 4,
                                                                 size_t checks = 512) {
            return radiusSearch(query, target, r, trees, checks);
        }

        /**
         * @ingroup detect
         * @brief Perform a 2-NN search in feature space and return correspondences with the distance set to the ratio
         * of the distance to the first match to the distance to the seconds, all squared for efficiency
         * @param query query features
         * @param target target features to search into
         * @param trees number of randomized trees to use in case of high-dimensional data - set this to zero to use an
         * exact, linear search
         * @param checks number of checks to perform during search in case of high-dimensional data
         * @return matches as correspondences
         */
        core::Correspondence::VecPtr computeRatioMatches(const Eigen::MatrixXf& query,
                                                         const Eigen::MatrixXf& target,
                                                         size_t trees = 4,
                                                         size_t checks = 512);

        /**
         * @ingroup detect
         * @brief Perform ratio matching between two or more sets of features and fuse the matches
         * @param query query features
         * @param target target features to search into
         * @param trees number of randomized trees to use in case of high-dimensional data - set this to zero to use an
         * exact, linear search
         * @param checks number of checks to perform during search in case of high-dimensional data
         * @return matches as correspondences
         */
        core::Correspondence::VecPtr computeFusionMatches(
                const std::vector<Eigen::MatrixXf, Eigen::aligned_allocator<Eigen::MatrixXf> >& query,
                const std::vector<Eigen::MatrixXf, Eigen::aligned_allocator<Eigen::MatrixXf> >& target,
                size_t trees = 4,
                size_t checks = 512);
    }
}

#include "eigen_search_impl.hpp"
    
#endif
