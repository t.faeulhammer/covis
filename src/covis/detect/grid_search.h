// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_DETECT_GRID_SEARCH_H
#define COVIS_DETECT_GRID_SEARCH_H

// Own
#include <covis/core/range.h>
#include "search_base.h"
#include "../core/correspondence.h"

// STL
#include <queue>

namespace covis {
    namespace detect {
        /**
         * @ingroup detect
         * @class GridSearch
         * @brief Search in 3D data using a grid
         *
         * @note This class returns squared distances
         *
         * @tparam PointT point type, must contain XYZ data
         * @author Anders Glent Buch
         */
        template<typename PointT>
        class GridSearch : public SearchBase<PointT> {
            using SearchBase<PointT>::_target;
            
            public:
                /// Pointer type
                typedef boost::shared_ptr<GridSearch<PointT> > Ptr;
                
                /// Pointer to const type
                typedef boost::shared_ptr<const GridSearch<PointT> > ConstPtr;

                /// Point index type
                typedef int IdxT;

                /// List of point indices type
                typedef std::vector<IdxT> ListT;
                
                /**
                 * Constructor: set metric resolution, default to 0.01 in all directions
                 * @param dx x resolution
                 * @param dy y resolution
                 * @param dz z resolution
                 * @param sort set to true to return sorted radius neighbors
                 */
                GridSearch(float dx = 0.01, float dy = 0.01, float dz = 0.01, bool sort = false) :
                        _dx(dx), _dy(dy), _dz(dz), _sort(sort), _useNeighborCells(false) {}

                /**
                 * Constructor: index a point set, setting metric resolution, default to 0.01 in all directions
                 * @param target target point set
                 * @param dx x resolution
                 * @param dy y resolution
                 * @param dz z resolution
                 * @param sort set to true to return sorted radius neighbors
                 */
                GridSearch(typename pcl::PointCloud<PointT>::ConstPtr target,
                           float dx = 0.01, float dy = 0.01, float dz = 0.01, bool sort = false) :
                        _dx(dx), _dy(dy), _dz(dz), _sort(sort), _useNeighborCells(false) {
                    this->setTarget(target);
                }

                /// Empty destructor
                virtual ~GridSearch() {}

                /**
                 * Set the sorting flag
                 * @param sort sorting flag
                 */
                inline void setSort(bool sort) {
                    _sort = sort;
                }

                /**
                 * Set this flag to enable further checks in a 27-point neighborhood around the current cell
                 * @param useNeighborCells
                 */
                inline void setUseNeighborCells(bool useNeighborCells) {
                    _useNeighborCells = useNeighborCells;
                }

                /**
                 * Return the indices of points within the cell occupied by an indexed point
                 * @param idx point index
                 * @return indices of indexed points within the cell in which the point appears
                 */
                const ListT& getVoxel(size_t idx) const;

                /**
                 * Return the number of points within the cell occupied by an indexed point
                 * @param idx point index
                 * @return number of indexed points within the cell in which the point appears
                 */
                size_t count(size_t idx) const;

                /**
                 * Return the number of points within the cell occupied by a query point
                 * @param query query point
                 * @return number of indexed points within the cell in which the query point appears
                 */
                size_t count(const PointT& query) const;

            private:
                /// Grid resolution, x
                float _dx;

                /// Grid resolution, y
                float _dy;

                /// Grid resolution, z
                float _dz;

                /// Set to true to return sorted radius neighbors
                bool _sort;

                /// Set to true to also check all cells in a 27-point neighborhood
                bool _useNeighborCells;

                /// Computed bounding box corner, min
                PointT _min;

                /// Computed bounding box corner, max
                PointT _max;

                /// Computed number of cells, x
                IdxT _nx;

                /// Computed number of cells, y
                IdxT _ny;

                /// Computed number of cells, z
                IdxT _nz;

                /// Grid type
                typedef std::vector<std::vector<std::vector<ListT> > > IndexT;

                /// Grid
                IndexT _index;

                /// Three-dimensional integer coordinate
                struct Coordinate {
                    IdxT x, y, z;
                };

                /// Inverse index pointing from linear point index to grid coordinate
                std::vector<Coordinate> _reverseIndex;

                /// Pair type of <index,distance> for sorting
                typedef std::pair<IdxT,float> PairT;

                /// Vector of pairs
                typedef std::vector<std::pair<IdxT,float> > PairVecT;

                /// Priority queue for sorting
                typedef std::priority_queue<PairT, PairVecT, core::CmpDescend<PairT> > QueueT;

                /// Get the index of a query point
                inline Coordinate getIdx(const PointT& p) const {
                    Coordinate c;
                    c.x = IdxT((p.x - _min.x) / _dx);
                    c.y = IdxT((p.y - _min.y) / _dy);
                    c.z = IdxT((p.z - _min.z) / _dz);

                    return c;
                }

                /// Get the immediate neighbors of an indexed cell
                inline ListT getNeighbors(IdxT ix, IdxT iy, IdxT iz) const {
                    if(ix < 0 || iy < 0 || iz < 0 || ix >= _nx || iy >= _ny || iz >= _nz)
                        return ListT();
                    else
                        return _index[ix][iy][iz];
                }

                /// Get the immediate neighbors of an indexed cell
                inline ListT getNeighbors(const Coordinate& c) const {
                    return getNeighbors(c.x, c.y, c.z);
                }

                /// Get the immediate neighbors of a query point
                inline ListT getNeighbors(const PointT& p) const {
                    COVIS_ASSERT_MSG(!_index.empty(), "No target points added to index!");

                    if(!(std::isfinite(p.x) && std::isfinite(p.y) && std::isfinite(p.z)))
                        return ListT();

                    const Coordinate c = getIdx(p);
                    if(_useNeighborCells) {
                        ListT result;

                        for(IdxT iix = c.x-1; iix <= c.x+1; ++iix) {
                            for(IdxT iiy = c.y-1; iiy <= c.y+1; ++iiy) {
                                const ListT l1 = getNeighbors(iix, iiy, c.z-1);
                                const ListT l2 = getNeighbors(iix, iiy, c.z);
                                const ListT l3 = getNeighbors(iix, iiy, c.z+1);

                                result.insert(result.end(), l1.begin(), l1.end());
                                result.insert(result.end(), l2.begin(), l2.end());
                                result.insert(result.end(), l3.begin(), l3.end());
                            }
                        }

                        return result;
                    } else {
                        return getNeighbors(c);
                    }
                }

                /// @copydoc SearchBase::index()
                void index();
                
                /// @copydoc SearchBase::doKnn()
                void doKnn(const PointT& query, size_t k, core::Correspondence& result) const;
                
                /// @copydoc SearchBase::doRadius()
                void doRadius(const PointT& query, float r, core::Correspondence& result) const;
        };
    }
}

#ifndef COVIS_PRECOMPILE
#include "grid_search_impl.hpp"
#endif

#endif
