// Copyright (c) 2016, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

// Own
#include "ppf_matching.h"
#include "eigen_search.h"
#include "../core/range.h"

namespace covis {
    namespace detect {
        core::Correspondence::VecPtr computeKnnMatchesPPF(const core::Correspondence::Vec& ppfcorr,
                                                          size_t numQuery,
                                                          size_t numTarget,
                                                          const std::vector<std::pair<int,int> >& idxQuery,
                                                          const std::vector<std::pair<int,int> >& idxTarget,
                                                          size_t k) {
            COVIS_ASSERT(k > 0);
            Eigen::MatrixXi votes(numTarget, numQuery);
            votes.setZero();
            for(size_t i = 0; i < ppfcorr.size(); ++i) {
                const core::Correspondence& c = ppfcorr[i];
                for(size_t j = 0; j < c.size(); ++j) {
                    const int queryRef = idxQuery[c.query].first;
                    const int targetRef = idxTarget[c.match[j]].first;
                    ++votes(targetRef, queryRef);
                }
            }

            core::Correspondence::VecPtr result(new core::Correspondence::Vec(numQuery));
            if(k == 1) {
                for(size_t i = 0; i < numQuery; ++i) {
                    int maxidx;
                    const float maxvotes = votes.col(i).maxCoeff(&maxidx);
                    (*result)[i] = core::Correspondence(i, maxidx, -maxvotes);
                }
            } else {
                for(size_t i = 0; i < numQuery; ++i) {
                    std::vector<int> coli(votes.col(i).data(), votes.col(i).data() + numTarget);
                    std::vector<size_t> order = core::sort<int>(coli, false);
                    order.resize(MIN(k, order.size()));
                    (*result)[i].query = i;
                    (*result)[i].match = order;
                    (*result)[i].distance.resize(order.size());
                    for(size_t j = 0; j < order.size(); ++j)
                        (*result)[i].distance[j] = -coli[j];
                }
            }

            return result;
        }

        core::Correspondence::VecPtr computeKnnMatchesPPF(const feature::MatrixT& query,
                                                          const feature::MatrixT& target,
                                                          size_t numQuery,
                                                          size_t numTarget,
                                                          const std::vector<std::pair<int,int> >& idxQuery,
                                                          const std::vector<std::pair<int,int> >& idxTarget,
                                                          size_t k) {
            core::Correspondence::VecPtr ppfcorr = detect::computeKnnMatches(query, target, 50);

            return computeKnnMatchesPPF(*ppfcorr, numQuery, numTarget, idxQuery, idxTarget, k);
        }
    }
}
