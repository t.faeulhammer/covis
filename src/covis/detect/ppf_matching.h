// Copyright (c) 2016, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_DETECT_PPF_MATCHING_H
#define COVIS_DETECT_PPF_MATCHING_H

// Own
#include "../core/correspondence.h"
#include "covis/feature/ppf_extraction.h"

namespace covis {
    namespace detect {
        /**
         * @ingroup detect
         * @brief Compute PPF matches
         *
         * Compute reference point matches using point pair features computed by @ref feature::PPFExtraction and the
         * convenience function @ref feature::computePPFEigen()
         *
         * @param query first set of PPFs
         * @param target second set of PPFs
         * @param numQuery number of surface points used for generating the first set of PPFs
         * @param numTarget number of surface points used for generating the second set of PPFs
         * @param idxQuery index map for the first set (see the class referenced above)
         * @param idxTarget index map for the second set (see the class referenced above)
         * @param k number of nearest neighbors to search for
         * @return correspondences between individual surface points
         */
        core::Correspondence::VecPtr computeKnnMatchesPPF(const feature::MatrixT& query,
                                                          const feature::MatrixT& target,
                                                          size_t numQuery,
                                                          size_t numTarget,
                                                          const std::vector<std::pair<int,int> >& idxQuery,
                                                          const std::vector<std::pair<int,int> >& idxTarget,
                                                          size_t k = 1);

        /**
         * @ingroup detect
         * @brief Compute PPF matches
         *
         * Compute reference point matches using point pair features computed by @ref feature::PPFExtraction and the
         * convenience function @ref feature::computePPFEigen()
         *
         * @param ppfcorr feature matches between two sets of PPFs
         * @param numQuery number of surface points used for generating the first set of PPFs
         * @param numTarget number of surface points used for generating the second set of PPFs
         * @param idxQuery index map for the first set (see the class referenced above)
         * @param idxTarget index map for the second set (see the class referenced above)
         * @param k number of nearest neighbors to search for
         * @return correspondences between individual surface points
         */
        core::Correspondence::VecPtr computeKnnMatchesPPF(const core::Correspondence::Vec& ppfcorr,
                                                          size_t numQuery,
                                                          size_t numTarget,
                                                          const std::vector<std::pair<int,int> >& idxQuery,
                                                          const std::vector<std::pair<int,int> >& idxTarget,
                                                          size_t k = 1);
    }
}

#endif
