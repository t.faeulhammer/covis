// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_DETECT_FEATURE_SEARCH_IMPL_HPP
#define COVIS_DETECT_FEATURE_SEARCH_IMPL_HPP

namespace covis {
    namespace detect {
        template<typename FeatureT, typename DistT>
        void FeatureSearch<FeatureT, DistT>::index() {
            // Clean up
            if(_mtarget.ptr() != NULL)
                delete[] _mtarget.ptr();

            // Set target data
            std::vector<bool> nans;
            if(_target)
                convert(*_target, _mtarget, nans);
            else
                COVIS_ASSERT_MSG(_mtarget.ptr() != NULL, "Target features not set!");
            
            // Build
#ifdef FLANN_1_8
            _tree.buildIndex(_mtarget);
#else
            if(_tree != NULL)
                delete _tree;
            _tree = new flann::Index<DistT>(_mtarget, flann::KDTreeIndexParams(_trees));
            _tree->buildIndex();
#endif
        }
        
        template<typename FeatureT, typename DistT>
        void FeatureSearch<FeatureT, DistT>::doKnn(const FeatureT& query, size_t k, core::Correspondence& result) const {
            // Convert
            bool isnan;
            flann::Matrix<ElementT> mquery;
            convert(query, mquery, isnan);

            // Output
            if(isnan) {
                COVIS_MSG_WARN("NaN feature given to feature k-NN search - returning empty correspondence!");
                result = core::Correspondence();
            } else {
                // Search
                result.match.resize(k);
                result.distance.resize(k);
#ifdef FLANN_1_8
                flann::Matrix<idx_t> mmatch(&result.match[0], 1, k);
                flann::Matrix<float> mdist(&result.distance[0], 1, k);
                _tree.knnSearch(mquery, mmatch, mdist, k, _sparam);
#else
                std::vector<std::vector<idx_t> > idx;
                std::vector<std::vector<ResultT> > distsq;

                _tree->knnSearch(mquery, idx, distsq, int(k), _sparam);

                // Convert back
                for (size_t i = 0; i < k; ++i) {
                    result.match[i] = idx[0][i];
                    result.distance[i] = distsq[0][i];
                }
#endif
            }

            // Clean up
            delete[] mquery.ptr();
        }
        
        template<typename FeatureT, typename DistT>
        void FeatureSearch<FeatureT, DistT>::doRadius(const FeatureT& query, float r, core::Correspondence& result) const {
            // Convert
            bool isnan;
            flann::Matrix<ElementT> mquery;
            convert(query, mquery, isnan);

            // Output
            if(isnan) {
                COVIS_MSG_WARN("NaN feature given to feature radius search - returning empty correspondence!");
                result = core::Correspondence();
            } else {
                std::vector<std::vector<idx_t> > idx;
                std::vector<std::vector<ResultT> > distsq;

                // Search
#ifdef FLANN_1_8
                _tree.radiusSearch(mquery, idx, distsq, r * r, _sparam);

                // Convert back
                result.match = idx[0];
                result.distance = distsq[0];
#else
                _tree->radiusSearch(mquery, idx, distsq, r * r, _sparam);

                // Convert back
                const size_t k = idx[0].size();
                result.match.resize(k);
                result.distance.resize(k);
                for (size_t i = 0; i < k; ++i) {
                    result.match[i] = idx[0][i];
                    result.distance[i] = distsq[0][i];
                }
#endif
            }

            // Clean up
            delete[] mquery.ptr();
        }
    }
}

#endif
