#include "template_matching.h"

#include "../core/progress_display.h"
#include "../core/io.h"
#include "../core/macros.h"
#include "../core/range.h"
#include "../core/stat.h"

#include <cmath>
using namespace std;

// OpenCV
using namespace cv;

// Covis
using namespace covis::detect;

TemplateMatching::TemplateMatching(int width, int height, float lowEdgeThreshold, float highEdgeThreshold) :
                            _width(width),
                            _height(height),
                            _ksize(5),
                            _normalize(true),
                            _twidth(0),
                            _theight(0),
                            _lowEdgeThreshold(lowEdgeThreshold),
                            _highEdgeThreshold(highEdgeThreshold),
							_bidirectional(false) {
    COVIS_ASSERT(width > 0 && height > 0);
}

void TemplateMatching::loadTemplates(const vector<Mat>& templates, bool external) {
    // Clean up
    _t.resize(templates.size());
    _ti.resize(templates.size());
    _te.resize(templates.size());
    _dte.resize(templates.size());
    _responses.resize(templates.size());

    // Maximal template sizes
    _twidth = 0;
    _theight = 0;
    
    // Load
    for(size_t i = 0; i < templates.size(); ++i) {
        checkImage(templates[i]);
        templates[i].copyTo(_t[i]);
        
        _theight = MAX(_theight, _t[i].rows);
        _twidth = MAX(_twidth, _t[i].cols);
        
        // Convert to intensity, 8-bit unsigned
        if(_t[i].channels() >= 3) {
            cvtColor(_t[i], _ti[i], COLOR_BGR2GRAY);
        } else {
            _t[i].copyTo(_ti[i]);
            cvtColor(_t[i], _t[i], COLOR_GRAY2BGR);
        }
        
        // Get edges and orientations
        Mat tmag, to;
        computeEdges(_ti[i], _te[i], 10, external);//, tmag, to);
        
        // Get distance transform
        dt(_te[i], _dte[i]);
        
        // Convert intensity template to float
        if(_ti[i].depth() == CV_8U)
            _ti[i] = Mat_<float>(_ti[i] / 255);
        else if(_ti[i].depth() == CV_64F)
            _ti[i] = Mat_<float>(_ti[i]);
        
        // Convert edge template to float
        _te[i] = Mat_<float>(_te[i] / 255);
    }
    
    // Prepare DFTs
    _tedft.resize(templates.size());
    _dtedft.resize(templates.size());
    Size dftSize;
    dftSize.width = getOptimalDFTSize(_width + _twidth - 1);
    dftSize.height = getOptimalDFTSize(_height + _theight - 1);
#ifdef _OPENMP
#pragma omp parallel for
#endif
    for(size_t i = 0; i < templates.size(); ++i) {
        // Allocate outputs, zero-padded
        _tedft[i] = Mat(dftSize, _te[i].type(), Scalar::all(0));
        _dtedft[i] = Mat(dftSize, _dte[i].type(), Scalar::all(0));
    
        // Copy image contents into upper left corner of outputs
        Mat teroi(_tedft[i], Rect(0, 0, templates[i].cols, templates[i].rows));
        Mat dteroi(_dtedft[i], Rect(0, 0, templates[i].cols, templates[i].rows));
        _te[i].copyTo(teroi);
        _dte[i].copyTo(dteroi);
    
        // Now do the DFT, only use the relevant number of rows
        dft(_tedft[i], _tedft[i], 0, templates[i].rows);
        dft(_dtedft[i], _dtedft[i], 0, templates[i].rows);
    }
}

void TemplateMatching::loadEdgeTemplates(const vector<Mat>& templates) {
    // Clean up
    _te.resize(templates.size());
    _dte.resize(templates.size());
    _responses.resize(templates.size());

    // Maximal template sizes
    _twidth = 0;
    _theight = 0;
    
    // Load
    for(size_t i = 0; i < templates.size(); ++i) {
        checkImage(templates[i]);
        templates[i].copyTo(_te[i]);
        
        _theight = MAX(_theight, _te[i].rows);
        _twidth = MAX(_twidth, _te[i].cols);
        
        // Get indices of edges
//        _teidx[i] = indices(_te[i]);
        
        // Get distance transform
        dt(_te[i], _dte[i]);
        
        // Convert edge template to float
        _te[i] = Mat_<float>(_te[i] / 255);
    }
    
    // Prepare DFTs
    _tedft.resize(templates.size());
    _dtedft.resize(templates.size());
    Size dftSize;
    dftSize.width = getOptimalDFTSize(_width + _twidth - 1);
    dftSize.height = getOptimalDFTSize(_height + _theight - 1);
#ifdef _OPENMP
#pragma omp parallel for
#endif
    for(size_t i = 0; i < templates.size(); ++i) {
        // Allocate outputs, zero-padded
        _tedft[i] = Mat(dftSize, _te[i].type(), Scalar::all(0));
        _dtedft[i] = Mat(dftSize, _dte[i].type(), Scalar::all(0));
    
        // Copy image contents into upper left corner of outputs
        Mat teroi(_tedft[i], Rect(0, 0, templates[i].cols, templates[i].rows));
        Mat dteroi(_dtedft[i], Rect(0, 0, templates[i].cols, templates[i].rows));
        _te[i].copyTo(teroi);
        _dte[i].copyTo(dteroi);
    
        // Now do the DFT, only use the relevant number of rows
        dft(_tedft[i], _tedft[i], 0, templates[i].rows);
        dft(_dtedft[i], _dtedft[i], 0, templates[i].rows);
    }
}

void TemplateMatching::loadImage(const cv::Mat& image, bool external) {
    COVIS_ASSERT_MSG(_twidth > 0 && _theight, "Templates have not been loaded!");
    
    checkImage(image);
    
    _color = image.clone();

    if(_roi.area() > 0)
    	_color = _color(_roi);

    COVIS_ASSERT_MSG(_color.rows == _height && _color.cols == _width, "Image not consistent with expected dimensions!");

    // Convert to intensity, 8-bit unsigned
    if(_color.channels() >= 3)
        cvtColor(_color, _intensity, COLOR_BGR2GRAY);
    else {
        cvtColor(_color, _color, COLOR_GRAY2BGR);
        _color.copyTo(_intensity);
    }

    // Get edges
//    Mat mag, ori;
    computeEdges(_intensity, _edges, 0, external);//, mag, ori);
    
    // compute distance transform (float)
    dt(_edges, _dt);

    // Convert intensity template to float
    if(_intensity.depth() == CV_8U)
        _intensity = Mat_<float>(_intensity / 255);
    else if(_intensity.depth() == CV_64F)
        _intensity = Mat_<float>(_intensity);
    
    // Convert edge image to float
    _edges = Mat_<float>(_edges / 255);
    
    // Compute DFTs
    Size dftSize;
    dftSize.width = getOptimalDFTSize(_width + _twidth - 1);
    dftSize.height = getOptimalDFTSize(_height + _theight - 1);
    
    // Allocate outputs, zero-padded
    _edgesdft = Mat(dftSize, _edges.type(), Scalar::all(0));
    _dtdft = Mat(dftSize, _dt.type(), Scalar::all(0));

    // Copy image contents into upper left corner of outputs
    Mat teroi(_edgesdft, Rect(0, 0, _width, _height));
    Mat dteroi(_dtdft, Rect(0, 0, _width, _height));
    _edges.copyTo(teroi);
    _dt.copyTo(dteroi);

    // Now do the DFT, only use the relevant number of rows
    dft(_edgesdft, _edgesdft, 0, _height);
    dft(_dtdft, _dtdft, 0, _height);
}

void TemplateMatching::checkImage(const cv::Mat& image) {
    COVIS_ASSERT(!image.empty());
    COVIS_ASSERT(image.depth() == CV_8U);
    COVIS_ASSERT(image.channels() <= 4);
}

void TemplateMatching::matchIntensity(size_t& id, cv::Point& pmin, float& min, cv::Mat& response) {
    COVIS_ASSERT_MSG(!_ti.empty(), "Intensity templates not loaded - use loadTemplates()!");
    min = FLT_MAX;
    for(size_t i = 0; i < _ti.size(); ++i) {
        _responses[i] = matchi(i);
        
        // Get best match
        double mini;
        Point pmini;
        minMaxLoc(_responses[i], &mini, 0, &pmini, 0);
        
        // If we have a ROI, offset detection
        if(_roi.area() > 0) {
        	pmin.x += _roi.x;
        	pmin.y += _roi.y;
        }

        // If best so far, update output
        if(mini < min) {
            id = i;
            pmin = pmini;
            min = mini;
            response = _responses[i];
        }
    }
}

void TemplateMatching::matchEdges(size_t& id, cv::Point& pmin, float& min, cv::Mat& response) {
    COVIS_ASSERT_MSG(!_te.empty(), "Edge templates not loaded - use loadTemplates() or loadEdgeTemplates()!");
    min = FLT_MAX;
    
#ifdef _OPENMP
#pragma omp parallel for
#endif
    for(size_t i = 0; i < _te.size(); ++i) {
        _responses[i] = matche(i);

        // Get best match
        double mini = FLT_MAX;
        Point pmini;
        minMaxLoc(_responses[i], &mini, 0, &pmini, 0);

        // If we have a ROI, offset detection
        if(_roi.area() > 0) {
        	pmini.x += _roi.x;
        	pmini.y += _roi.y;
        }

        // If best so far, update output
#ifdef _OPENMP
#pragma omp critical
#endif
        if(mini < min) {
        	id = i;
        	pmin = pmini;
        	min = mini;
        	response = _responses[i];
        }
    }
}

void TemplateMatching::getAllMatches(std::vector<size_t>& vid, std::vector<cv::Point>& vpmin, std::vector<float>& vmin) const {
    // Generate a joint image of <index,response> for best match at each pixel
    Mat_<Vec2f> idxResp(_height, _width, Vec2f(-1, FLT_MAX));
    float max = 0;
    
    // Loop over templates
    for(size_t i = 0; i < _te.size(); ++i) {
    	// Loop over the response image for current template
		for(int r = 0; r < _responses[i].rows; ++r) {
			const float* row = _responses[i].ptr<float>(r);
			for(int c = 0; c < _responses[i].cols; ++c) {
				const float& value = row[c];
				if(value < idxResp(r,c)[1]) {
					idxResp(r,c) = Vec2f(i, value);
				}
				if(value > max)
					max = value;
			}
		}
    }
    
    // Finally, since above loop does not touch all pixels in idxResp, clamp all infinities to max
	for(int r = 0; r < idxResp.rows; ++r) {
		Vec2f* row = idxResp.ptr<Vec2f>(r);
		for(int c = 0; c < idxResp.cols; ++c) {
			float& value = row[c][1];
			if(value == FLT_MAX)
				value = max;
		}
	}
	
	// Perform NMS on response image
	Mat_<float> idxRespArray[2];
	split(idxResp, idxRespArray);
	Mat_<float> idx = idxRespArray[0];
	Mat_<float> resp = idxRespArray[1];
	
	// Change response to a score and run NMS
	vpmin = nms(max - resp, MAX(_twidth, _theight) / 2);
	
	// Set the rest of the outputs
	vid.resize(vpmin.size());
	vmin.resize(vpmin.size());
	for(size_t i = 0; i < vpmin.size(); ++i) {
		vid[i] = idx(vpmin[i]);
		vmin[i] = resp(vpmin[i]);
        // If we have a ROI, offset detection
        if(_roi.area() > 0) {
        	vpmin[i].x += _roi.x;
        	vpmin[i].y += _roi.y;
        }
	}
	
	// Sort ascending
	const vector<size_t> order = core::sort(vmin);
	vid = core::reorder(vid, order);
	vpmin = core::reorder(vpmin, order);
}

void TemplateMatching::drawDetection(cv::Mat& image, size_t id, const cv::Point& pmin, bool showROI, bool showTemplate) const {
	// Sanity check
	if(_roi.area() > 0)
	    ; // TODO
    else
        COVIS_ASSERT_MSG(image.rows == _height && image.cols == _width, "Image for drawing detection has wrong size!");
	COVIS_ASSERT(image.depth() == CV_32F || image.depth() == CV_64F || image.depth() == CV_8U);
	
	// Get template
    Mat t;
    if(_te.size() > 0)
    	t = _te[id];
    else
    	t = _t[id];
	
    // Get the four corners of the template match
    Point bboxmin, bboxmax;
    bboxmin.x = pmin.x;
    bboxmin.y = pmin.y;
    bboxmax.x = pmin.x + t.cols;
    bboxmax.y = pmin.y + t.rows;
    
    // Make sure image is 3-channel
    if(image.channels() == 1)
    	cvtColor(image, image, CV_GRAY2BGR);
    
    // Get the upper pixel value (white)
    const double max = ( (image.depth() == CV_32F || image.depth() == CV_64F) ? 1.0 : 255 );
    const Scalar white = Scalar::all(max);
    const Scalar rcolor = Scalar(3*max/4, 0, 3*max/4);
    
    // Create a rectangle with score/penalty
    rectangle(image, bboxmin, bboxmax, rcolor, 2);
    
    // Draw a filled circle in the middle of the bbox, either using COM (edges) or just center coordinate
    Point pc;
    if(_te.size() > 0) {
        Moments mu = moments(_te[id]);
        const Point com(mu.m10/mu.m00, mu.m01/mu.m00);
    	pc = pmin + com;
    } else {
    	pc = pmin + Point(t.cols/2, t.rows/2);
    }
    circle(image, pc, 5, rcolor, -1);
    
    // Start position for the template in the bottom right corner of the main window
    const Point offset(image.cols - t.cols, image.rows - t.rows);
    
    // Overlay the best template on the input image
    if(showTemplate) {
		if(_t.size() > 0)
			t.copyTo( image(Rect(offset.x, offset.y, t.cols, t.rows)) );
		else // No intensity template, start with black
			image(Rect(offset.x, offset.y, t.cols, t.rows)) = Scalar::all(0);
		
		// If we have edge templates, overlay the template edges with blue
		if(_te.size() > 0) {
			for(int i = 0; i < _te[id].rows; ++i) {
				for(int j = 0; j < _te[id].cols; ++j) {
					if(_te[id].at<float>(i,j) > 0) {
						if(image.depth() == CV_32F)
							image.at<Vec3f>(offset.y + i, offset.x + j) = Vec3f(1,0,0);
						else if(image.depth() == CV_64F)
							image.at<Vec3d>(offset.y + i, offset.x + j) = Vec3d(1,0,0);
						else
							image.at<Vec3b>(offset.y + i, offset.x + j) = Vec3b(255,0,0);
					}
				}
			}
		}
	    
	    // Show the index of the best template
	    Point bottom(image.cols - t.cols, image.rows - t.rows - 5);
	    if(_te.size() > 1)
	        putText(image, covis::core::stringify(id), bottom, FONT_HERSHEY_COMPLEX, 0.75, white);
	    else
	        putText(image, "Template", bottom, FONT_HERSHEY_COMPLEX, 0.75, white);
    }
    
    // Show the ROI
    if(showROI)
		if(_roi.area() > 0)
			rectangle(image, _roi, 0.75*white, 3);
}

void TemplateMatching::drawEdges(Mat& image) const {
	// Sanity check
	if(_roi.area() > 0)
	    ; // TODO
	else
    	COVIS_ASSERT_MSG(image.rows == _height && image.cols == _width, "Image for drawing edges has wrong size!");
	COVIS_ASSERT(image.depth() == CV_32F ||image.depth() == CV_64F || image.depth() == CV_8U);
    
    // Make sure image is 3-channel
    if(image.channels() == 1)
    	cvtColor(image, image, CV_GRAY2BGR);
    
    // Overlay image edges with blue, if we have it
    if(!_edges.empty()) {
    	for(int i = 0; i < _height; ++i) {
    		for(int j = 0; j < _width; ++j) {
    			if(_edges.at<float>(i,j) > 0) {
    			    const int ii = (_roi.area() > 0 ? i + _roi.y : i);
    			    const int jj = (_roi.area() > 0 ? j + _roi.x : j);
    				if(image.depth() == CV_32F)
    					image.at<Vec3f>(ii,jj) = Vec3f(1,0,0);
    				else if(image.depth() == CV_64F)
    					image.at<Vec3d>(ii,jj) = Vec3d(1,0,0);
    				else
    					image.at<Vec3b>(ii,jj) = Vec3b(255,0,0);
    			}
    		}
    	}
    }
}

void TemplateMatching::computeEdges(const Mat& image, Mat& edges, int border, bool external) const {/*,
        Mat& mag,
        Mat& ori) const {*/
    COVIS_ASSERT(image.depth() == CV_8U);
    
    if(image.channels() >= 3)
        cvtColor(image, edges, COLOR_BGR2GRAY);
    else
        image.copyTo(edges);
    
    if(border > 0)
    	copyMakeBorder(edges, edges, border, border, border, border, BORDER_CONSTANT, Scalar::all(0));
    
    // Blur
//    blur(edges, edges, Size(_ksize, _ksize));
    GaussianBlur(edges, edges, Size(_ksize, _ksize), 0);
    
    if(_normalize)
        normalize(edges, edges, 255, 0, CV_MINMAX);
    
//    // Get derivatives
//    Mat dx, dy;
//    Sobel(edges, dx, CV_32F, 1, 0, 3);
//    Sobel(edges, dy, CV_32F, 0, 1, 3);
//    
//    // The Sobel 3x3 kernel has an energy of 8, so we normalize that away
//    dx /= 8.0f;
//    dy /= 8.0f;
//    
//    // Convert to polar
//    cartToPolar(dx, dy, mag, ori, false);
    
    // Extract edge pixels
    // TODO: Tune the low/high thresholds
    float lowthres = _lowEdgeThreshold;
    float highthres = _highEdgeThreshold;
    if(_lowEdgeThreshold <= 0)
        lowthres = 0.66*core::median(edges);
    if(_highEdgeThreshold <= 0)
        highthres = 2 * lowthres; // Lower threshold means more edges
    Canny(edges, edges, lowthres, highthres, 3);
    
    if(external) {
		// Close the edge image first
		const Mat kernel = getStructuringElement(MORPH_RECT, Size(5,5));
		dilate(edges, edges, kernel);
		erode(edges, edges, kernel);
		
		// Then extract contours
		vector<vector<Point> > contours;
		vector<Vec4i> hierarchy;
		Mat tmp = edges.clone();
		findContours(tmp, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
		
		edges = Scalar::all(0);
		drawContours(edges, contours, 0, Scalar::all(255));
    }
    
    // Remove borders again
    if(border > 0)
    	edges = edges(Rect(border, border, image.cols, image.rows));
    
//    // Normalize the derivatives
//    for(int r = 0; r < image.rows; ++r) {
//        for(int c = 0; c < image.cols; ++c) {
//            const float& magrc = mag.at<float>(r,c);
//            
//            if(magrc >= 5) {
//                dx.at<float>(r,c) /= magrc;
//                dy.at<float>(r,c) /= magrc;
//            } else {
//                dx.at<float>(r,c) = 0;
//                dy.at<float>(r,c) = 0;
//            }
//        }
//    }
}

void TemplateMatching::computeDerivs(const Mat& image, Mat& dx, Mat& dy, int border, bool normalizeMagnitude) const {
    COVIS_ASSERT(image.depth() == CV_8U);
    
    Mat tmp;
    
    if(image.channels() >= 3)
        cvtColor(image, tmp, COLOR_BGR2GRAY);
    else
        image.copyTo(tmp);
    
    if(border > 0)
    	copyMakeBorder(tmp, tmp, border, border, border, border, BORDER_CONSTANT, Scalar::all(0));
    
    // Blur
//    blur(edges, edges, Size(_ksize, _ksize));
    GaussianBlur(tmp, tmp, Size(_ksize, _ksize), 0);
    
    if(_normalize)
        normalize(tmp, tmp, 255, 0, CV_MINMAX);
    
    // Get derivatives
    Sobel(tmp, dx, CV_32F, 1, 0, 3);
    Sobel(tmp, dy, CV_32F, 0, 1, 3);
    
    // The Sobel 3x3 kernel has an energy of 8, so we normalize that away
    dx /= 8.0f;
    dy /= 8.0f;
    
    // Remove borders again
    if(border > 0) {
    	dx = dx(Rect(border, border, image.cols, image.rows));
    	dy = dy(Rect(border, border, image.cols, image.rows));
    }
    
    // Normalize the derivatives
    if(normalizeMagnitude) {
        // Convert to polar to get magnitude
        Mat mag, ori;
        cartToPolar(dx, dy, mag, ori, false);
		for(int r = 0; r < image.rows; ++r) {
			for(int c = 0; c < image.cols; ++c) {
				const float& magrc = mag.at<float>(r,c);
				
				if(magrc >= 1e-5f) {
					dx.at<float>(r,c) /= magrc;
					dy.at<float>(r,c) /= magrc;
				} else {
					dx.at<float>(r,c) = 0;
					dy.at<float>(r,c) = 0;
				}
			}
		}
    }
}

void TemplateMatching::dt(const Mat& edges, Mat& result) {
    COVIS_ASSERT(edges.type() == CV_8U);
    
    // Invert for distance transform
    Mat edgesInv;
    cv::subtract(cv::Scalar(255), edges, edgesInv);

    // Compute distance transform to black pixels
    distanceTransform(edgesInv, result, CV_DIST_L2, CV_DIST_MASK_PRECISE);
}

Mat TemplateMatching::matchi(size_t i) {
    cv::Mat_<float> match;
    matchTemplate(_intensity, _ti[i], match, TM_SQDIFF); // SSE of intensity differences
    sqrt(match / _ti[i].total(), match); // RMSE
    
    return match;
}

Mat TemplateMatching::matche(size_t i) {
    cv::Mat_<float> match1, match2;
    
#if 1
    /*
     * WITH FFT
     */
    // Last true takes the conjugate of B, giving correlation instead of convolution
    mulSpectrums(_dtdft, _tedft[i], match1, 0, true);
    // Get back to the spatial domain
    dft(match1, match1, DFT_INVERSE + DFT_SCALE, abs(_dt.rows - _te[i].rows)+1);
    // Compute final match
    match1 = match1(Rect(0, 0, abs(_dt.cols - _te[i].cols)+1, abs(_dt.rows - _te[i].rows)+1));

    // Bidirectional
    if(_bidirectional) {
		mulSpectrums(_edgesdft, _dtedft[i], match2, 0, true);
		dft(match2, match2, DFT_INVERSE + DFT_SCALE, abs(_dt.rows - _te[i].rows)+1);
		match2 = match2(Rect(0, 0, abs(_dt.cols - _te[i].cols)+1, abs(_dt.rows- _te[i].rows)+1));
    }
#else
    /*
     * WITHOUT FFT
     */
    // Sum of Euclidean distances [px]
    matchTemplate(_dt, _te[i], match1, TM_CCORR);
    matchTemplate(_edges, _dte[i], match2, TM_CCORR);
#endif

    cv::Mat_<float> match = ( _bidirectional ? 0.5 * (match1 + match2) / _te[i].total() : match1 / _te[i].total() );
    
    return match;
}

std::vector<cv::Point> TemplateMatching::nms(const Mat& src, int sz) const {
    COVIS_ASSERT(src.channels() == 1);
    
    // initialise the block mask and destination
    const int M = src.rows;
    const int N = src.cols;
    Mat block = 255*Mat_<uint8_t>::ones(Size(2*sz+1,2*sz+1));
//    dst = Mat_<uint8_t>::zeros(src.size());
//    dst = Mat::zeros(src.size(), src.type());
    std::vector<cv::Point> result;

    // iterate over image blocks
    for (int m = 0; m < M; m+=sz+1) {
        for (int n = 0; n < N; n+=sz+1) {
            Point  ijmax;
            double vcmax, vnmax;

            // get the maximal candidate within the block
            Range ic(m, min(m+sz+1,M));
            Range jc(n, min(n+sz+1,N));
            minMaxLoc(src(ic,jc), NULL, &vcmax, NULL, &ijmax);
            Point cc = ijmax + Point(jc.start,ic.start);

            // search the neighbours centered around the candidate for the true maxima
            Range in(max(cc.y-sz,0), min(cc.y+sz+1,M));
            Range jn(max(cc.x-sz,0), min(cc.x+sz+1,N));

            // mask out the block whose maxima we already know
            Mat_<uint8_t> blockmask;
            block(Range(0,in.size()), Range(0,jn.size())).copyTo(blockmask);
            Range iis(ic.start-in.start, min(ic.start-in.start+sz+1, in.size()));
            Range jis(jc.start-jn.start, min(jc.start-jn.start+sz+1, jn.size()));
            blockmask(iis, jis) = Mat_<uint8_t>::zeros(Size(jis.size(),iis.size()));

            minMaxLoc(src(in,jn), NULL, &vnmax, NULL, &ijmax, blockmask);
//            Point cn = ijmax + Point(jn.start, in.start);
            
//            cc=cn;

            // if the block centre is also the neighbour centre, then it's a local maxima
            if (vcmax > vnmax) {
//                dst.at<uint8_t>(cc.y, cc.x) = 255;
                result.push_back(cc);
//                switch(src.type()) {
//                    case CV_8U:
//                        dst.at<uchar>(cc.y, cc.x) = src.at<uchar>(cc.y, cc.x);
//                        break;
//                    case CV_16U:
//                        dst.at<ushort>(cc.y, cc.x) = src.at<ushort>(cc.y, cc.x);
//                        break;
//                    case CV_32F:
//                        dst.at<float>(cc.y, cc.x) = src.at<float>(cc.y, cc.x);
//                        break;
//                    case CV_64F:
//                        dst.at<double>(cc.y, cc.x) = src.at<float>(cc.y, cc.x);
//                        break;
//                    default:
//                        COVIS_THROW("Unhandled element type for NMS: " << src.type());
//                }
            }
        }
    }
    
    return result;
}
