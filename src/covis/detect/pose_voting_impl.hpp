// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_DETECT_POSE_VOTING_IMPL_HPP
#define COVIS_DETECT_POSE_VOTING_IMPL_HPP

// Own
#include "pose_voting.h"
#include "point_search.h"
#include "grid_search.h"
#include "../core.h"
#include "../filter/non_maximum_suppression.h"

// PCL
#include <pcl/common/centroid.h>

namespace covis {
    namespace detect {
        template<typename PointT>
        core::Detection PoseVoting<PointT>::estimate() {
            // Sanity checks
            COVIS_ASSERT(_source && _target);
            COVIS_ASSERT(_correspondences && !_correspondences->empty());
            COVIS_ASSERT(_bwrot > 0 && _bwrot <= M_PI);
            COVIS_ASSERT(_tessellation > 0 && _tessellation < 2 * M_PI);

            core::ScopedTimer::Ptr tvoting;
            if(_verbose)
                tvoting.reset(new core::ScopedTimer("Computing votes"));

            // The auto case for the translation bandwidth
            if(_bwt <= 0)
                _bwt = 10 * detect::computeResolution<PointT>(_target);

            // Compute votes
            core::Detection::MatrixVecT voteT;
            std::vector<size_t> voteToCorrIdx;
            computeVotes(voteT, voteToCorrIdx); // Uses _correspondences, writes _translationVotes

            if(_translationVotes->empty()) {
                if(_verbose)
                    COVIS_MSG_WARN("No votes! Returning...");
                return core::Detection();
            }

            // Create a voxel-based search object
            detect::GridSearch<PointT> grid(_translationVotes, _bwt, _bwt, _bwt);

            tvoting.reset();

            // Count the size of the voxel which contains each translation vote
            std::vector<size_t> sizes(_translationVotes->size());
            for(size_t idx = 0; idx < _translationVotes->size(); ++idx)
                sizes[idx] = grid.count(idx);

            // Now find the largest-sized vote for each correspondence and keep track of the index
            std::vector<float> sizeCorr(_correspondences->size(), 0); // Per-correspondence max count
            std::vector<size_t> corrToVoteIdx(_correspondences->size()); // Index map correspondence --> max vote
            for(size_t idx = 0; idx < _translationVotes->size(); ++idx) { // Loop over votes
                const size_t cidx = voteToCorrIdx[idx]; // Get index back to correspondence for current vote
                // Update max count for current correspondence
                if(sizes[idx] > sizeCorr[cidx]) {
                    sizeCorr[cidx] = sizes[idx];
                    corrToVoteIdx[cidx] = idx;
                }
            }

//            /// TODO: Keep only the translation votes that are in max-voxels
//            CloudT translationVotesMax(_correspondences->size(), 1);
//            core::Detection::MatrixVecT voteTMax(_correspondences->size());
//            for(size_t i = 0; i < _correspondences->size(); ++i) {
//                translationVotesMax[i] = _translationVotes->points[corrToVoteIdx[i]];
//                voteTMax[i] = voteT[corrToVoteIdx[i]];
//            }
//
//             // Redo indices
//            *_translationVotes = translationVotesMax;
//            voteT = voteTMax;
//            voteToCorrIdx = core::range<size_t>(_correspondences->size());
//            corrToVoteIdx = voteToCorrIdx;

            core::ScopedTimer::Ptr tkde;
            if(_verbose)
                tkde.reset(new core::ScopedTimer("Estimating density using " + core::stringify(_translationVotes->size()) + " votes"));

            // Find Euclidean neighbors
            typename detect::PointSearch<PointT>::Ptr tree(new detect::PointSearch<PointT>(_translationVotes, false));

            // Precompute squared bandwidths
            const float bwtsq = _bwt * _bwt;
            const float bwrotsq = _bwrot * _bwrot;

            // Find SE(3) neighbors and compute KDEs in the process
            _densities.resize(_correspondences->size(), 0.0f);
            std::vector<size_t> votes(_correspondences->size());
            core::ProgressDisplay::Ptr pd;
            if(_verbose)
                pd.reset(new core::ProgressDisplay(_correspondences->size()));

#ifdef _OPENMP
#pragma omp parallel for
#endif
            for(size_t cidx = 0; cidx < _correspondences->size(); ++cidx) {
                float density = 0;
                size_t voters = 0;
                const size_t idx = corrToVoteIdx[cidx]; // Get vote index for correspondence
                const Eigen::Matrix3f &R1t = voteT[idx].topLeftCorner(3, 3).transpose(); // Precomputed transposed rotation
                // Loop over Euclidean neighbors and accumulate density estimate
                const core::Correspondence corrnn = tree->radius(_translationVotes->points[idx], _bwt);
//                const core::Correspondence corrnn = grid.radius(_translationVotes->points[idx], _bwt);
                for(size_t cradidx = 0; cradidx < corrnn.size(); ++cradidx) {
                    // Avoid self
//                    if(corrnn.match[cradidx] == idx)
//                        continue;
//                    // Avoid all votes of self
//                    if(voteToCorrIdx[corrnn.match[cradidx]] == cidx)
//                        continue;
                    // Avoid non-conforming correspondences
                    const core::Correspondence& c = (*_correspondences)[voteToCorrIdx[corrnn.match[cradidx]]];
                    if((voteT[idx].topLeftCorner(3,3) * _source->points[c.query].getVector3fMap() + voteT[idx].col(3).head<3>() - _target->points[c.match[0]].getVector3fMap()).squaredNorm() > bwtsq)
                        continue;

                    // Now find the SO(3) geodesic distance
                    const Eigen::Matrix3f &R2 = voteT[corrnn.match[cradidx]].topLeftCorner(3, 3);
                    const Eigen::Matrix3f RtR = R1t * R2;
                    const float acosarg = (RtR.trace() - 1) / 2;
                    const float angle = (acosarg <= -1 ? M_PI : (acosarg >= 1 ? 0 : acosf(acosarg)));

                    // Finally, if SO(3) distance is also low, we have an SE(3) neighbor
                    if(angle <= _bwrot) {
                        const float distsq = corrnn.distance[cradidx]; // Squared Euclidean
                        density += expf(-0.5 * distsq / bwtsq) * expf(-0.5 * angle * angle / bwrotsq); // Gaussian
                        ++voters;
                    }
                }

                _densities[cidx] = density;
                votes[cidx] = voters;

#pragma omp critical
                if(_verbose)
                    ++(*pd);
            }

            tkde.reset();

            // Output detection: highest scoring pose
            const size_t imax = std::distance(_densities.begin(), std::max_element(_densities.begin(), _densities.end()));
            core::Detection result;
            result.pose = voteT[corrToVoteIdx[imax]];
            result.params["kde"] = _densities[imax];
            result.params["votes"] = votes[imax];

            // Now get only one pose per correspondence
            typename CloudT::Ptr modes(new CloudT(corrToVoteIdx.size(), 1));
            for(size_t idx = 0; idx < corrToVoteIdx.size(); ++idx)
                modes->points[idx] = _translationVotes->points[corrToVoteIdx[idx]];

            // Perform NMS
            _allDetections.clear();
            filter::NonMaximumSuppression<PointT> nms(0.2 * _diag, 0.1);
            nms.setScores(_densities);
            nms.filter(modes);
            const std::vector<bool>& keep = nms.getMask();
            for(size_t idx = 0; idx < modes->size(); ++idx) {
                // Not suppressed, local maximum
                if(keep[idx]) {
                    core::Detection mode;
                    mode.pose = voteT[corrToVoteIdx[idx]];
                    mode.params["kde"] = _densities[idx];
                    mode.params["votes"] = votes[idx];
                    _allDetections.push_back(mode);
                }
            }

            return result;
        }

        template<typename PointT>
        void PoseVoting<PointT>::computeVotes(core::Detection::MatrixVecT& voteT, std::vector<size_t>& voteToCorrIdx) {
            // Demean query to make sure we are voting for translations of the object centroid in the scene
            typename CloudT::Ptr Q(new CloudT);
            Eigen::Vector4f C;
            pcl::compute3DCentroid(*_source, C);
            pcl::demeanPointCloud(*_source, C, *Q);

            // Precompute deltas, rhos and full rotations for the object
            std::vector<float> rhos(Q->size());
            std::vector<float> deltas(Q->size());
            std::vector<Eigen::Matrix3f, Eigen::aligned_allocator<Eigen::Matrix3f> > Ros(Q->size());
#ifdef _OPENMP
#pragma omp parallel for
#endif
            for(size_t idx = 0; idx < _correspondences->size(); ++idx) {
                if((*_correspondences)[idx].empty())
                    continue;

                // Query index
                const size_t iq = (*_correspondences)[idx].query;

                // Get correspondence
                const PointT pq = Q->points[iq];

                // Avoid NaNs
                if(pq.getVector3fMap().hasNaN() || pq.getNormalVector3fMap().hasNaN())
                    continue;

                // Project the object position vector to its normal vector and get the length of projection
                // https://en.wikipedia.org/wiki/Vector_projection#Vector_projection_2
                const float multiplier = pq.getVector3fMap().dot(pq.getNormalVector3fMap()) / pq.getNormalVector3fMap().squaredNorm();
                const Eigen::Vector3f d = multiplier * pq.getNormalVector3fMap();
                // deltas[iq] = d.norm();
               deltas[iq] = multiplier > 0 ? d.norm() : -d.norm();

                // Compute the radial vector and its length
                const Eigen::Vector3f r = pq.getVector3fMap() - d;
                rhos[iq] = r.norm();

                // Get a full rotation for the object side, used in the inner loop below
                Ros[iq].col(2) = pq.getNormalVector3fMap().normalized();
                Ros[iq].col(0) = -r / rhos[iq];
                Ros[iq].col(1) = Ros[iq].col(2).cross(Ros[iq].col(0));
            }

            // Precompute all cosines and sines for the tessellated angles for voting below
            std::vector<double> costheta, sintheta;
            for(double theta = 0.0; theta < 2.0 * M_PI; theta += _tessellation) {
                costheta.push_back(cosf(theta));
                sintheta.push_back(sinf(theta));
            }

            // Compute number of tesselations and number of votes
            const size_t numTes = costheta.size();
            const size_t numVotes = _correspondences->size() * numTes;

            // Preallocate voting results
            _translationVotes.reset(new CloudT(numVotes, 1)); // Votes for translations of the centered model
            voteToCorrIdx.resize(numVotes); // Index map vote --> correspondence
            voteT.resize(numVotes); // Full pose for each vote (non-centered)

            // Cast votes
#ifdef _OPENMP
#pragma omp parallel for
#endif
            for(size_t idx = 0; idx < _correspondences->size(); ++idx) {
                if((*_correspondences)[idx].empty())
                    continue;

                // Get target point
                const PointT pt = _target->points[(*_correspondences)[idx].match[0]];

                // Avoid NaNs
                if(pt.getVector3fMap().hasNaN() || pt.getNormalVector3fMap().hasNaN())
                    continue;

                // Get object data
                const size_t iq = (*_correspondences)[idx].query;
                const float delta = deltas[iq];
                const float rho = rhos[iq];
                const Eigen::Matrix3f& Ro = Ros[iq];

                // Point normal in the scene
                const Eigen::Vector3f n = pt.getNormalVector3fMap().normalized();

                // Now follow the negative scene normal with that length
                const Eigen::Vector3f po = pt.getVector3fMap() - delta * n;

                // Get a random radial vector in the circle in the target scene
                const Eigen::Vector3f rt = rho * n.unitOrthogonal();

                // Precompute cross product between normal and radial vector
                const Eigen::Vector3f ncrossrt = n.cross(rt);

                // Compute the votes, and find the full (non-centered) pose for each vote in the process
                for(size_t idxtes = 0; idxtes < numTes; ++idxtes) {
                    // Rotated radial vector in the scene
                    const Eigen::Vector3f rrot = rt * costheta[idxtes] + ncrossrt * sintheta[idxtes];

                    // Sample an origo and store
                    const Eigen::Vector3f ps = po + rrot;
                    _translationVotes->points[idx * numTes + idxtes].getVector3fMap() = ps;

                    // Get a full rotation for the scene
                    Eigen::Matrix3f Rs;
                    Rs.col(2) = n;
                    Rs.col(0) = rrot.normalized();
                    Rs.col(1) = Rs.col(2).cross(Rs.col(0));

                    // Get a relative rotation
                    const Eigen::Matrix3f R = Rs * Ro.transpose();

                    // Undo the demeaning
                    core::Detection::MatrixT T;
                    T.block<3, 1>(0, 3) = ps - R * C.head<3>();
                    T.topLeftCorner(3, 3) = R;
                    T.row(3) << 0, 0, 0, 1;

                    // Store pose and index
                    voteT[idx * numTes + idxtes] = T;
                    voteToCorrIdx[idx * numTes + idxtes] = idx;
                }
            }
        }
    }
}

#endif
