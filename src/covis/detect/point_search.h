// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_DETECT_POINT_SEARCH_H
#define COVIS_DETECT_POINT_SEARCH_H

// Own
#include "search_base.h"
#include "../core/correspondence.h"
#include "../core/stat.h"

// PCL
#include <pcl/conversions.h>
#include <pcl/PolygonMesh.h>
#include <pcl/common/common.h>

// FLANN
#include <flann/flann.hpp>

namespace covis {
    namespace detect {
        /**
         * @ingroup detect
         * @class PointSearch
         * @brief Convenience class for k-d tree-based low-dimensional search
         * 
         * This class is a simple wrapper around FLANN's k-d tree search class for unordered data.
         * 
         * @note This class returns squared distances
         * 
         * @todo Make this class auto-select between kd-tree and organized search
         * 
         * @tparam PointT point type, must contain XYZ data
         * @tparam DistT distance type, defaults to <b>flann::L2_3D<float></b>
         * @author Anders Glent Buch
         */
        template<typename PointT, typename DistT=flann::L2_3D<float> >
        class PointSearch : public SearchBase<PointT> {
            using SearchBase<PointT>::_target;
            
            public:
                /// Pointer type
                typedef boost::shared_ptr<PointSearch<PointT> > Ptr;
                
                /// Pointer to const type
                typedef boost::shared_ptr<const PointSearch<PointT> > ConstPtr;

                /// Tree type
                typedef typename flann::KDTreeSingleIndex<DistT> TreeT;

                /// Tree instance
                TreeT tree;
                
                /**
                 * Constructor: setup k-d tree
                 * @param sort set to false to disable sorted results
                 * @param leaf set the maximum leaf size of the tree
                 */
                PointSearch(const bool sort = true, size_t leaf = 15) : _mdata(0), _sort(sort), _leaf(leaf) {}

                /**
                 * Constructor: setup k-d tree and index the provided point cloud
                 * @param target target point set
                 * @param sort set to false to disable sorted results
                 * @param leaf set the maximum leaf size of the tree
                 */
                PointSearch(typename pcl::PointCloud<PointT>::ConstPtr target,
                            const bool sort = true,
                            size_t leaf = 15) : _mdata(0), _sort(sort), _leaf(leaf) {
                    SearchBase<PointT>::setTarget(target);
                }
                
                /// Empty destructor
                virtual ~PointSearch() {
                    if(_mdata)
                        delete[] _mdata;
                }

                /**
                 * Set the sorting flag
                 * @param sort sorting flag
                 */
                inline void setSort(bool sort) {
                    _sort = sort;
                }

                /**
                 * Set leaf size
                 * @param leaf leaf size
                 */
                inline void setLeaf(size_t leaf) {
                    _leaf = leaf;
                }
                
                /**
                 * Compute the resolution of the indexed target point set as the mean nearest neighbor distance
                 * @param robust if set to true, use the median instead of the mean
                 * @param k set how many neighbors to use for estimating resolution
                 * @return estimated resolution
                 */
                inline float resolution(bool robust = false, int k = 2) const {
                    COVIS_ASSERT_MSG(_target, "Target not set!");
                    COVIS_ASSERT(k > 0);
                    
                    // All valid distances
                    std::vector<float> dists;
                    
                    // Start
                    for(size_t i = 0; i < _target->size(); ++i) {
                        if(pcl_isfinite((*_target)[i].x) && pcl_isfinite((*_target)[i].y) && pcl_isfinite((*_target)[i].z)) {
                            const core::Correspondence c = this->knn(_target->points[i], k);
                            for(size_t j = 0; j < c.size(); ++j)
                                if(c.match[j] != i)
                                    dists.push_back(sqrtf(c.distance[j]));
                        }
                    }
                    
                    return (robust ? core::median(dists) : core::mean(dists));
                }
                
            private:
                /// @copydoc SearchBase::index()
                void index();
                
                /// @copydoc SearchBase::doKnn()
                void doKnn(const PointT& query, size_t k, core::Correspondence& result) const;
                
                /// @copydoc SearchBase::doRadius()
                void doRadius(const PointT& query, float r, core::Correspondence& result) const;

                /// Temporary copy of the point cloud to index
                float* _mdata;

                /// Set to true (default) to return neighbors sorted by distance
                bool _sort;

                /// Maximum number of points in a leaf
                size_t _leaf;

                /// Resolved point type dimension
                size_t _dim;
        };
        
        /**
         * @ingroup detect
         * @brief Perform a single-shot bulk k-NN search in Euclidean space
         * @note This function returns squared distances
         * @param query query points
         * @param target target points to search into
         * @param k number of neighbors to search for
         * @return matches as correspondences
         */
        template<typename PointT>
        inline core::Correspondence::VecPtr knnSearch(typename pcl::PointCloud<PointT>::ConstPtr query,
                typename pcl::PointCloud<PointT>::ConstPtr target,
                size_t k) {
            PointSearch<PointT> pm;
            pm.setTarget(target);
            
            return pm.knn(query, k);
        }
        
        /**
         * @ingroup detect
         * @brief Perform a single-shot bulk radius search in Euclidean space
         * @note This function returns squared distances
         * @param query query points
         * @param target target points to search into
         * @param r Euclidean search radius
         * @return matches as correspondences
         */
        template<typename PointT>
        inline core::Correspondence::VecPtr radiusSearch(typename pcl::PointCloud<PointT>::ConstPtr query,
                typename pcl::PointCloud<PointT>::ConstPtr target,
                float r) {
            PointSearch<PointT> pm;
            pm.setTarget(target);
            
            return pm.radius(query, r);
        }
        
        /**
         * @ingroup detect
         * @brief Estimate the resolution of a point cloud
         * @param cloud point cloud for which to compute resolution
         * @param robust if true, use a robust measure
         * @param k set how many neighbors to use for estimating resolution
         * @return resolution
         */
        template<typename PointT>
        inline float computeResolution(typename pcl::PointCloud<PointT>::ConstPtr cloud,
                bool robust = false,
                int k = 5) {
            PointSearch<PointT> pm;
            pm.setTarget(cloud);
            
            return pm.resolution(robust, k);
        }

        /**
         * @ingroup detect
         * @brief Estimate the resolution of a mesh
         * @param mesh mesh for which to compute resolution
         * @param robust if true, use a robust measure
         * @param k set how many neighbors to use for estimating resolution - this parameter is only used if no face
         * information is available on the mesh
         * @return resolution
         */
        inline float computeResolution(pcl::PolygonMeshConstPtr mesh,
                                       bool robust = false,
                                       int k = 5) {
            pcl::PointCloud<pcl::PointXYZ>::Ptr tmp(new pcl::PointCloud<pcl::PointXYZ>);
            pcl::fromPCLPointCloud2(mesh->cloud, *tmp);

            if(mesh->polygons.empty()) {
                PointSearch<pcl::PointXYZ> pm;
                pm.setTarget(tmp);

                return pm.resolution(robust, k);
            } else {
                std::vector<float> dists;
                for(size_t i = 0; i < mesh->polygons.size(); ++i) {
                    if(mesh->polygons[i].vertices.size() > 2) {
                        for (size_t j = 0; j < mesh->polygons[i].vertices.size(); ++j) {
                            const pcl::PointXYZ &p = tmp->points[mesh->polygons[i].vertices[j]];
                            pcl::PointXYZ pnext;
                            if(j == mesh->polygons[i].vertices.size() - 1)
                                pnext = tmp->points[mesh->polygons[i].vertices[0]];
                            else
                                pnext = tmp->points[mesh->polygons[i].vertices[j+1]];
                            
                            const float distsq = (pnext.x - p.x) * (pnext.x - p.x) +
                                                 (pnext.y - p.y) * (pnext.y - p.y) +
                                                 (pnext.z - p.z) * (pnext.z - p.z);
                            if(!isnanf(distsq))
                                dists.push_back(sqrtf(distsq));
                        }
                    }
                }

                return (robust ? core::median(dists) : core::mean(dists));
            }
        }

        /**
         * @ingroup detect
         * @brief Compute the length of the diagonal of the bounding box of a point cloud
         * @param cloud point cloud for which to compute resolution
         * @return diagonal
         */
        template<typename PointT>
        inline float computeDiagonal(typename pcl::PointCloud<PointT>::ConstPtr cloud) {
            PointT min, max;
            pcl::getMinMax3D<PointT>(*cloud, min, max);
            const float dx = max.x - min.x;
            const float dy = max.y - min.y;
            const float dz = max.z - min.z;

            return sqrtf(dx * dx + dy * dy + dz * dz);
        }

        /**
         * @ingroup detect
         * @brief Compute the length of the diagonal of the bounding box of a point cloud
         * @param mesh mesh for which to compute resolution
         * @return diagonal
         */
        inline float computeDiagonal(pcl::PolygonMeshConstPtr mesh) {
            pcl::PointCloud<pcl::PointXYZ>::Ptr tmp(new pcl::PointCloud<pcl::PointXYZ>);
            pcl::fromPCLPointCloud2(mesh->cloud, *tmp);

            return computeDiagonal<pcl::PointXYZ>(tmp);
        }
    }
}

#include "point_search_impl.hpp"
    
#endif
