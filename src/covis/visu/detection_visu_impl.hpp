// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_VISU_DETECTION_VISU_IMPL_HPP
#define COVIS_VISU_DETECTION_VISU_IMPL_HPP

#include "detection_visu.h"

// PCL
#include <pcl/point_types.h>
#include <pcl/common/transforms.h>

namespace covis {
    namespace visu {
        template<typename PointT>
        void DetectionVisu<PointT>::render() {
            // Sanity checks
            COVIS_ASSERT(!_queries.empty() || !_queryMeshes.empty());
            COVIS_ASSERT(!_detections.empty());
            
            if(_targetMesh)
                addMesh(_targetMesh, "target");
            else if(_target)
                addPointCloud<PointT>(_target, "target");

            // Compute title offsets, one per object
            std::map<size_t,size_t> offsets;
            size_t cnt = 0;
            size_t numIdx = 0; // Compute for how many different objects we have detections
            for(size_t i = 0; i < _detections.size(); ++i) {
                const size_t idx = _detections[i].idx;
                if(offsets.find(idx) == offsets.end()) { // New index
                    offsets[idx] = cnt++;
                    ++numIdx;
                }
            }
            
            if(!_queryMeshes.empty()) {
                std::vector<pcl::PolygonMesh::Ptr> qt(_detections.size());
                for(size_t i = 0; i < _detections.size(); ++i) {
                    if(!_detections[i])
                        continue;
                    // Get index of detected object
                    const size_t idx = _detections[i].idx;
                    const size_t offset = offsets[idx];
                    // Convert to point cloud (ignore potential color because we use our own colors here)
                    pcl::PointCloud<pcl::PointXYZ> cloudt;
                    pcl::fromPCLPointCloud2(_queryMeshes[idx]->cloud, cloudt);
                    // Transform point cloud
                    pcl::transformPointCloud(cloudt, cloudt, _detections[i].pose);
                    // Copy back
                    qt[i].reset(new pcl::PolygonMesh);
                    qt[i]->header = _queryMeshes[idx]->header;
                    qt[i]->polygons = _queryMeshes[idx]->polygons;
                    pcl::toPCLPointCloud2<pcl::PointXYZ>(cloudt, qt[i]->cloud);
                    // Generate label
                    std::ostringstream oss;
                    oss << "detection_" << i;
                    // Generate color red --> blue
                    double r, g, b;
                    if(_detections.size() == 1) { // Special case: only one detection
                        redgreen(1, r, g, b); // Green
                    } else {
                        if(numIdx == 1) // Multi-instances of the same object
                            redgreen(1.0 - double(i) / double(_detections.size()-1), r, g, b); // Graduated by detection index
                        else
                            redgreen(_queryMeshes.size() == 1 ? 1 : 1.0 - double(idx) / double(_queryMeshes.size() - 1), r, g, b); // Graduated by object index
                    }

                    // Add
                    if(hasCloud(oss.str()))
                        visu.removePointCloud(oss.str());
                    addMesh(qt[i], oss.str());
                    
                    auto actor = getMeshActor(oss.str());
                    if(actor) {
                        actor->GetProperty()->SetColor(r, g, b);
                        actor->GetProperty()->SetOpacity(_opacity);
                    }

                    std::string label = (_detections[i].label.empty() ? oss.str() : _detections[i].label);
                    if(_detections.size() > 1 && numIdx == 1) { // Multi-instances of the same object
                        label = "Detections of " + label;
                        r = 255 - _bg.r;
                        g = 255 - _bg.g;
                        b = 255 - _bg.b;
                    }

                    if(!visu.updateText(label, 5, 5 + 20 * offset, 15, r, g, b, label))
                        visu.addText(label, 5, 5 + 20 * offset, 15, r, g, b, label);
                }
            } else {
                std::vector<typename pcl::PointCloud<pcl::PointXYZ>::Ptr> qt(_detections.size());
                for(size_t i = 0; i < _detections.size(); ++i) {
                    if(!_detections[i])
                        continue;
                    // Get index of detected object
                    const size_t idx = _detections[i].idx;
                    const size_t offset = offsets[idx];
                    // Transform point cloud
                    qt[i].reset(new pcl::PointCloud<pcl::PointXYZ>);
                    pcl::copyPointCloud<PointT,pcl::PointXYZ>(*_queries[idx], *qt[i]);
                    pcl::transformPointCloud<pcl::PointXYZ>(*qt[i], *qt[i], _detections[i].pose);
                    // Generate label
                    std::ostringstream oss;
                    oss << "detection_" << i;
                    // Generate color red --> blue
                    double r, g, b;
                    if(_detections.size() == 1) { // Special case: only one detection
                        redgreen(1, r, g, b); // Green
                    } else {
                        if(numIdx == 1) // Multi-instances of the same object
                            redgreen(1.0 - double(i) / double(_detections.size()-1), r, g, b); // Graduated by detection index
                        else
                            redgreen(_queries.size() == 1 ? 1 : 1.0 - double(idx) / double(_queries.size()) -1, r, g, b); // Graduated by object index
                    }
                    // Add
                    if(hasCloud(oss.str()))
                        visu.removePointCloud(oss.str());
                    addColor<pcl::PointXYZ>(qt[i], 255.0 * r, 255.0 * g, 255.0 * b, oss.str());
                    visu.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_OPACITY, _opacity, oss.str());

                    std::string label = (_detections[i].label.empty() ? oss.str() : _detections[i].label);
                    if(_detections.size() > 1 && numIdx == 1) { // Multi-instances of the same object
                        label = "Detections of " + label;
                        r = 255 - _bg.r;
                        g = 255 - _bg.g;
                        b = 255 - _bg.b;
                    }

                    if(!visu.updateText(label, 5, 5 + 20 * offset, 15, r, g, b, label))
                        visu.addText(label, 5, 5 + 20 * offset, 15, r, g, b, label);
                }
            }
        }
    }
}

#endif
