// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_VISU_CORR_VISU_IMPL_HPP
#define COVIS_VISU_CORR_VISU_IMPL_HPP

// Own
#include "corr_visu.h"
#include "../core/random.h"

// PCL
#include <pcl/common/common.h>
#include <pcl/common/transforms.h>
#include <pcl/visualization/pcl_visualizer.h>

// VTK
#include <vtkLine.h>

namespace covis {
    namespace visu {
        template<typename PointT>
        void CorrVisu<PointT>::show() {
            // Sanity checks
            COVIS_ASSERT_MSG(_query && _target, "Query and target must be set!");
            COVIS_ASSERT_MSG(_corrpcl, "Correspondences not set!");

            // Apply separation, if enabled
            typename pcl::PointCloud<PointT>::Ptr query2(new pcl::PointCloud<PointT>);
            if(_separate) {
                // In case of no overlap, nothing actually happens here, as t will be zero
                pcl::transformPointCloud<PointT>(*_query, *query2, separate(_query, _target), Eigen::Quaternionf::Identity());
            }  else {
                *query2 = *_query;
            }
            
            // Add point clouds and correspondences
            switch(_mode) {
                case LINES: {
                    if (_showPoints) {
                        addPointCloud<PointT>(query2, "query");
                        addPointCloud<PointT>(_target, "target");
                    }

                    // Create the polydata where we will store all the geometric data
                    vtkSmartPointer<vtkPolyData> linesPolyData = vtkSmartPointer<vtkPolyData>::New();

                    // Create a vtkPoints container and store the points in it
                    vtkSmartPointer<vtkPoints> pts = vtkSmartPointer<vtkPoints>::New();
                    for (size_t i = 0; i < _corrpcl->size(); i += _level) {
                        const PointT &p1 = query2->points[(*_corrpcl)[i].index_query];
                        const PointT &p2 = _target->points[(*_corrpcl)[i].index_match];
                        pts->InsertNextPoint(p1.x, p1.y, p1.z);
                        pts->InsertNextPoint(p2.x, p2.y, p2.z);
                    }

                    // Add the points to the polydata container
                    linesPolyData->SetPoints(pts);

                    // Create the lines
                    vtkSmartPointer<vtkCellArray> lines = vtkSmartPointer<vtkCellArray>::New();
                    for (vtkIdType i = 0; i < pts->GetNumberOfPoints(); i += 2) {
                        vtkSmartPointer<vtkLine> line = vtkSmartPointer<vtkLine>::New();
                        line->GetPointIds()->SetId(0, i);
                        line->GetPointIds()->SetId(1, i + 1);
                        lines->InsertNextCell(line);
                    }

                    // Add the lines
                    linesPolyData->SetLines(lines);

                    // Create random colors
                    vtkSmartPointer<vtkUnsignedCharArray> colors = vtkSmartPointer<vtkUnsignedCharArray>::New();
                    colors->SetNumberOfComponents(3);
                    for (vtkIdType i = 0; i < lines->GetNumberOfCells(); ++i) {
                        double rgb[3];
                        jet(double(i) / MAX(1,double(lines->GetNumberOfCells() - 1)), rgb[0], rgb[1], rgb[2]);
                        rgb[0] *= 255;
                        rgb[1] *= 255;
                        rgb[2] *= 255;
                        colors->InsertTuple(i, rgb);
                    }

                    // Add the colors
                    linesPolyData->GetCellData()->SetScalars(colors);

                    // Map the line data to graphics
                    vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();

#if VTK_MAJOR_VERSION <= 5
                    mapper->SetInput(linesPolyData);
#else
                    mapper->SetInputData(linesPolyData);
#endif
                    // Get an actor
                    vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
                    actor->SetMapper(mapper);

                    // Tweak the appearance of the lines
                    actor->GetProperty()->SetOpacity(0.5);
                    actor->GetProperty()->SetLineWidth(1.5);

                    // And finally add to PCL
                    visu.getRenderWindow()->GetRenderers()->GetFirstRenderer()->AddActor(actor);

                    break;
                } case COLORS: {
                    // Find minimal scalar value
                    float smin = FLT_MAX;
                    for(size_t i = 0; i < query2->size(); ++i)
                        if(query2->points[i].z < smin)
                            smin = query2->points[i].z;
                    smin *= (smin < 0.0f ? 1.25f : 0.75f); // Decrease to offset from the rest of the colors
                    // Initialize scalar fields for query/target
                    std::vector<float> fieldQuery(_query->size(), smin);
                    std::vector<float> fieldTarget(_target->size(), smin);
                    // Set scalar fields
                    for(size_t i = 0; i < _corrpcl->size(); ++i)
                        fieldQuery[(*_corrpcl)[i].index_query] =
                                fieldTarget[(*_corrpcl)[i].index_match] =
                                        query2->points[(*_corrpcl)[i].index_query].z;
                    // TODO: Add a dummy point to query in the case that _corrpcl are dense
                    query2->push_back(PointT());
                    fieldQuery.push_back(smin);
                    // Add to visu
                    addScalarField<PointT>(query2, fieldQuery, "query");
                    addScalarField<PointT>(_target, fieldTarget, "target");
                    break;
                } default:
                    COVIS_THROW("Unknown correspondence visualization method!");
            }
            
            // Start
            Visu3D::show();
        }
        
        template<typename PointT>
        Eigen::Vector3f CorrVisu<PointT>::separate(typename pcl::PointCloud<PointT>::ConstPtr query,
                typename pcl::PointCloud<PointT>::ConstPtr target,
                float multiplier) const {
            // Get extremal points, neglecting zeros
            std::vector<int> qindices, tindices;
            for(size_t i = 0; i < query->size(); ++i)
                if(query->points[i].x != 0 && query->points[i].y != 0 && query->points[i].z != 0)
                    qindices.push_back(i);
            for(size_t i = 0; i < target->size(); ++i)
                if(target->points[i].x != 0 && target->points[i].y != 0 && target->points[i].z != 0)
                    tindices.push_back(i);
            Eigen::Vector4f qmin, qmax, tmin, tmax;
            pcl::getMinMax3D<PointT>(*query, qindices, qmin, qmax);
            pcl::getMinMax3D<PointT>(*target, tindices, tmin, tmax);
            
            // If any dimension is overlapping, we move the query points away below
            Eigen::Vector3f t(0, 0, 0);
            if(qmax[2] > tmin[2])
                t[2] = qmax[2] - tmin[2];
            else if(qmax[1] > tmin[1])
                t[1] = qmax[1] - tmin[1];
            else if(qmax[0] > tmin[0])
                t[0] = qmax[0] - tmin[0];
            
            return multiplier * t;
        }
    }
}

#endif
