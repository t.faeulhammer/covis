// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_CORE_CONVERTERS_H
#define COVIS_CORE_CONVERTERS_H

// PCL
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

// Eigen
#include <Eigen/Core>

namespace covis {
    namespace core {
        /**
         * @ingroup core
         *
         * @brief Convert all the data of a point cloud to an Eigen matrix
         * @note Since the storage order for Eigen is column-major, this function places all observations in columns
         * @warning This function will return bogus if the point type does not contain all float members
         *
         * @param cloud point cloud
         * @return Eigen matrix, with points stored in columns
         * @tparam point type
         */
        template<typename PointT>
        Eigen::MatrixXf map(const pcl::PointCloud<PointT>& cloud);

        /**
         * @ingroup core
         *
         * @brief Copy the columns of an Eigen matrix into the data of a point cloud
         * @note Since the storage order for Eigen is column-major, this function reads all observations in columns
         *
         * @param m Eigen matrix, with points stored in columns
         * @return point cloud
         * @tparam point type
         */
        template<typename PointT>
        pcl::PointCloud<PointT> map(const Eigen::MatrixXf& m);

        /**
         * @ingroup core
         *
         * @brief Convert the XYZ data of a point cloud to an Eigen matrix
         * @note Since the storage order for Eigen is column-major, this function places all observations in columns
         *
         * @param cloud point cloud
         * @return Eigen matrix, with points stored in columns
         * @tparam point type with members x, y and z
         */
        template<typename PointT>
        Eigen::MatrixXf mapxyz(const pcl::PointCloud<PointT>& cloud);

        /**
         * @ingroup core
         *
         * @brief Copy the columns of an Eigen matrix into the XYZ data of a point cloud
         * @note Since the storage order for Eigen is column-major, this function reads all observations in columns
         *
         * @param m Eigen matrix, with points stored in columns
         * @return point cloud
         * @tparam point type with members x, y and z
         */
        template<typename PointT>
        pcl::PointCloud<PointT> mapxyz(const Eigen::MatrixXf& m);

        /**
         * @ingroup core
         *
         * @brief Convert the descriptor data of a point cloud to an Eigen matrix
         * @note Since the storage order for Eigen is column-major, this function places all observations in columns
         * @warning This function will return bogus if the point type does not contain all float members
         *
         * @param cloud point cloud
         * @return Eigen matrix, with features stored in columns
         * @tparam feature type
         */
        template<typename FeatureT>
        Eigen::MatrixXf mapfeat(const pcl::PointCloud<FeatureT>& cloud);

        /**
         * @ingroup core
         *
         * @brief Copy the columns of an Eigen matrix into the descriptor data of a point cloud
         * @note Since the storage order for Eigen is column-major, this function reads all observations in columns
         *
         * @param m Eigen matrix, with features stored in columns
         * @return point cloud
         * @tparam feature type
         */
        template<typename FeatureT>
        pcl::PointCloud<FeatureT> mapfeat(const Eigen::MatrixXf& m);
        
        /**
         * Convert a point and an RF to a 4-by-4 homogeneous transformation matrix
         * @param p point
         * @param rf RF
         * @return matrix
         */
        template<typename PointT, typename RFT>
        Eigen::Matrix4f maprf(const PointT& p, const RFT& rf);
        
        /**
         * Convert a point and an RF to a 4-by-4 homogeneous transformation matrix representing the inverse
         * @param p point
         * @param rf RF
         * @return matrix inverse
         */
        template<typename PointT, typename RFT>
        Eigen::Matrix4f maprfi(const PointT& p, const RFT& rf);
    }
}

#include "converters_impl.hpp"

#endif