// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_CORE_PRIMITIVE_2D_H
#define COVIS_CORE_PRIMITIVE_2D_H

// Own
#include "primitive.h"
#include "ids.h"

// PCL
#include <pcl/point_types.h>

/// structure for representing magnitude and orientation
struct Gradient {
        Gradient(double m, double o){
            this->m = m;
            this->o = o;
        }
        double m, o;

};

// AGB: I moved the Ids struct to ids.h, and registered it with PCL

namespace covis {
    namespace core {
        /**
         * @class Primitive
         * @ingroup core
         * @brief Primitive base class
         *
         * TODO: Explain and give examples
         *
         * @author
         */
        class Primitive2D : public core::Primitive {
            public:
                /**
                 * Parameter constructor
                 * @param ids intrinsic dimensions
                 * @param grad magnitude and orientation
                 */
                Primitive2D(const Ids& ids, const Gradient& grad) : _ids(ids), _gradient(grad) {}
                /// Empty constructor
                Primitive2D() : _ids(0,0,0), _gradient(0,0) {}
                /// Empty destructor
                virtual ~Primitive2D() {}

                /// Public coordinate x
                int x;
                /// Public coordinate y
                int y;

                /// Public color
                PCL_ADD_RGB;

                /**
                 * Get intrinsic dimensions
                 * @return ids intrinsic dimensions (i0D, i1D and i2D)
                 */
                inline Ids getIds() {
                    return this->_ids;
                }
                /**
                 * Get magnitude and orientation
                 * @return gradient magnitude and orientation
                 */
                inline Gradient getGradient() {
                    return this->_gradient;
                }


            private:
                /// Intrinsic dimensions
                Ids _ids;
                /// Magnitude and orientation
                Gradient _gradient;
        };
    }
}

#endif /* COVIS_CORE_PRIMITIVE_2D_H */
