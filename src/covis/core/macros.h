// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_CORE_MACROS_H
#define COVIS_CORE_MACROS_H

// STL
#include <cstdlib>
#include <exception>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>

/*
 * Message macros
 */

/**
 * @ingroup core
 * @brief Print a message to standard output
 */
#define COVIS_MSG(ostreamexp) std::cout << "\033[1m[COVIS]\033[0m " << ostreamexp << std::endl

/**
 * @ingroup core
 * @brief Print an important message to standard output
 */
#define COVIS_MSG_INFO(ostreamexp) COVIS_MSG("\033[0;32m" << ostreamexp << "\033[0m")

/**
 * @ingroup core
 * @brief Print a warning message to standard error output
 */
#define COVIS_MSG_WARN(ostreamexp) COVIS_MSG("\033[0;33m" << ostreamexp << "\033[0m")

/**
 * @ingroup core
 * @brief Print an error message to standard error output
 */
#define COVIS_MSG_ERROR(ostreamexp) COVIS_MSG("\033[1;31m" << ostreamexp << "\033[0m")

/**
 * Timing macro
 */
#include "scoped_timer.h"

/**
 * @ingroup core
 * @brief Use @ref covis::core::ScopedTimer "ScopedTimer" to time a command
 */
#define COVIS_TIME(cmd) { core::ScopedTimer t; cmd }

/*
 * Exception macro
 */

/// @cond
namespace covis {
    namespace core {
        void terminate();
    }
}
/// @endcond

/**
 * @ingroup core
 * @brief Throw an exception with a message to standard output
 */
#define COVIS_THROW(ostreamexp) \
do {\
    std::stringstream tmp;\
    tmp << __FILE__ << ":" << __LINE__ << ": " << ostreamexp;\
    COVIS_MSG_ERROR(tmp.str());\
    std::set_terminate(covis::core::terminate);\
    throw std::runtime_error(tmp.str());\
} while(false)

/*
 * Assertion macros
 */

/**
 * @ingroup core
 * @brief Assert a condition is met, otherwise throw an exception with a specified message
 */
#define COVIS_ASSERT_MSG(condition, ostreamexp) if(!(condition)) COVIS_THROW(ostreamexp)

/**
 * @ingroup core
 * @brief Assert a condition is met, otherwise throw an exception with a standard message
 */
#define COVIS_ASSERT(condition) COVIS_ASSERT_MSG(condition, "assertion '" << #condition << "' failed!")

#ifdef NDEBUG

/**
 * @ingroup core
 * @brief (Empty during release builds)
 */
#define COVIS_ASSERT_DBG(condition) 

/**
 * @ingroup core
 * @brief (Empty during release builds)
 */
#define COVIS_ASSERT_DBG_MSG(condition, ostreamexp) 

#else // NDEBUG

/**
 * @ingroup core
 * @brief Assert a condition is met, otherwise throw an exception with a standard message
 */
#define COVIS_ASSERT_DBG(condition) COVIS_ASSERT(condition)

/**
 * @ingroup core
 * @brief Assert a condition is met, otherwise throw an exception with a specified message
 */
#define COVIS_ASSERT_DBG_MSG(condition, ostreamexp) COVIS_ASSERT_MSG(condition, ostreamexp)

#endif // NDEBUG

#endif
