// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_CORE_GRAPH_H
#define COVIS_CORE_GRAPH_H

#include "../detect/point_search.h"

// STL
#include <queue>
#include <stack>

/// @cond
namespace {
    typedef std::pair<size_t, size_t> bdfspair;
    
    inline const bdfspair& next(const std::queue<bdfspair>& q) {
        return q.front();
    }

    inline const bdfspair& next(const std::stack<bdfspair>& q) {
        return q.top();
    }
    
    template<typename PointT, typename Visitor, bool bfs>
    std::vector<size_t> bdfs(pcl::PointCloud<PointT>& cloud,
                             //                 float radius,
                             size_t k,
                             Visitor& visit,
                             size_t seed,
                             size_t max) {
        // Sanity checks
        COVIS_ASSERT(!cloud.empty());
        COVIS_ASSERT(seed < cloud.size());
//            COVIS_ASSERT(radius > 0.0f);
        COVIS_ASSERT(k > 0);

        if(max == 0)
            max = cloud.size();

        // Setup a search index
        covis::detect::PointSearch<PointT> ps(cloud.makeShared(), true);

        std::vector<bool> visited(cloud.size(), false);
        std::vector<size_t> path;

        // Initialize objects for search - each element an index of <parent, child>
        typedef typename std::conditional<bfs, std::queue<bdfspair>, std::stack<bdfspair>>::type BDFSQueue;
        BDFSQueue q;

        // Mark seed visited and add to queue as parent of itself
        visited[seed] = true;
        q.push(std::make_pair(seed, seed));

        // While container is non-empty
        while(!q.empty() && path.size() < max) {
            // Get a const ref to next element
            const bdfspair& t = next(q);

            // Unvisited vertex and its parent
            const size_t v = t.second; // Child index

            // Visitor function invocation
            visit(t.first, t.second);
            path.push_back(t.second);

            // Remove top
            q.pop();

            // Find children
//                const core::Correspondence& corr = ps.radius(cloud[v], radius);
            const covis::core::Correspondence& corr = ps.knn(cloud[v], k);

            // Loop over edges
            for (size_t i = 0; i < corr.match.size(); ++i) {
                // Get neighbor vertex (child of v)
                const size_t u = corr.match[i];
                // If neighbor not previously visited
                if (!visited[u]) {
                    // Mark neighbor as visited and add to queue
                    visited[u] = true;
                    q.push(std::make_pair(v, u));
                }
            }
        } // End while container is non-empty

        return path;
    }
}
/// @endcond

namespace covis {
    namespace core {
        template<typename PointT, typename Visitor>
        std::vector<size_t> bfs(pcl::PointCloud<PointT>& cloud,
                                //                 float radius,
                                size_t k,
                                Visitor& visit,
                                size_t seed = 0,
                                size_t max = 0) {
            return bdfs<PointT,Visitor,true>(cloud, k, visit, seed, max);
        }


        template<typename PointT, typename Visitor>
        std::vector<size_t> dfs(pcl::PointCloud<PointT>& cloud,
                                //                 float radius,
                                size_t k,
                                Visitor& visit,
                                size_t seed = 0,
                                size_t max = 0) {
            return bdfs<PointT,Visitor,false>(cloud, k, visit, seed, max);
        }
    }
}

#endif
