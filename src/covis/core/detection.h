// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE

#ifndef COVIS_CORE_DETECTION_H
#define COVIS_CORE_DETECTION_H

// Own
#include "macros.h"

// STL
#include <limits>
#include <map>
#include <vector>

// Eigen
#include <Eigen/Core>
#include <Eigen/StdVector>

namespace covis {
    namespace core {
        /**
         * @ingroup core
         * @class Detection
         * @brief Structure for representing a single object detection
         *
         * This struct has three types of quality measures of a detection:
         *   - Inlier fraction: the number of inlier object points relative to the total number of points
         *   - Outlier fraction: the number of outlier object points relative to the total number of points
         *   - RMSE: the root mean square error of the inlier points
         *   - Penalty: an internal error function used for ranking detections
         *  
         * @sa @ref detect::FitEvaluation, @ref detect::Ransac, @ref detect::PoseVoting, @ref detect::Recognition
         * @author Anders Glent Buch
         */
        struct Detection {
                /// Matrix type
                typedef Eigen::Matrix<float,4,4,Eigen::DontAlign> MatrixT;

                /// Matrix vector type used by this object
                typedef std::vector<MatrixT> MatrixVecT;
                
                /// Vector type
                typedef std::vector<Detection> Vec;
                
                /// Extra user-specifiable parameters
                std::map<std::string, float> params;
                
                /**
                 * Constructor for setting all members
                 * @param pose pose, set to all zeros
                 * @param rmse RMSE, set to infinite
                 * @param penalty penalty, set to infinite
                 * @param inlierfrac inlier fraction, set to 0
                 * @param outlierfrac outlier fraction, set to 1
                 * @param idx index
                 * @param label label
                 */
                Detection(const MatrixT& pose = MatrixT::Zero(),
                        float rmse = std::numeric_limits<float>::max(),
                        float penalty = std::numeric_limits<float>::max(),
                        float inlierfrac = 0,
                        float outlierfrac = 1,
                        int idx = 0,
                        const std::string& label = "") {
                    this->pose = pose;
                    this->rmse = rmse;
                    this->penalty = penalty;
                    this->inlierfrac = inlierfrac;
                    this->outlierfrac = outlierfrac;
                    this->idx = idx;
                    this->label = label;
                }
                
                /// Alignment pose
                MatrixT pose;
                
                /// RMSE of the alignment
                float rmse;
                
                /// Penalty function evaluation of the fit, can be equal to RMSE
                float penalty;
                
                /// Inlier fraction of the detected object
                float inlierfrac;
                
                /// Outlier fraction of the detected object
                float outlierfrac;
                
                /// Index of the detected object
                int idx;
                
                /// Name of the detected object
                std::string label;

                /**
                 * Set the pose of this class to zero
                 */
                inline void clear() {
                    pose = MatrixT::Zero();
                }
                
                /**
                 * Return true if this detection has a non-zero pose
                 * This can be used for easy check of a detection:
                 * @code
                 * Detection d;
                 * if(d)
                 *   // Do something...
                 * @endcode
                 */
                inline operator bool() const {
                    return !pose.isZero();
                }
        };

        /**
         * @ingroup core
         *
         * Sort a vector of detections in ascending order according to penalty
         *
         * @param d input/output detections
         */
        void sort(Detection::Vec& d);

        /**
         * @ingroup core
         *
         * Sort a vector of detections according to a named parameter
         * @note The named parameter must appear in all detections, otherwise this function will throw an exception
         *
         * @param d input/output detections
         * @param param parameter name to sort by
         * @param descend set to true for descending sort, false for ascending sort
         */
        void sort(Detection::Vec& d, const std::string param, bool descend);
        
        /**
         * @ingroup core
         * 
         * Print a detection to a stream
         * 
         * @param os stream to print to
         * @param d detection
         * @return modified stream
         */
        std::ostream& operator<<(std::ostream& os, const Detection& d);
    }
}

#endif
