// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

// Own
#include "stat.h"
#include "macros.h"
#include "progress_display.h"

// STL
#include <algorithm>
#include <cmath>

// Eigen
#include <eigen3/Eigen/Dense>

double covis::core::median(const cv::Mat& m) {
    cv::Mat tmp = ( m.isContinuous() ? m.reshape(1, 1) : m.clone().reshape(1, 1) ); // Reshape to 1 channel and 1 row
    std::vector<double> vtmp;
    tmp.copyTo(vtmp);
    
    return median<double>(vtmp);
}

void covis::core::firstCombination(size_t item[], size_t n) {
    for (size_t i = 0; i < n; ++i)
        item[i] = i;
}

bool covis::core::nextCombination(size_t item[], size_t k, size_t n)  {
    COVIS_ASSERT(k <= n);
    for (size_t i = 1; i <= k; ++i) {
        if (item[k-i] < n-i) {
            ++item[k-i];
            for (size_t j = k-i+1; j < k; ++j)
                item[j] = item[j-1] + 1;

            return true;
        }
    }

    return false;
}

covis::core::PCA covis::core::pcaTrain(const Eigen::MatrixXf& m, float energy) {
    COVIS_ASSERT(energy > 0 && energy <= 1);

    const Eigen::VectorXf c = m.rowwise().mean();
    const Eigen::MatrixXf mc = m.colwise() - c;
    const Eigen::MatrixXf mcov = mc * mc.transpose();
    Eigen::SelfAdjointEigenSolver<Eigen::MatrixXf> eig(mcov);

    if(energy == 1)
        return PCA(c, eig.eigenvectors());

    Eigen::VectorXf values = eig.eigenvalues(); // Sorted increasing
    values /= values.sum();
    float venergy = 0;
    int i = values.size() - 1;
    while(i >= 0) {
        venergy += values(i);
        if(venergy >= energy)
            break;

        --i;
    }

    Eigen::MatrixXf vectors = eig.eigenvectors();

    return PCA(c, vectors.rightCols(values.size() - i));
}

Eigen::MatrixXf covis::core::pcaProject(const Eigen::MatrixXf& m, const PCA& pca, size_t components) {
    if(components == 0)
        return pca.second.transpose() * (m.colwise() - pca.first);
    else
        return pca.second.rightCols(components).transpose() * (m.colwise() - pca.first);
}

#if EIGEN_VERSION_AT_LEAST(3,3,2)
// Algorithm here: https://en.wikipedia.org/wiki/Non-negative_matrix_factorization
covis::core::NNMF covis::core::nnmfTrain(const Eigen::MatrixXf& m, size_t components, size_t iterations, bool verbose) {
    COVIS_ASSERT(components > 0);
    COVIS_ASSERT(iterations > 0);

    // Create our Spectra matrix with randomized initial values
    Eigen::MatrixXf W = Eigen::MatrixXf::Random(m.rows(), components).array().abs();

    // Create our Gain matrix with randomized initial values
    Eigen::MatrixXf H = Eigen::MatrixXf::Random(components, m.cols()).array().abs();

    // Run the update rules
    for(size_t i = 0; i < iterations; i++) {
        const Eigen::MatrixXf Wt = W.transpose();
        H = H.array() * (Wt * m).array() / ( (Wt * W * H).array() + 1e-5f );

        const Eigen::MatrixXf Ht = H.transpose();
        W = W.array() * (m * Ht).array() / ( (W * H * Ht).array() + 1e-5f );

        if(verbose)
            COVIS_MSG("Iteration " << i + 1 << ": " << (m - W * H).norm() / sqrtf(m.size()));
    }

    return NNMF(W, H);
}

Eigen::MatrixXf covis::core::nnmfProject(const Eigen::MatrixXf& m, const NNMF& nnmf) {
    const Eigen::MatrixXf inv = Eigen::CompleteOrthogonalDecomposition<Eigen::MatrixXf>(nnmf.first).pseudoInverse();
    return inv * m;
}
#endif

size_t covis::core::otsu(const size_t* hist, size_t bins) {
    // Sanity check
    COVIS_ASSERT(bins > 0);

    // Sum and mean of histogram bin values
    float sum = 0.0f;
    float meann = 0.0f;
    for(size_t x = 0; x < bins; ++x) {
        sum += float(hist[x]);
        meann += float(x * hist[x]);
    }
    meann /= sum;

    // Partial forward/backward sums of histogram probabilities
    float omega1 = 0.0f;
    float omega2 = 1.0f;

    // Partial forward/backward sums of histogram probabilities weighted by bin values
    float px1 = 0.0f;
    float px2 = meann;

    // Maximal inter-class variance
    float interClassVarianceMax = 0.0f;

    // Resulting threshold
    size_t t = 0;

    // Start
    for(size_t x = 0; x < bins; ++x) {
        // Bin probability
        const float pi = float(hist[x]) / sum;

        // Update class probabilities
        omega1 += pi;
        if(omega1 <= 0.0f || omega1 >= 1.0f)
            continue;

        omega2 -= pi;
        if(omega2 <= 0.0f || omega2 >= 1.0f)
            continue;

        // Update partial sums
        const float pxi = pi * x;
        px1 += pxi;
        px2 -= pxi;

        // Compute class means
        const float mu1 = px1 / omega1;
        const float mu2 = px2 / omega2;

        // Compute inter-class variance
        const float mu12sq = (mu1 - mu2) * (mu1 - mu2);
        const float interClassVariance = omega1 * omega2 * mu12sq;

        // Update results if inter-class variance is maximized
        if(interClassVariance > interClassVarianceMax) {
            interClassVarianceMax = interClassVariance;
            t = x;
        }
    }

    return t;
}
