// Copyright (c) 2016, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_CORE_SORTABLEPAIR_IMPL_HPP
#define COVIS_CORE_SORTABLEPAIR_IMPL_HPP

#include "sortable_pair.h"

namespace covis {
	namespace core {

		template<typename T>
		bool pairCompare ( const pair<unsigned int, T>& first, const pair<unsigned int, T>& second ) {
			return first.second > second.second;
		}

		template<typename T> 
		SortablePair<T>::SortablePair() {
			this->index = 0;
			this->sorted = false;
		}

		template<typename T>
		bool SortablePair<T>::add ( T value ) {
			if ( this->sorted ) {
				return false;
			}

			this->values.push_back ( make_pair ( this->index++, value ) );
			return true;
		}

		template<typename T>
		bool SortablePair<T>::sort() {

			if ( !this->sorted ) {
				std::sort ( this->values.begin(), this->values.end(), pairCompare<T> );
				this->sorted = true;
				return true;
			}
			return false;
		}

		template<typename T>
		vector<T> SortablePair<T>::getValues() {
			vector<T> vals;

			for ( typename vector<pair<unsigned int, T> >::iterator it = this->values.begin() ; it != this->values.end(); ++it )  {
				vals.push_back ( it->second );
			}

			return vals;
		}

		template<typename T>
		vector<T> SortablePair<T>::getOriginalIndexes() {
			vector<T> vals;

			for ( typename vector<pair<unsigned int, T> >::iterator it = this->values.begin() ; it != this->values.end(); ++it )  {
				vals.push_back ( it->first );
			}

			return vals;
		}

		template<typename T>
		vector<pair<unsigned int, T> > SortablePair<T>::getPairs() {
			return this->values;
		}

		template<typename T>
		void SortablePair<T>::printValues() {
			bool first = true;
			for ( typename vector<pair<unsigned int, T> >::iterator it = this->values.begin() ; it != this->values.end(); ++it )  {
				if ( !first ) {
					cout << ", ";
				} else {
					first = false;
				}
				cout << "(" << it->first << ", " << it->second << ")";
			}
			cout << endl;
		}
	}
}

#endif