// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE

#include "macros.h"
#include "correspondence.h"
#include "range.h"

// STL
#include <fstream>

// PCL
#include "pcl/correspondence.h"


bool covis::core::cmpCorrDistAscend(const Correspondence& c1, const Correspondence& c2) {
    COVIS_ASSERT(c1.size() >= 1 && c2.size() >= 1);
    return c1.distance[0] < c2.distance[0];
}

void covis::core::sort(Correspondence::Vec& corr) {
   std::sort(corr.begin(), corr.end(), cmpCorrDistAscend);
}

pcl::CorrespondencesPtr covis::core::convert(const Correspondence::Vec& corr) {
    pcl::CorrespondencesPtr result(new pcl::Correspondences);
    result->reserve(corr.size());
    for(size_t i = 0; i < corr.size(); ++i)
        for(size_t j = 0; j < corr[i].size(); ++j)
            result->push_back(pcl::Correspondence(corr[i].query, corr[i].match[j], corr[i].distance[j]));

    return result;
}

covis::core::Correspondence::VecPtr covis::core::flatten(const Correspondence::Vec& corr) {
    core::Correspondence::VecPtr result(new core::Correspondence::Vec);
    result->reserve(corr.size());
    for(size_t i = 0; i < corr.size(); ++i)
        for(size_t j = 0; j < corr[i].size(); ++j)
            result->push_back(core::Correspondence(corr[i].query, corr[i].match[j], corr[i].distance[j]));

    return result;
}

covis::core::Correspondence::VecPtr covis::core::invert(const Correspondence::Vec& corr) {
    const covis::core::Correspondence::Vec flat = *flatten(corr);
    core::Correspondence::VecPtr result(new core::Correspondence::Vec);
    result->reserve(flat.size());
    for(size_t i = 0; i < flat.size(); ++i)
        if(flat[i])
            result->push_back(core::Correspondence(flat[i].match[0], flat[i].query, flat[i].distance[0]));

    return result;
}

covis::core::Correspondence::VecPtr covis::core::unique(const Correspondence::Vec& corr, size_t k) {
    covis::core::Correspondence::VecPtr flat = flatten(corr);
    if(flat->empty())
        return flat;

    size_t qsize = 0;
    for(size_t i = 0; i < flat->size(); ++i)
        if((*flat)[i]) // Check for empty
            if((*flat)[i].query > qsize)
                qsize = (*flat)[i].query;
    ++qsize;

    std::vector<int> queries(flat->size(), -1);
    std::vector<std::vector<float>> distances(qsize);
    std::vector<std::vector<size_t>> matches(qsize);
    for(size_t i = 0; i < flat->size(); ++i) {
        if((*flat)[i]) { // Check for empty
            const int iq = (*flat)[i].query;
            const float dq = (*flat)[i].distance[0];

            queries[i] = iq;
            distances[iq].push_back(dq);
            matches[iq].push_back((*flat)[i].match[0]);
        }
    }

    core::Correspondence::VecPtr result(new core::Correspondence::Vec());
    std::vector<bool> visited(qsize, false);
    for(size_t i = 0; i < flat->size(); ++i) {
        const int iq = queries[i];
        if(iq >= 0 && !visited[iq]) {
            const std::vector<size_t> order = core::sort<float>(distances[iq], true);
            matches[iq] = core::reorder(matches[iq], order);
            distances[iq].resize(k);
            matches[iq].resize(k);
            result->push_back(core::Correspondence(iq, matches[iq], distances[iq]));
            visited[iq] = true;
        }
    }

    return flatten(*result);
}

covis::core::Correspondence::VecPtr covis::core::computeAllCorrespondences(size_t numQuery,
                                                                           size_t numTarget,
                                                                           float distance) {
    core::Correspondence::VecPtr result(new core::Correspondence::Vec(numQuery * numTarget));
    size_t idx = 0;
    for(size_t i = 0; i < numQuery; ++i)
        for(size_t j = 0; j < numTarget; ++j)
            (*result)[idx++] = core::Correspondence(i, j, distance);

    return result;
}

std::ostream& covis::core::operator<<(std::ostream& os, const Correspondence& corr) {
    const size_t size = corr.size();
    if (size == 0) {
        os << "(Empty correspondence)";
    } else {
        os << "Correspondence of size " << size << ":" << std::endl;
        
        os << "\tQuery: " << corr.query << std::endl;
        
        if (size == 1) {
            os << "\tMatch (index, distance): (" << corr.match[0] << ", " << corr.distance[0] << ")";
        } else {
            os << "\tMatches (index, distance):" << std::endl;
            for (size_t i = 0; i < size - 1; ++i)
                os << "\t\t(" << corr.match[i] << ", " << corr.distance[i] << ")" << std::endl;
            os << "\t\t(" << corr.match[size-1] << ", " << corr.distance[size-1] << ")";
        }
    }
    
    return os;
}

void covis::core::save(const std::string& filename, const Correspondence::Vec& corr) {
    std::ofstream ofs(filename.c_str());
    for(Correspondence::Vec::const_iterator it = corr.begin(); it != corr.end(); ++it) {
        COVIS_ASSERT(ofs);
        ofs << it->size() << " " << it->query << " ";
        for(size_t i = 0; i < it->size(); ++i)
            ofs << it->match[i] << " ";
        for(size_t i = 0; i < it->size(); ++i)
            ofs << it->distance[i] << " ";
        ofs << std::endl;
    }
    COVIS_ASSERT(ofs);
}

void covis::core::load(const std::string& filename, Correspondence::Vec& corr) {
    corr.clear();
    std::ifstream ifs(filename.c_str());
    while(true) {
        COVIS_ASSERT(ifs);
        size_t size;
        ifs >> size;
        Correspondence c(size);
        ifs >> c.query;
        for(size_t i = 0; i < size; ++i)
            ifs >> c.match[i];
        for(size_t i = 0; i < size; ++i)
            ifs >> c.distance[i];
        if(ifs.eof())
            break;
        else
            corr.push_back(c);
    }
}
