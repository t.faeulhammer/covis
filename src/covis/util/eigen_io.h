// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_UTIL_EIGEN_IO_H
#define COVIS_UTIL_EIGEN_IO_H

// Eigen
#include <eigen3/Eigen/Core>

namespace covis {
    namespace util {
        /**
         * @class EigenIO
         * @ingroup util
         * @brief Class for reading/writing Eigen matrices from/to ASCII or binary files
         *
         *
         * @tparam Derived Eigen matrix type, e.g. @b Eigen::Matrix4f or @b Eigen::Vector3f
         * @author Anders Glent Buch
         */
        template<typename Derived=Eigen::MatrixXf>
        class EigenIO {
            public:
                /// Matrix type for this instance
                typedef Derived MatrixT;

                /**
                 * Constructor: set default parameters
                 * @param rowMajor row-major format
                 * @param binary read from binary files
                 * @param append append to file during saving
                 */
                EigenIO(bool rowMajor = true, bool binary = false, bool append = false) :
                        _rowMajor(rowMajor),
                        _binary(binary),
                        _append(append) {}

                /**
                 * Load the contents of a data file into a matrix
                 * If the matrix is initialized to a certain size, this dictates the number of elements that will be
                 * read from the file
                 * On the other hand, if the matrix is uninitialized, the number of rows/columns is found using
                 * @ref core::rowscols()
                 * @param filepath file to read
                 * @param m Eigen matrix to load into
                 * @return Eigen matrix
                 */
                void load(const std::string& filepath, Eigen::MatrixBase<Derived>& m) const;

                /**
                 * Save a matrix to a data file
                 * @param filepath output file
                 * @param m Eigen matrix to save from
                 */
                void save(const std::string& filepath, const Eigen::MatrixBase<Derived>& m) const;

                /**
                 * Set the row-major flag
                 * @param rowMajor row-major flag
                 */
                inline void setRowMajor(bool rowMajor) {
                        _rowMajor = rowMajor;
                }

                /**
                 * Set the binary flag
                 * @param binary binary flag
                 */
                inline void setBinary(bool binary) {
                        _binary = binary;
                }
                
            private:
                /// Set to true (default) to read data in row-major
                bool _rowMajor;

                /// Set to true (not default) to read from binary files instead of ASCII files
                bool _binary;

                /// Set to true (not default) to append to file during saving
                bool _append;
        };

        /**
         * @ingroup util
         * @brief Read an Eigen matrix of any type from a raw data file
         * @note This function reads the number of elements in the matrix object, so using e.g. @b Eigen::Matrix4f
         * causes this function to read 16 elements. If the matrix is uninitialized, as for e.g. @b Eigen::MatrixXf, the
         * function @ref rowscols() is used to count the number of elements in the file, causing this function to be
         * slower. This, however, only works for ASCII files.
         * @note The data file cannot contain anything else than numeric information
         * @param filepath input file path
         * @param m output matrix
         * @param rowMajor if true (default), assume file data is stored in row-major format, i.e. each line of the file
         * is read into each row into the output
         * @param binary if true, read from a binary file
         * @exception an exception is thrown if the file fails to read
         */
        template<typename Derived>
        inline void loadEigen(const std::string& filepath,
                              Eigen::MatrixBase<Derived>& m,
                              bool rowMajor = true,
                              bool binary = false) {
                EigenIO<Derived> eio(rowMajor, binary);
                eio.load(filepath, m);
        }

        /**
         * @ingroup util
         * @brief Write an Eigen matrix of any type to a raw data file
         * @param filepath input file path
         * @param m input matrix
         * @param rowMajor if true (default), write data in row-major format, i.e. each row into the input is written
         * into each line of the file
         * @param binary if true, write to a binary file
         * @param append if true, append data to an existing file
         * @exception an exception is thrown if the file fails to write
         */
        template<typename Derived>
        void saveEigen(const std::string& filepath,
                       const Eigen::MatrixBase<Derived>& m,
                       bool rowMajor = true,
                       bool binary = false,
                       bool append = false) {
                EigenIO<Derived> eio(rowMajor, binary, append);
                eio.save(filepath, m);
        }
    }
}

#include "eigen_io_impl.hpp"

#endif
