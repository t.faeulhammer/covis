// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

// Own
#include "dataset_loader.h"

// Boost
#include <boost/filesystem.hpp>
#include <boost/regex.hpp>
namespace fs=boost::filesystem;

// STL
#include <iomanip>
using namespace std;

// PCL
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>

// Covis
#include "ply_loader.h"
#include "../core/range.h"
#include "../util/eigen_io.h"

namespace {
    std::vector<std::string> findPaths(const fs::path& path,
                                       const std::string& extensions,
                                       const std::string& regex = "") {
        // First find a matching extension
        std::vector<std::string> extCandidates;
        boost::split(extCandidates, extensions, boost::is_any_of("  \t\n,;:|"), boost::token_compress_on);
        std::string extFound;
        for(auto ext : extCandidates) {
            for(fs::directory_iterator it(path); it != fs::directory_iterator(); ++it) {
                const fs::path pit = it->path();
                if(boost::regex_search(pit.filename().string(), boost::regex(regex))) { // Regex match
                    if(al::iequals(pit.extension().string(), ext)) { // Extension match
                        extFound = ext;
                        goto extFoundLabel;
                    }
                }
            }
        }

extFoundLabel:
        // Output paths
        std::vector<std::string> result;
        for(fs::directory_iterator it(path); it != fs::directory_iterator(); ++it) {
            const fs::path pit = it->path();
            if(boost::regex_search(pit.filename().string(), boost::regex(regex))) // Regex match
                if(al::iequals(pit.extension().string(), extFound)) // Extension match
                    result.push_back(pit.string());
        }

        covis::core::natsort(result);

        return result;
    }
}

namespace covis {
    namespace util {
        void DatasetLoader::parse() {
            /*
             * Sanity checks
             */
            const fs::path proot(_rootPath.empty() ? "." : _rootPath);
            COVIS_ASSERT_MSG(fs::is_directory(proot), "Invalid root path!");
            COVIS_ASSERT_MSG(_objectExtension.size() && _sceneExtension.size(),
                    "You must specify an extension for object and scene files!");

            if(!_poseDir.empty())
                COVIS_ASSERT_MSG(_poseExtension.size(), "No ground truth extension provided!");

            // Get all object/scene/GT dir candidates provided
            std::vector<std::string> objectDirCandidates, sceneDirCandidates, poseDirCandidates;
            boost::split(objectDirCandidates, _objectDir, boost::is_any_of(" \t\n,;:|"), boost::token_compress_on);
            boost::split(sceneDirCandidates, _sceneDir, boost::is_any_of(" \t\n,;:|"), boost::token_compress_on);
            if(!_poseDir.empty())
                boost::split(poseDirCandidates, _poseDir, boost::is_any_of(" \t\n,;:|"), boost::token_compress_on);


            fs::path pobject, pscene, ppose;
            for(auto cand : objectDirCandidates) {
                if(fs::is_directory(proot / fs::path(cand))) {
                    pobject = proot / fs::path(cand);
                    break;
                }
            }

            for(auto cand : sceneDirCandidates) {
                if(fs::is_directory(proot / fs::path(cand))) {
                    pscene = proot / fs::path(cand);
                    break;
                }
            }

            if(!_poseDir.empty()) {
                for(auto cand : poseDirCandidates) {
                    if(fs::is_directory(proot / fs::path(cand))) {
                        ppose = proot / fs::path(cand);
                        break;
                    }
                }
            }

            /*
             * Get model/scene/pose file names
             */
            _objectPaths = findPaths(pobject, _objectExtension, _regexObject);
            COVIS_ASSERT_MSG(_objectPaths.size(), "No object models found!");

            // Object labels
            _objectLabels.clear();
            for(size_t i = 0; i < _objectPaths.size(); ++i)
                _objectLabels.push_back(fs::path(_objectPaths[i]).stem().string());

            _scenePaths = findPaths(pscene, _sceneExtension, _regexScene);
            COVIS_ASSERT_MSG(_scenePaths.size() > 0, "No scene models found!");

            // Scene labels
            _sceneLabels.clear();
            for(size_t i = 0; i < _scenePaths.size(); ++i)
                _sceneLabels.push_back(fs::path(_scenePaths[i]).stem().string());

            if(!ppose.empty()) {
                _posePaths.clear();
                _poseMap.clear();
                // Get seperator candidates
                std::vector<std::string> sepCandidates;
                boost::split(sepCandidates, _poseSeparator, boost::is_any_of("  \t\n,;:|"), boost::token_compress_on);
                // Iterate through all files in pose directory
                for(fs::directory_iterator it(ppose); it != fs::directory_iterator(); ++it) {
                    const fs::path pit = it->path();
                    if(fs::is_regular_file(pit)) {
                        std::string posePath = pit.string();

                        // Loop over objects
                        for(auto obj : _objectLabels) {
                            // If pose file matches object + either seperator or a dot
                            auto range = al::ifind_first(posePath, obj + '.');
                            if(range.empty()) {
                                for(auto sep : sepCandidates) {
                                    auto rangeTmp = al::ifind_first(posePath, obj + sep);
                                    if(!rangeTmp.empty()) {
                                        range = rangeTmp;
                                        break;
                                    }
                                }
                            }

                            if(range.empty())
                                continue;

                            // Remove the object label part of the pose file (but not the sep/dot)
                            posePath.erase(range.begin(), range.end()-1);

                            // Loop over scenes
                            for(auto scn : _sceneLabels) {
                                // If pose file matches scene + either seperator or a dot
                                auto range = al::ifind_first(posePath, scn + '.');
                                if(range.empty()) {
                                    for(auto sep : sepCandidates) {
                                        auto rangeTmp = al::ifind_first(posePath, scn + sep);
                                        if(!rangeTmp.empty()) {
                                            range = rangeTmp;
                                            break;
                                        }
                                    }
                                }

                                if(range.empty())
                                    continue;

                                // Insert into map for easy access in at()
                                auto key = std::make_pair(scn, obj);

                                // - but skip if we encounter a duplicate
                                if(_poseMap.count(key)) {
                                    COVIS_MSG_WARN("Pose for object/scene pair " << obj << "/" << scn <<
                                                   " already loaded in " << _poseMap[key] <<
                                                   " - ignoring pose file " << pit.string() << "!");
                                    continue;
                                }

                                // Now we have a match for both scene and object
                                _posePaths.push_back(pit.string());

                                _poseMap[key] = pit.string();
                            }
                        }
                    }
                }

                core::natsort(_posePaths);

                // Pose labels
                _poseLabels.clear();
                for(size_t i = 0; i < _posePaths.size(); ++i)
                    _poseLabels.push_back(fs::path(_posePaths[i]).stem().string());
            }
        }
        
        bool DatasetLoader::empty(size_t index) const {
            COVIS_ASSERT_MSG(index < size(), "Invalid scene index - must be in [0," << size()-1 << "]!");
            
            // Loop over all objects and check if we have GT pose for one or more
            for(size_t j = 0; j < _objectLabels.size(); ++j)
                if(_poseMap.count(std::make_pair(_sceneLabels[index], _objectLabels[j])))
                    return false;
            
            return true;
        }

        DatasetLoader::SceneEntry DatasetLoader::at(size_t index) const {
            COVIS_ASSERT_MSG(index < size(), "Invalid scene index - must be in [0," << size()-1 << "]!");

            // Prepare result
            SceneEntry result;
            result.index = index;

            // Get the scene model
            result.scene.reset(new ModelT);
            load(_scenePaths[index], *result.scene);

            // Get the scene label
            result.label = _sceneLabels[index];

            // Prepare object mask
            result.objectMask.assign(_objectLabels.size(), false);

            // Loop over all objects and load those that we have a pose for
            for(size_t j = 0; j < _objectLabels.size(); ++j) {
                auto key = std::make_pair(result.label, _objectLabels[j]);
                if(_poseMap.count(key)) {
                    auto it = std::find(_posePaths.begin(), _posePaths.end(), _poseMap.find(key)->second);
                    COVIS_ASSERT(it != _posePaths.end()); // This has to not break
                    const ptrdiff_t idx = std::distance(_posePaths.begin(), it);

                    ModelPtr tmp(new ModelT);
                    load(_objectPaths[j], *tmp);
                    result.objects.push_back(tmp);

                    // Get the object label
                    result.objectLabels.push_back(_objectLabels[j]);

                    // Set object mask
                    result.objectMask[j] = true;

                    // Set pose
                    PoseT m;
                    util::loadEigen(_posePaths[idx], m, true);
                    result.poses.push_back(m);
                    result.poseLabels.push_back(_poseLabels[idx]);
                }
            }

            return result;
        }

        std::vector<pcl::PolygonMesh::Ptr> DatasetLoader::getObjects() const {
            std::vector<pcl::PolygonMesh::Ptr> result(_objectPaths.size());
            for(size_t i = 0; i < _objectPaths.size(); ++i) {
                result[i].reset(new pcl::PolygonMesh);
                load(_objectPaths[i], *result[i]);
            }

            return result;
        }
    }
}

std::ostream& covis::util::operator<<(ostream& os, const DatasetLoader& loader) {
    os << "Loaded dataset:" << endl;
    if(loader.size() == 0) {
        os << "\t(empty)" << endl;
    } else {
        os << "\tObjects: ";
        if(loader.getObjectLabels().size() <= 10) {
            core::print(loader.getObjectLabels(), os);
        } else {
            os << "(" << loader.getObjectLabels().size() << " objects)" << endl;
        }

        os << "\tScenes: ";
        if(loader.getSceneLabels().size() <= 10) {
            core::print(loader.getSceneLabels(), os);
        } else {
            os << "(" << loader.getSceneLabels().size() << " scenes " <<
                 loader.getSceneLabels().front() << "..." << loader.getSceneLabels().back() << ")" << endl;
        }

        os << "\tNumber of poses: " << loader.getPosePaths().size() << endl;
    }

    return os;
}

std::ostream& covis::util::operator<<(ostream& os, const DatasetLoader::SceneEntry& entry) {
    os << "Entry for scene \"" << entry.label << "\" (index " << entry.index << "):" << endl;
    if(entry.objectLabels.empty()) {
        os << "\t(empty)" << endl;
    } else {
        os << "\tObjects: ";
        if(entry.objectLabels.size() <= 10) {
            core::print(entry.objectLabels, os);
        } else {
            os << "(" << entry.objectLabels.size() << " objects)" << endl;
        }

        os << "\tPoses: ";
        if(entry.poseLabels.size() <= 10) {
            core::print(entry.poseLabels, os);
        } else {
            os << "(" << entry.poseLabels.size() << " poses)" << endl;
        }
    }

    return os;
}
