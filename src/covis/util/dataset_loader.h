// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_UTIL_DATASET_LOADER_H
#define COVIS_UTIL_DATASET_LOADER_H

// Boost
#include <boost/algorithm/string.hpp>
namespace al=boost::algorithm;

// STL
#include <iostream>

// PCL
#include <pcl/conversions.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl/point_cloud.h>

// Eigen
#include <eigen3/Eigen/Core>

// Covis
#include "loader_3d.h"
#include "../core/detection.h"
#include "../core/macros.h"

namespace covis {
    namespace util {
        /**
         * @class DatasetLoader
         * @ingroup util
         * @brief Class for loading 3D datasets
         * 
         * Use this class to iterate over a test dataset containing 3D object/scene models and optionally ground truth
         * pose information. Start by setting up all paths and file extensions via the constructor and/or the setters.
         * File extensions must be fully specified, i.e. as <b>.pcd</b> or <b>.ply</b>.
         * 
         * Once paths have been set, use @ref parse() to prepare using the dataset. This function resolves all paths to
         * all models and pose files (if any). You can now load the complete information (objects and poses) for any
         * scene using @ref at(). The information for a single scene is encapsulated in a
         * @ref SceneEntry "DatasetLoader::SceneEntry" containing the current scene label, the object models and their
         * poses.
         * 
         * A few assumptions are made on the dataset for this class to function properly:
         *   - Object/scene models are stored as 3D point clouds or meshes, either in PCD or PLY format.
         *   - Ground truth poses are given as ASCII or binary files containing a single 4-by-4 rigid
         *   transformation matrix.
         *   - Each ground truth pose file corresponds to a single object instance in a scene. The filename of this pose
         *   file is composed by the following syntax:@n
         *     - <b>\<model>\<separator>\<scene>\<extension></b>@n
         *   or:@n
         *     - <b>\<scene>\<separator>\<model>\<extension></b>
         *     
         * For example, the ICVS 2015 3D object recognition dataset (https://gitlab.com/caro-sdu/covis-data) uses the
         * following:
         *   - root path: @b /path/to/covis-data/datasets/icvs_2015
         *   - object directory: @b objects
         *   - scene directory: @b scenes
         *   - pose directory: @b ground_truth
         *   - object/scene model extension: <b>.pcd</b>
         *   - pose file extension: <b>.txt</b>
         *   - pose separator @b -
         *
         * @author Anders Glent Buch
         */
        class DatasetLoader {
            public:
                /// Model type
                typedef pcl::PolygonMesh ModelT;
                
                /// Pointer to model
                typedef ModelT::Ptr ModelPtr;
                
                /// Pose type
                typedef core::Detection::MatrixT PoseT;

                /// Pose vector type
                typedef core::Detection::MatrixVecT PoseVecT;
                
                /// Complete information for a single scene, as retrieved by @ref at() "DatasetLoader::at()"
                struct SceneEntry {
                    size_t index; //!< Index of the scene
                    ModelPtr scene; //!< Model of the scene
                    std::string label; //!< Label of the scene
                    std::vector<bool> objectMask; //!< Mask designating which of all the objects are present in the scene
                    std::vector<ModelPtr> objects; //!< Models of the objects present in the scene
                    std::vector<std::string> objectLabels; //!< Labels of the objects present in the scene
                    PoseVecT poses; //!< Poses of the objects present in the scene
                    std::vector<std::string> poseLabels; //!< Labels of the poses

                    /**
                     * Return true if this scene has no objects with ground true information available
                     * @return empty status
                     */
                    inline bool empty() const {
                        return objects.empty();
                    }
                    
                    /**
                     * Convert all object models to templated point clouds
                     * @return templated object point clouds
                     */
                    template<typename PointT>
                    inline std::vector<typename pcl::PointCloud<PointT>::Ptr> objectsAs() const {
                        std::vector<typename pcl::PointCloud<PointT>::Ptr> result(objects.size());
                        for(size_t i = 0; i < objects.size(); ++i) {
                            result[i].reset(new pcl::PointCloud<PointT>);
                            pcl::fromPCLPointCloud2<PointT>(objects[i]->cloud, *result[i]);
                        }
                        
                        return result;
                    }
                    
                    /**
                     * Convert scene to a templated point cloud
                     * @return templated scene point cloud
                     */
                    template<typename PointT>
                    inline typename pcl::PointCloud<PointT>::Ptr sceneAs() const {
                        COVIS_ASSERT(scene);
                        typename pcl::PointCloud<PointT>::Ptr result(new pcl::PointCloud<PointT>);
                        pcl::fromPCLPointCloud2<PointT>(scene->cloud, *result);
                        
                        return result;
                    }
                };
                
                /**
                 * Constructor: set all paths and extensions
                 * @param rootPath root path
                 * @param objectDir object directory
                 * @param sceneDir scene directory
                 * @param poseDir ground truth pose directory
                 * @param objectExtension extension for object model files
                 * @param sceneExtension extension for scene model files
                 * @param poseExtension extension for ground truth files
                 * @param poseSeparator separator for ground truth files
                 */
                DatasetLoader(const std::string& rootPath = "",
                        const std::string& objectDir = "",
                        const std::string& sceneDir = "",
                        const std::string& poseDir = "",
                        const std::string& objectExtension = "",
                        const std::string& sceneExtension = "",
                        const std::string& poseExtension = "",
                        const std::string& poseSeparator = "") :
                            _rootPath(rootPath.empty() ? "." : rootPath),
                            _objectDir(objectDir.empty() ? "models,objects,meshes,." : objectDir),
                            _sceneDir(sceneDir.empty() ? "scenes,test" : sceneDir),
                            _poseDir(poseDir.empty() ? "gt,ground_truth,GroundTruth_3Dscenes" : poseDir),
                            _objectExtension(objectExtension.empty() ? ".ply,.pcd" : objectExtension),
                            _sceneExtension(sceneExtension.empty() ? ".ply,.pcd" : sceneExtension),
                            _poseExtension(poseExtension.empty() ? ".txt,.xf" : poseExtension),
                            _poseSeparator(poseSeparator.empty() ? "-,_" : poseSeparator) {}
                
                /// Destructor
                virtual ~DatasetLoader() {}

                /**
                 * Set a regular expression to use when loading objects
                 * @param regexObject object file name regular expression
                 */
                inline void setRegexObject(const std::string& regexObject) {
                    _regexObject = regexObject;
                }

                /**
                 * Set a regular expression to use when loading objects
                 * @param regexScene scene file name regular expression
                 */
                inline void setRegexScene(const std::string& regexScene) {
                    _regexScene = regexScene;
                }
                
                /** 
                 * Resolve all paths and prepare to access the dataset
                 * @sa at()
                 */
                void parse();
                
                /**
                 * Check if a scene contains GT pose for zero objects, in which case it may not be required to do @ref at()
                 * @param index scene index
                 * @return true if no GT pose was found
                 */
                bool empty(size_t index) const;
                
                /**
                 * Get the index'th scene data
                 * @param index scene index
                 * @return scene data
                 */
                SceneEntry at(size_t index) const;
                
                /**
                 * Get the scene data for a labeled scene
                 * @param label scene label
                 * @return scene data
                 */
                inline SceneEntry at(const std::string& label) const {
                    for(size_t i = 0; i < _sceneLabels.size(); ++i)
                        if(al::iequals(_sceneLabels[i], label))
                            return at(i);
                    
                    COVIS_THROW("No scene with label \"" << label << "\"!");
                }
                
                
                /**
                 * Get the number of scenes
                 * @return number of scenes
                 */
                inline size_t size() const {
                    return _scenePaths.size();
                }
                
                /**
                 * Get the resolved object paths
                 * @return object paths
                 */
                inline const std::vector<std::string>& getObjectPaths() const {
                    return _objectPaths;
                }
                
                /**
                 * Get the resolved scene paths
                 * @return scene paths
                 */
                inline const std::vector<std::string>& getScenePaths() const {
                    return _scenePaths;
                }
                
                /**
                 * Get the resolved pose paths
                 * @return pose paths
                 */
                inline const std::vector<std::string>& getPosePaths() const {
                    return _posePaths;
                }
                
                /**
                 * Get the object labels - these are automatically resolved as the stem of the object files
                 * @return object labels
                 */
                inline const std::vector<std::string>& getObjectLabels() const {
                    return _objectLabels;
                }
                
                /**
                 * Get the scene labels - these are automatically resolved as the stem of the scene files
                 * @return scene labels
                 */
                inline const std::vector<std::string>& getSceneLabels() const {
                    return _sceneLabels;
                }

                /**
                 * Get the pose labels - these are automatically resolved as the stem of the pose files
                 * @return pose labels
                 */
                inline const std::vector<std::string>& getPoseLabels() const {
                    return _poseLabels;
                }

                /**
                 * Get all object models of this dataset
                 * @return all object models
                 */
                std::vector<pcl::PolygonMesh::Ptr> getObjects() const;
                
            private:
                /// Full path to the root directory of the dataset
                std::string _rootPath;
                
                /// Name of the subdirectory of the dataset containing object models
                std::string _objectDir;
                
                /// Name of the subdirectory of the dataset containing scene models
                std::string _sceneDir;
                
                /// Name of the subdirectory of the dataset containing ground truth poses, if any
                std::string _poseDir;
                
                /// Extension for object model files (.pcd or .ply) - if empty we use @ref _sceneExtension
                std::string _objectExtension;
                
                /// Extension for scene model files (.pcd or .ply) - if empty we use @ref _objectExtension
                std::string _sceneExtension;
                
                /// Extension for ground truth pose files - this is '.txt' in the case of e.g. <object>-<scene>.txt
                std::string _poseExtension;
                
                /// Separator for ground truth pose files - this is '-' in the case of e.g. <object>-<scene>.txt
                std::string _poseSeparator;

                /// Regular expression to use when loading objects
                std::string _regexObject;

                /// Regular expression to use when loading scenes
                std::string _regexScene;
                
                /// Full path to all objects
                std::vector<std::string> _objectPaths;

                /// Full path to all scenes
                std::vector<std::string> _scenePaths;

                /// Full path to all scenes
                std::vector<std::string> _posePaths;

                /// Labels for the objects
                std::vector<std::string> _objectLabels;

                /// Labels for the scenes
                std::vector<std::string> _sceneLabels;

                /// Labels for the poses
                std::vector<std::string> _poseLabels;

                /// Map between <scene,object> and pose file
                std::map<std::pair<std::string,std::string>, std::string> _poseMap;
        };

        /**
         * @ingroup util
         * @brief Print a @ref DatasetLoader instance to a stream
         * @param os stream to print to
         * @param loader dataset loader instance
         * @return modified stream
         */
        std::ostream& operator<<(std::ostream& os, const DatasetLoader& loader);
        
        /**
         * @ingroup util
         * @brief Print a @ref DatasetLoader::SceneEntry to a stream
         * @param os stream to print to
         * @param entry scene data
         * @return modified stream
         */
        std::ostream& operator<<(std::ostream& os, const DatasetLoader::SceneEntry& entry);
    }
}

#endif
