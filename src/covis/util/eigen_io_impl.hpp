// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_UTIL_EIGEN_IO_IMPL_HPP
#define COVIS_UTIL_EIGEN_IO_IMPL_HPP

// Own
#include "../core/io.h"

// STL
#include <fstream>

namespace covis {
    namespace util {
        template<typename Derived>
         void EigenIO<Derived>::load(const std::string& filepath, Eigen::MatrixBase<Derived>& m) const {
            // Sanity checks
            if(m.rows() == 0 || m.cols() == 0) {
                if(_binary) {
                    COVIS_MSG_WARN("Output matrix dimensions not initialized for binary file - returning zero matrix!");
                    return;
                }

                // Auto
                const std::pair<size_t,size_t> dims = core::rowscols(filepath);
                m.derived().resize(dims.first, dims.second);
            }

            // Open file
            std::ifstream ifs(filepath.c_str(), (_binary ? std::ios::binary : std::ios::in));
            if(!ifs)
                COVIS_THROW("Cannot open file \"" << filepath << "\" for reading!");

            // Read
            typedef typename Eigen::MatrixBase<Derived>::Scalar Scalar;
            for(typename Eigen::MatrixBase<Derived>::Index r = 0; r < m.rows(); ++r) {
                for(typename Eigen::MatrixBase<Derived>::Index c = 0; c < m.cols(); ++c) {
                    if(_rowMajor) {
                        if(_binary)
                            ifs.read(reinterpret_cast<char*>(&m.derived()(r, c)), sizeof(Scalar));
                        else
                            ifs >> m.derived()(r,c);
                    } else {
                        if(_binary)
                            ifs.read(reinterpret_cast<char*>(&m.derived()(c, r)), sizeof(Scalar));
                        else
                            ifs >> m.derived()(c,r);
                    }

                    if(!ifs)
                        COVIS_THROW("Failed to read matrix from file \"" << filepath << "\"! " <<
                                                                         (r + 1) * (c + 1) - 1 << " element(s) have been found, but matrix size is " <<
                                                                         m.rows() * m.cols() << "!");
                }
            }
        }

        template<typename Derived>
        void EigenIO<Derived>::save(const std::string& filepath, const Eigen::MatrixBase<Derived>& m) const {
            // Sanity checks
            if(m.rows() == 0 || m.cols() == 0) {
                COVIS_MSG_WARN("Input matrix dimensions not initialized!");
                return;
            }

            // Open file
            std::ofstream ofs;
            ofs.open(filepath.c_str(),
                     (_binary ? std::ios::binary : std::ios::out) | (_append ? std::ios::app : std::ios::out));
            if(!ofs)
                COVIS_THROW("Cannot open file \"" << filepath << "\" for writing!");

            // Write
            typedef typename Eigen::MatrixBase<Derived>::Scalar Scalar;
            for(typename Eigen::MatrixBase<Derived>::Index r = 0; r < m.rows(); ++r) {
                for(typename Eigen::MatrixBase<Derived>::Index c = 0; c < m.cols(); ++c) {
                    if(_rowMajor) {
                        if(_binary)
                            ofs.write(reinterpret_cast<const char*>(&m.derived()(r,c)), sizeof(Scalar));
                        else
                            ofs << m.derived()(r,c) << " ";
                    } else {
                        if(_binary)
                            ofs.write(reinterpret_cast<const char*>(&m.derived()(c, r)), sizeof(Scalar));
                        else
                            ofs << m.derived()(c,r) << " ";
                    }

                    if(!ofs)
                        COVIS_THROW("Failed to write matrix to file \"" << filepath << "\"!");
                }
                if(!_binary)
                    ofs << std::endl;
            }
        }
    }
}

#endif