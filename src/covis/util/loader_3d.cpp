// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.


// Own
#include "loader_3d.h"

bool covis::util::load(const std::string &filename, pcl::PolygonMesh& mesh) {
    const std::string ext = covis::core::extension(filename);
    if(ext == "pcd")
        return (pcl::io::loadPCDFile(filename, mesh.cloud) == 0); // Returns 0 on success
    else if(ext == "ply")
        return loadPLYFile(filename, mesh);
    else if(ext == "obj")
        return (pcl::io::loadPolygonFileOBJ(filename, mesh) > 0); // Returns number of points loaded
    else if(ext == "stl")
        return (pcl::io::loadPolygonFileSTL(filename, mesh) > 0); // Returns number of points loaded
    else if(ext == "vtk")
        return (pcl::io::loadPolygonFileVTK(filename, mesh) > 0); // Returns number of points loaded
    else if(ext == "xyz")
        return load(filename, mesh.cloud);
    else
        COVIS_THROW("Invalid file extension for file: " << filename << "!");
}

bool covis::util::load(const std::string& filename, pcl::PCLPointCloud2& cloud) {
    const std::string ext = core::extension(filename);
    if(ext == "pcd")
        return (pcl::io::loadPCDFile(filename, cloud) == 0); // Returns 0 on success
    else if (ext == "ply")
        return loadPLYFile(filename, cloud);
    else if(ext == "obj") {
        pcl::PolygonMesh mesh;
        if(pcl::io::loadPolygonFileOBJ(filename, mesh) == 0) // Returns number of points loaded
            return false;
        cloud = mesh.cloud;
        return true;
    } else if(ext == "stl") {
        pcl::PolygonMesh mesh;
        if(pcl::io::loadPolygonFileSTL(filename, mesh) == 0) // Returns number of points loaded
            return false;
        cloud = mesh.cloud;
        return true;
    } else if(ext == "vtk") {
        pcl::PolygonMesh mesh;
        if(pcl::io::loadPolygonFileVTK(filename, mesh) == 0) // Returns number of points loaded
            return false;
        cloud = mesh.cloud;
        return true;
    }  else if(ext == "xyz") {
        cv::Mat_<cv::Vec3f> xyz = cv::imread(filename, cv::IMREAD_UNCHANGED);
        if(xyz.empty() || xyz.channels() != 3)
            return false;
        pcl::PointCloud<pcl::PointXYZ> pc(xyz.cols, xyz.rows);
        for(int r = 0; r < xyz.rows; ++r) {
            for(int c = 0; c < xyz.cols; ++c) {
                pcl::PointXYZ& p = pc(c,r);
                p.x = xyz(r,c)[0];
                p.y = xyz(r,c)[1];
                p.z = xyz(r,c)[2];
            }
        }
        pcl::toPCLPointCloud2(pc, cloud);
        return true;
    } else
        COVIS_THROW("Invalid file extension for file: " << filename << "!");
}
