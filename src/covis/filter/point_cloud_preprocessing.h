// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FILTER_POINT_CLOUD_PREPROCESSING_H
#define COVIS_FILTER_POINT_CLOUD_PREPROCESSING_H

// Own
#include "point_cloud_filter_base.h"
#include "../core/io.h"
#include "../core/macros.h"
#include "../core/range.h"
#include "../core/traits.h"
#include "../detect/point_search.h"
#include "../util/ply_loader.h"

// STL
#include <cmath>

// Boost
#include <boost/shared_ptr.hpp>

// PCL
#include <pcl/point_cloud.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/common/centroid.h>
#include <pcl/features/feature.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/filter.h>
#include <pcl/filters/passthrough.h>
#include <pcl/io/pcd_io.h>
#include <pcl/segmentation/sac_segmentation.h>

namespace covis {
    namespace filter {
        /**
         * @class PointCloudPreprocessing
         * @ingroup filter
         * @brief Preprocess a point cloud model for later use e.g. during recognition
         *
         * You enable a functionality by setting a non-zero parameter for it. For example, setting a non-zero normal
         * estimation radius (@ref setNormalRadius()) enables normal estimation. Turn everything off again using
         * @ref reset().
         *
         * @todo For now this class downsamples before performing normal estimation for efficiency. However, this can
         * lead to inaccurate normals. Introduce a flag to switch between the two modes.
         *
         * @tparam PointT input point type, must contain XYZ+normal data
         * @author Anders Glent Buch
         */
        template<typename PointT>
        class PointCloudPreprocessing : public PointCloudFilterBase<PointT> {
            public:
                /// Pointer type
                typedef typename boost::shared_ptr<PointCloudPreprocessing<PointT> > Ptr;
                
                /**
                 * Default constructor
                 */
                PointCloudPreprocessing() {
                    reset();
                }
                
                /// Destructor
                virtual ~PointCloudPreprocessing() {}
                
                /// @copydoc PointCloudFilterBase::filter()
                typename pcl::PointCloud<PointT>::Ptr filter(typename pcl::PointCloud<PointT>::ConstPtr cloud);

                /**
                 * Apply the filter to an input mesh, return a point cloud
                 * @param mesh input mesh
                 * @return filtered point cloud
                 */
                typename pcl::PointCloud<PointT>::Ptr filter(pcl::PolygonMesh::ConstPtr mesh);

                /**
                 * Set scaling on the coordinates
                 * @param scale XYZ scaling
                 */
                inline void setScale(float scale) {
                    _scale = scale;
                }

                /**
                 * Set whether to remove NaN points/normals
                 * @param removeNan NaN removal flag
                 */
                inline void setRemoveNan(bool removeNan) {
                    _removeNan = removeNan;
                }

                /**
                 * Set far limit (ignored if <= 0)
                 * @param far far limit
                 */
                inline void setFar(float far) {
                    _far = far;
                }

                /**
                 * Set dominant plane removal flag
                 * @param removeTable dominant plane removal flag
                 */
                inline void setRemoveTable(bool removeTable) {
                    _removeTable = removeTable;
                }

                /**
                 * Set the table removal threshold, use 0 for an automatic value
                 * @param tableRemovalThreshold table removal threshold
                 */
                inline void setTableRemovalThreshold(float tableRemovalThreshold) {
                    _tableRemovalThreshold = tableRemovalThreshold;
                }

                /**
                 * Set downsampling resolution (ignored if <= 0)
                 * In case of a point cloud model, this parameter is interpreted as a metric value giving the spacing
                 * between points. In case of a mesh model, this corresponds to the decimation level, a number in ]0,1],
                 * where 1 means no downsampling.
                 * @param resolution downsampling resolution
                 */
                inline void setResolution(float resolution) {
                    _resolution = resolution;
                }

                /**
                 * Set normal estimation radius (ignored if <= 0)
                 * @param normalRadius normal estimation radius
                 */
                inline void setNormalRadius(float normalRadius) {
                    _normalRadius = normalRadius;
                }

                /**
                 * Set consistent normal orientation flag
                 * @param orientNormals consistent normal orientation flag
                 */
                inline void setOrientNormals(bool orientNormals) {
                    _orientNormals = orientNormals;
                }

                /**
                 * Set to true to assume non-convex models during normal correction
                 * @param concave concavity flag
                 */
                inline void setConcave(bool concave) {
                    _concave = concave;
                }

                /**
                 * Turn off all functionality except verbosity
                 */
                inline void reset() {
                    _scale = 1;
                    _removeNan = false;
                    _far = 0;
                    _removeTable = false;
                    _tableRemovalThreshold = -1;
                    _resolution = 0;
                    _normalRadius = 0;
                    _orientNormals = false;
                    _concave = false;
                    _verbose = false;
                }

                /**
                 * Set verbosity
                 * @param verbose verbosity
                 */
                inline void setVerbose(bool verbose) {
                    _verbose = verbose;
                }
                
            private:
                /// Scaling on the XYZ coordinates - this is the very first operation performed, if enabled
                float _scale;

                /// NaN removal flag
                bool _removeNan;

                /// Remove all points with a z-coordinate larger than this value
                float _far;

                /// Remove dominant plane
                bool _removeTable;

                /// Dominant plane removal threshold
                float _tableRemovalThreshold;

                /// Normal estimation radius
                float _normalRadius;

                /// Output point cloud resolution
                float _resolution;

                /// Consistent normal orientation flag
                float _orientNormals;

                /// Set to true to assume non-convex models during normal correction
                bool _concave;

                /// Verbosity
                bool _verbose;

                /**
                 * Compute normals
                 * @param result input/output point cloud
                 */
                inline void computeNormals(typename pcl::PointCloud<PointT>::Ptr result, bool weight = true) {
                    if(_verbose)
                        COVIS_MSG_INFO("Performing normal estimation with a radius of " << _normalRadius << "...");

                    if(weight) {
                        detect::PointSearch<PointT> search(result, false);
                        pcl::PointCloud<pcl::Normal> n(result->width, result->height);
                        const double radsq = _normalRadius * _normalRadius;
#ifdef COVIS_USE_OPENMP
#pragma omp parallel for
#endif
                        for(size_t i = 0; i < result->size(); ++i) {
                            const core::Correspondence crad = search.radius(result->points[i], _normalRadius);
                            if(crad.match.size() < 3) {
                                n[i].getNormalVector3fMap().setConstant(std::numeric_limits<float>::quiet_NaN());
                                n[i].curvature = std::numeric_limits<float>::quiet_NaN ();
                                continue;
                            }

                            int cnt = 0;
                            Eigen::Vector3d c = Eigen::Vector3d::Zero();
                            for(size_t j = 0; j < crad.size(); ++j) {
                                const PointT& p = result->points[crad.match[j]];
                                if(!pcl::isFinite(p))
                                    continue;
                                c += p.getVector3fMap().template cast<double>();
                                ++cnt;
                            }

                            if(cnt == 0) {
                                n[i].getNormalVector3fMap().setConstant(std::numeric_limits<float>::quiet_NaN());
                                n[i].curvature = std::numeric_limits<float>::quiet_NaN ();
                                continue;
                            }

                            c /= float(cnt);

                            Eigen::Matrix3d cov = Eigen::Matrix3d::Zero();
                            for(size_t j = 0; j < crad.size(); ++j) {
                                const PointT& p = result->points[crad.match[j]];
                                if(!pcl::isFinite(p))
                                    continue;
                                const Eigen::Vector3d v = p.getVector3fMap().template cast<double>() - c;
                                cov += v * v.transpose() * std::exp(-0.5 * double(crad.distance[j]) / radsq);
                            }

                            pcl::solvePlaneParameters(cov.cast<float>(), n[i].normal_x, n[i].normal_y, n[i].normal_z, n[i].curvature);

                            pcl::flipNormalTowardsViewpoint<PointT>(result->points[i],
                                                                    result->sensor_origin_.coeff(0),
                                                                    result->sensor_origin_.coeff(1),
                                                                    result->sensor_origin_.coeff(2),
                                                                    n[i].normal_x,
                                                                    n[i].normal_y,
                                                                    n[i].normal_z);
                        }

                        pcl::concatenateFields<PointT,pcl::Normal,PointT>(*result, n, *result);
                    } else { // No weighting
                        pcl::NormalEstimationOMP<PointT, pcl::Normal> ne;
                        ne.setRadiusSearch(_normalRadius);

                        pcl::PointCloud<pcl::Normal> n;
                        ne.setInputCloud(result);
                        ne.compute(n);

                        pcl::concatenateFields<PointT,pcl::Normal,PointT>(*result, n, *result);
                    }
                }

                /**
                 * @struct Downsampler
                 * @brief Point cloud downsampler
                 * @tparam PointT2 point type, suffix 2 used not to shadow the class template
                 * @tparam HasNormal true if point type has normal/curvature data
                 */
                template<typename PointT2, bool HasNormal = core::HasNormal<PointT2>::value>
                struct Downsampler {
                    /**
                     * Downsample a point cloud, include normal/curvature information if available
                     * @param cloud point cloud
                     * @param resolution output resolution
                     * @return downsampled point cloud
                     */
                    static typename pcl::PointCloud<PointT2>::Ptr run(typename pcl::PointCloud<PointT2>::ConstPtr cloud,
                                                                     float resolution);
                };
        };

        /**
         * @ingroup filter
         * Scale the XYZ coordinates of a point cloud
         * @param cloud input point cloud
         * @param scaling output point cloud scale
         * @return scaled point cloud
         */
        template<typename PointT>
        inline typename pcl::PointCloud<PointT>::Ptr scale(typename pcl::PointCloud<PointT>::ConstPtr cloud,
                                                           float scaling) {
            typename pcl::PointCloud<PointT>::Ptr result(new pcl::PointCloud<PointT>(*cloud));
            for(size_t i = 0; i < result->size(); ++i) {
                result->points[i].x *= scaling;
                result->points[i].y *= scaling;
                result->points[i].z *= scaling;
            }

            return result;
        }

        /**
         * @ingroup filter
         * Scale the vertex coordinates of a mesh
         * @param mesh input mesh
         * @param scaling output mesh scale
         * @return scaled mesh
         */
        pcl::PolygonMesh::Ptr scale(pcl::PolygonMesh::ConstPtr mesh, float scaling);

        /**
         * @ingroup filter
         * Remove background points from a point cloud
         * @param cloud input point cloud
         * @param threshold far threshold
         * @return trimmed point cloud
         */
        template<typename PointT>
        inline typename pcl::PointCloud<PointT>::Ptr removeFarPoints(typename pcl::PointCloud<PointT>::ConstPtr cloud,
                                                                     float threshold) {
            typename pcl::PointCloud<PointT>::Ptr result(new pcl::PointCloud<PointT>);

            pcl::PassThrough<PointT> pt;
            pt.setInputCloud(cloud);
            pt.setFilterFieldName("z");
            pt.setFilterLimits(-threshold, threshold);
            pt.filter(*result);

            return result;
        }

        /**
         * @ingroup filter
         * Remove dominant (table) plane from a point cloud
         * @param cloud input point cloud
         * @param threshold Euclidean inlier threshold for determining whether a point is in the dominant plane -
         * set to zero to use 10 x the resolution of the input point cloud
         * @param iterations set the number of RANSAC iterations (0 for automatic)
         * @param invert if set to true, invert the logic of this function and only return the table points
         * @return trimmed point cloud
         */
        template<typename PointT>
        inline typename pcl::PointCloud<PointT>::Ptr removeDominantPlane(typename pcl::PointCloud<PointT>::ConstPtr cloud,
                                                                         float threshold = 0,
                                                                         size_t iterations = 0,
                                                                         bool invert = false) {
            float thres;
            if(threshold <= 0)
                thres = 10 * detect::computeResolution<PointT>(cloud);
            else
                thres = threshold;

            // Create the segmentation object
            pcl::SACSegmentation<PointT> seg;
            seg.setMaxIterations(iterations > 0 ? iterations : 100);
            seg.setOptimizeCoefficients(true);
            seg.setModelType(pcl::SACMODEL_PLANE);
            seg.setMethodType(pcl::SAC_RANSAC);
            seg.setDistanceThreshold(thres);

            seg.setInputCloud(cloud);

            pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
            pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
            seg.segment(*inliers, *coefficients);

            typename pcl::PointCloud<PointT>::Ptr result(new pcl::PointCloud<PointT>);
            if(inliers->indices.empty()) { // No plane found
                *result = *cloud;
            } else {
                pcl::ExtractIndices<PointT> ei;
                ei.setInputCloud(cloud);
                ei.setIndices(inliers);
                ei.setNegative(!invert);
                ei.filter(*result);
            }

            return result;
        }

        /**
         * Remove planar structures from a point cloud, measured by a total curvature estimate
         * @param cloud input/output point cloud
         * @param radius search radius
         * @param thres lower curvature threshold for keeping points - typical values lies between 0.01 and 0.025
         */
        template<typename PointT>
        inline void removePlanarStructures(typename pcl::PointCloud<PointT>::Ptr cloud,
                                           float radius, float thres = 0.01) {
            pcl::NormalEstimationOMP<PointT,pcl::Normal> ne;
            ne.setInputCloud(cloud);
            ne.setRadiusSearch(radius);

            pcl::PointCloud<pcl::Normal> normals;
            ne.compute(normals);

            std::vector<bool> mask(normals.size());
            for(size_t i = 0; i < normals.size(); ++i)
                mask[i] = (normals[i].curvature >= thres);

            *cloud = core::mask(*cloud, mask);
        }

        /**
         * @ingroup filter
         * Downsample a point cloud
         * @param cloud input point cloud
         * @param resolution output point cloud resolution
         * @return downsampled point cloud
         */
        template<typename PointT>
        inline typename pcl::PointCloud<PointT>::Ptr downsample(typename pcl::PointCloud<PointT>::ConstPtr cloud,
                                                                float resolution) {
            PointCloudPreprocessing<PointT> pcp;
            pcp.setResolution(resolution);

            return pcp.filter(cloud);
        }

         /**
          * @ingroup filter
          * Preprocess a point cloud
          * @param cloud input point cloud
          * @param scale scaling of coordinates
          * @param removeNan remove NaNs
          * @param far set to a non-zero value to remove points with a high z-coordinate
          * @param resolution output resolution
          * @param normalRadius normal estimation radius
          * @param orientNormals ensure consistent normal orientation
          * @param concave assume a non-convex model during consistent normal orientation
          * @param verbose verbose
          * @return preprocessed point cloud
          */
        template<typename PointT>
        inline typename pcl::PointCloud<PointT>::Ptr preprocess(
                typename pcl::PointCloud<PointT>::ConstPtr cloud,
                float scale,
                bool removeNan,
                float far,
                float resolution,
                float normalRadius,
                bool orientNormals,
                bool concave,
                bool verbose = false) {
            filter::PointCloudPreprocessing<PointT> pcp;
            pcp.setScale(scale);
            pcp.setRemoveNan(removeNan);
            pcp.setFar(far);
            pcp.setResolution(resolution);
            pcp.setNormalRadius(normalRadius);
            pcp.setOrientNormals(orientNormals);
            pcp.setConcave(concave);
            pcp.setVerbose(verbose);

            return pcp.filter(cloud);
        }

        /**
         * @ingroup filter
         * Preprocess a mesh and output a point cloud
         * @param mesh input mesh
         * @param scale scaling of coordinates
         * @param removeNan remove NaNs
         * @param far set to a non-zero value to remove points with a high z-coordinate
         * @param resolution output resolution
         * @param normalRadius normal estimation radius
         * @param orientNormals ensure consistent normal orientation (only used if input mesh has no faces)
         * @param concave assume a non-convex model during consistent normal orientation
         * (only used if input mesh has no faces)
         * @param verbose verbose
         * @return preprocessed point cloud
         */
        template<typename PointT>
        inline typename pcl::PointCloud<PointT>::Ptr preprocess(
                pcl::PolygonMesh::ConstPtr mesh,
                float scale,
                bool removeNan,
                float far,
                float resolution,
                float normalRadius,
                bool orientNormals,
                bool concave,
                bool verbose = false) {
            filter::PointCloudPreprocessing<PointT> pcp;
            pcp.setScale(scale);
            pcp.setRemoveNan(removeNan);
            pcp.setFar(far);
            pcp.setResolution(resolution);
            pcp.setNormalRadius(normalRadius);
            pcp.setOrientNormals(orientNormals);
            pcp.setConcave(concave);
            pcp.setVerbose(verbose);

            return pcp.filter(mesh);
        }
    }
}

#ifndef COVIS_PRECOMPILE
#include "point_cloud_preprocessing_impl.hpp"
#endif

#endif
