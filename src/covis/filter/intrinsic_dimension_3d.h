// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FILTER_INTRINSIC_DIMENSION_3D_H
#define COVIS_FILTER_INTRINSIC_DIMENSION_3D_H

// Own
#include "point_cloud_filter_base.h"
#include "../core/ids.h"
#include "../core/macros.h"

// PCL
#include <pcl/point_types.h>

namespace covis {
    namespace filter {
        /**
         * @class IntrinsicDimension3D
         * @ingroup filter
         * @brief Intrinsic dimension for point clouds
         *
         * This class implements the method of @cite demantke2011dimensionality.
         * Each point is characterized based on the point spread in its neighborhood. The point spread
         * along each axis is computed by the local covariance matrix of the three coordinates,
         * and the point is classified as being intrinsically 1-, 2- or 3-dimensional based on the eigenvalues.
         * 
         * As in the original work, this class also computes the optimal scale. To disable this behavior, which requires
         * additional computational resources, set the number of octaves to 0.
         * 
         * @note In this implementation, we have modified the interpretation of the resulting ID values to accomodate
         * the semantics of the 2D version of ID (see e.g. @ref covis::filter::IntrinsicDimension "IntrinsicDimension").
         * This means the ID 0 is assigned the value of @f$a_{2D}@f$ in the original work. ID 1 and ID 2 are assigned
         * the values of @f$a_{1D}@f$ and @f$a_{3D}@f$, respectively.
         *
         * @tparam PointInT input point type, must contain XYZ data
         * @tparam PointOutT output point type, must contain ID fields and scale, e.g. @ref covis::core::Ids
         * @sa @ref covis::core::Ids
         * @author Anders Glent Buch
         * @example example/intrinsic_dimension_3d/intrinsic_dimension_3d.cpp
         */
        template<typename PointInT, typename PointOutT = core::Ids>
        class IntrinsicDimension3D : public PointCloudFilterBase<PointInT, PointOutT> {
            public:
                /**
                 * Default constructor: initialize minimum scale to 0.002 and number of octaves to 8, giving a final
                 * scale of 0.032
                 */ 
                IntrinsicDimension3D() : _minScale(0.005), _octaves(8) {}

                /// Destructor
                virtual ~IntrinsicDimension3D() {}

                /// @copydoc PointCloudFilterBase::filter()
                virtual typename pcl::PointCloud<PointOutT>::Ptr filter(
                        typename pcl::PointCloud<PointInT>::ConstPtr cloud);
                
                /**
                 * Set minimum scale
                 * @param minScale scale
                 */
                inline void setMinScale(float minScale) {
                    _minScale = minScale;
                }
                
                /**
                 * Get minimum scale
                 * @return minimum scale
                 */
                inline float getMinScale() const {
                    return _minScale;
                }
                
                /**
                 * Set number of octaves
                 * @param octaves number of octaves
                 */
                inline void setOctaves(size_t octaves) {
                    _octaves = octaves;
                }
                
                /**
                 * Get number of octaves
                 * @return number of octaves
                 */
                inline size_t getOctaves() const {
                    return _octaves;
                }
                
            protected:
                /// Minimum scale, or Euclidean radius
                float _minScale;
                
                /**
                 * Number of octaves
                 * The scale-space is sampled at this number of scales, starting from the minimum scale
                 * The factor between each scale is set to @f$\sqrt 2@f$, so for e.g. 8 octaves, the final scale is
                 * 16 times the minimum scale.
                 */
                size_t _octaves;
        };
    }
}

#include "intrinsic_dimension_3d_impl.hpp"

#endif