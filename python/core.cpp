#include "covis_python_common.h"

#include <covis/core/correspondence.h>
#include <covis/core/detection.h>
#include <covis/core/transform.h>

#include <pcl/console/print.h>

using namespace boost::python;

PointCloud pointCloud(size_t size) {
    PointCloudT cloud(size, 1);
    PointCloud result;
    pcl::toPCLPointCloud2(cloud, result);
    return result;
}

PointCloud pointCloud() {
    return pointCloud(0);
}

PointCloud pointCloud(const object& array) {
    const Matrix m = fromNumpyArray(array);
    PointCloudT c(m.cols(), 1);
    for(size_t i = 0; i < c.size(); ++i) {
        PointT& pi = c[i];
        if(m.cols() >= 3) {
            pi.x = m(0,i);
            pi.y = m(1,i);
            pi.z = m(2,i);
            if(m.cols() >= 6) {
                pi.normal_x = m(3,i);
                pi.normal_y = m(4,i);
                pi.normal_z = m(5,i);
                if(m.cols() >= 7) {
                    pi.curvature = m(6,i);
                    if(m.cols() >= 10) {
                        pi.r = uint8_t(255.0f * m(7,i));
                        pi.g = uint8_t(255.0f * m(8,i));
                        pi.b = uint8_t(255.0f * m(9,i));
                        if(m.cols() >= 11) {
                            pi.a = uint8_t(255.0f * m(10,i));
                        }
                    }
                }
            }
        }
    }

    PointCloud result;
    pcl::toPCLPointCloud2(c, result);
    return result;
}

size_t pointCloudSize(PointCloud& cloud) {
    return cloud.width * cloud.height;
}

object pointCloudArray(const PointCloud& cloud) {
    PointCloudT c;
    pcl::fromPCLPointCloud2(cloud, c);
    Matrix m(PointTDim, c.size());
    for(size_t i = 0; i < c.size(); ++i) {
        const PointT& pi = c[i];
        m.col(i) << pi.x, pi.y, pi.z,
                pi.normal_x, pi.normal_y, pi.normal_z, pi.curvature,
                float(pi.r) / 255.0f, float(pi.g) / 255.0f, float(pi.b) / 255.0f, float(pi.a) / 255.0f;
    }

    return toNumpyArray(m);
}

PointCloud pointCloudAdd(const PointCloud& cloud1, const PointCloud& cloud2) {
    PointCloudT c1, c2;
    pcl::fromPCLPointCloud2(cloud1, c1);
    pcl::fromPCLPointCloud2(cloud2, c2);

    PointCloud result;
    pcl::toPCLPointCloud2(c1 + c2, result);

    return result;
}

PointCloud pointCloudIadd(PointCloud& cloud1, const PointCloud& cloud2) {
    cloud1 = pointCloudAdd(cloud1, cloud2);
    return cloud1;
}

list correspondenceMatch(covis::core::Correspondence& c) {
    return toList(c.match);
}

list correspondenceDistance(covis::core::Correspondence& c) {
    return toList(c.distance);
}

list sort(const list& correspondences) {
    covis::core::Correspondence::Vec corr = fromList<covis::core::Correspondence::Vec>(correspondences);
    covis::core::sort(corr);
    return toList(corr);
}

list flatten(const list& correspondences) {
    covis::core::Correspondence::Vec corr = fromList<covis::core::Correspondence::Vec>(correspondences);
    return toList(*covis::core::flatten(corr));
}

list invert(const list& correspondences) {
    covis::core::Correspondence::Vec corr = fromList<covis::core::Correspondence::Vec>(correspondences);
    return toList(*covis::core::invert(corr));
}

list unique(const list& correspondences, size_t k) {
    covis::core::Correspondence::Vec corr = fromList<covis::core::Correspondence::Vec>(correspondences);
    return toList(*covis::core::unique(corr, k));
}

list computeAllCorrespondences(size_t numQuery, size_t numTarget, float distance) {
    return toList(*covis::core::computeAllCorrespondences(numQuery, numTarget, distance));
}

/*
 * detection.h
 */
covis::core::Detection::MatrixT poseIdentity(const covis::core::Detection::MatrixT&) {
    return covis::core::Detection::MatrixT::Identity();
}

covis::core::Detection::MatrixT Identity() {
    return covis::core::Detection::MatrixT::Identity();
}

object poseArray(const covis::core::Detection::MatrixT& pose) {
    return toNumpyArray(pose);
}

float detectionKde(const covis::core::Detection& d) {
    return d.params.at("kde");
}


/*
 * transform.h
 */
PointCloud transform(const PointCloud& cloud, const covis::core::Detection::MatrixT& pose) {
    PointCloud result;
    covis::core::transform(cloud, result, pose);
    return result;
}

PointCloud transform(const PointCloud& cloud, const object& pose) {
    const Matrix m = fromNumpyArray(pose);
    PointCloud result;
    covis::core::transform(cloud, result, m);
    return result;
}

Mesh transform(const Mesh& mesh, const object& pose) {
    Mesh result = mesh;
    result.cloud = transform(result.cloud, pose);
    return result;
}

/*
 * Module
 */
BOOST_PYTHON_MODULE(core) {
    // Turn off excessive PCL messages
    pcl::console::setVerbosityLevel(pcl::console::L_ERROR);
    docstring_options local_docstring_options(true, true, false);

    // Suppress numpy warning about unused function
    _import_array();

    /*
     * PCL data types
     */

    class_<PointCloud>("PointCloud", no_init)
            .def_readwrite("width", &PointCloud::width)
            .def_readwrite("height", &PointCloud::height)
            .add_property("size", pointCloudSize)
            .def("array", pointCloudArray)
            .def("__add__", pointCloudAdd)
            .def("__iadd__", pointCloudIadd)
            ;
    def("PointCloud", (PointCloud(*)(size_t))pointCloud, arg("size"));
    def("PointCloud", (PointCloud(*)(void))pointCloud);
    def("PointCloud", (PointCloud(*)(const object&))pointCloud, arg("array"));

    class_<Mesh>("Mesh")
            .def_readwrite("cloud", &Mesh::cloud)
            .def_readwrite("polygons", &Mesh::polygons)
            ;

    class_<std::vector<pcl::Vertices>>("VerticesVector")
            .def_readonly("size", &std::vector<pcl::Vertices>::size)
            ;

    /*
     * correspondence.h
     */
    class_<covis::core::Correspondence>("Correspondence", init<size_t,size_t,float>())
            .def_readwrite("query", &covis::core::Correspondence::query)
            .add_property("match", correspondenceMatch)
            .add_property("distance", correspondenceDistance)
            ;
    def("sort", sort, arg("correspondences"));
    def("unique", unique, (arg("correspondences"), arg("k")=1));
    def("flatten", flatten, arg("correspondences"));
    def("invert", invert, arg("correspondences"));
    def("computeAllCorrespondences", computeAllCorrespondences,
        (arg("numQuery"), arg("numTarget"), arg("distance")=-1));

    /*
     * detection.h
     */
    class_<covis::core::Detection>("Detection")
            .def(self_ns::str(self))
            .def_readwrite("pose", &covis::core::Detection::pose)
            .def_readwrite("rmse", &covis::core::Detection::rmse)
            .def_readwrite("penalty", &covis::core::Detection::penalty)
            .def_readwrite("inlierfrac", &covis::core::Detection::inlierfrac)
            .def_readwrite("outlierfrac", &covis::core::Detection::outlierfrac)
            .def_readwrite("idx", &covis::core::Detection::idx)
            .def_readwrite("label", &covis::core::Detection::label)
            .add_property("kde", detectionKde)
            ;

    class_<covis::core::Detection::MatrixT>("Pose")
            .def_readonly("rows", &covis::core::Detection::MatrixT::rows)
            .def_readonly("cols", &covis::core::Detection::MatrixT::cols)
            .def_readonly("size", &covis::core::Detection::MatrixT::size)
            .add_property("Identity", poseIdentity)
            .def("array", poseArray)
            ;
    def("Identity", Identity);


    /*
     * transform.h
     */
    def("transform", (PointCloud(*)(const PointCloud&, const covis::core::Detection::MatrixT&))transform, args("cloud", "pose"));
    def("transform", (PointCloud(*)(const PointCloud&, const object&))transform, args("cloud", "pose"));
    def("transform", (Mesh(*)(const Mesh&, const object&))transform, args("mesh", "pose"));
}

